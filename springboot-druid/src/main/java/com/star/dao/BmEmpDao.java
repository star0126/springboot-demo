package com.star.dao;

import com.star.entity.BmEmp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author guoqing-li
 * @since 2020-03-26
 */
public interface BmEmpDao extends BaseMapper<BmEmp> {

}
