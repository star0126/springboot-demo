package com.star.service.impl;

import com.star.entity.BmEmp;
import com.star.dao.BmEmpDao;
import com.star.service.BmEmpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author guoqing-li
 * @since 2020-03-26
 */
@Service
public class BmEmpServiceImpl extends ServiceImpl<BmEmpDao, BmEmp> implements BmEmpService {

}
