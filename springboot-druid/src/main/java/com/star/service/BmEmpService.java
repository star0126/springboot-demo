package com.star.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.star.entity.BmEmp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author guoqing-li
 * @since 2020-03-26
 */
public interface BmEmpService extends IService<BmEmp> {

}
