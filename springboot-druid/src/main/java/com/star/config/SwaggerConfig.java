package com.star.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-03-26 15:25:21
 * @version: v1.0
 * @description: Swagger 2  配置类
 *
 * <p>打开 http://localhost:8080/swagger-ui.html api接口管理平台</p>
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName(apiInfo().getTitle())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.star.controllers"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("springboot-druid")
                .description("springboot-druid by 李国庆")
                .termsOfServiceUrl("https://www.baidu.com/")
                .contact(new Contact("李国庆", "", ""))
                .version("1.0")
                .build();
    }
}
