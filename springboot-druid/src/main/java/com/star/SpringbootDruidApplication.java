package com.star;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-03-25 11:54:03
 * @version: v1.0
 * @description: springboot启动类
 **/

@SpringBootApplication
@MapperScan("com.star.dao")
public class SpringbootDruidApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootDruidApplication.class,args);
    }
}
