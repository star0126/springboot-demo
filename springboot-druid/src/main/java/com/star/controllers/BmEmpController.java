package com.star.controllers;


import com.star.entity.BmEmp;
import com.star.service.BmEmpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author guoqing-li
 * @since 2020-03-26
 */
@Api(tags = "Emp操作")
@RestController
@RequestMapping("/bm-emp")
public class BmEmpController {

    @Autowired
    private BmEmpService empService;


    @ApiOperation(value = "根据员工id获得员工信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "员工id", required = true, name = "id", paramType = "path")
    })
    @GetMapping("/emp-info-by-id/{id}")
    public Map<String,Object> getEmpInfoById(@PathVariable("id") String id){
        Map<String,Object> map = new HashMap<>();
        BmEmp emp = empService.getById(id);
        if (emp==null || "".equals(emp)){
            map.put("data",new BmEmp());
            return map;
        }
        map.put("data",emp);
        return map;
    }
}

