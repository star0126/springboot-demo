package com.star;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-03-25 11:57:21
 * @version: v1.0
 * @description: 单元测试类
 **/
@SpringBootTest
public class SpringBootDruidApplicationTests {

    @Autowired
    DataSource dataSource;

    /**
     * @author: guoqing-li
     * @create: 2020/3/25 14:19
     * @description: 测试数据库连接池是否更换
     */
    @Test
    void contextLoads() throws SQLException {
        System.out.println(dataSource.getClass());
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        connection.close();
    }
}
