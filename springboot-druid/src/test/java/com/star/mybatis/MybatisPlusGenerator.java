package com.star.mybatis;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-03-25 14:56:19
 * @version: v1.0
 * @description: mybatis-plus 代码生成器
 **/
@SpringBootTest
public class MybatisPlusGenerator {

    @Test
    void contextLoads(){
        String[] strings = {
                "bm_dept","bm_emp"
        };
        generator(strings);
    }



    /**
     * @author: guoqing-li
     * @create: 2020/3/25 15:09
     * @description: 自定义代码生成器方法
     * <p>代码生成器官方文档：https://mybatis.plus/guide/generator.html</p>
     * <p>代码生成器配置官方说明：https://mybatis.plus/config/generator-config.html#parent</p>
     */
    private static void generator(String[] tableNames) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局策略配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(System.getProperty("user.dir") + "/src/main/java")  /*文件输出目录*/
                    .setFileOverride(true)  /*是否覆盖已有文件*/
                    .setOpen(false)  /*是否打开输出目录*/
                    .setEnableCache(false)  /*是否在xml中添加二级缓存配置*/
                    .setAuthor(AUTHOR)  /*开发人员*/
                    .setKotlin(false)  /*是否开启Kotlin*/
                    .setSwagger2(true)  /*是否生成Swagger2注解*/
                    .setActiveRecord(false) /*是否开启ActiveRecord*/
                    .setBaseResultMap(true) /*是否开启ResultMap*/
                    .setBaseColumnList(true) /*是否开启BaseColumnList*/
                    .setIdType(ID_TYPE) /*主键类型策略*/  /*还有一个时间类型策略*/
                    .setEntityName("%s") /*实体类命名方式*/
                    .setMapperName("%sDao") /*mapper命名方式*/
                    .setXmlName("%sMapper") /*mapper xml命名方式*/
                    .setServiceName("%sService") /*service命名方式*/
                    .setServiceImplName("%sServiceImpl") /*service impl命名方式*/
                    .setControllerName("%sController"); /*controller命名方式*/
        mpg.setGlobalConfig(globalConfig);

        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DBTYPE)
                        .setUrl(URL)
                        .setDriverName(DRIVER_NAME)
                        .setUsername(USER_NAME)
                        .setPassword(PASS_WORD);
        mpg.setDataSource(dataSourceConfig);

        //数据库表配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel)  /*数据库表映射到实体的命名策略(驼峰)*/
                .setColumnNaming(NamingStrategy.underline_to_camel)  /*数据库表字段映射到实体的命名策略, 未指定按照 naming 执行*/
                .setEntityLombokModel(true)  /*是否为lombok模型*/
                .setControllerMappingHyphenStyle(true)  /*是否驼峰转连字符*/
                .setEntityTableFieldAnnotationEnable(true)  /*是否生成实体时，生成字段注解*/
                .setRestControllerStyle(false)  /*生成 @RestController 控制器*/
                .setInclude(tableNames);  /*需要包含的表名*/
        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENT_PACKAGE)
          .setEntity(ENTITY)
          .setService(SERVICE)
          .setServiceImpl(SERVICEIMPL)
          .setMapper(MAPPER)
          .setXml(XML)
          .setController(CONTROLLER);
        mpg.setPackageInfo(pc);

        // 模板配置
        TemplateConfig templateConfig = new TemplateConfig();
        mpg.setTemplate(templateConfig);
        mpg.execute();
    }

    //开发人员
    private static String AUTHOR="guoqing-li";

    //组件生成策略  INPUT为输入；ASSIGN_UUID为分配uuid
    private static IdType ID_TYPE = IdType.INPUT;

    //数据库类型
    private static DbType DBTYPE = DbType.MYSQL;

    //数据库地址
    private static String URL = "jdbc:mysql://localhost:3306/boot_module?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&autoReconnect=true&serverTimezone=Asia/Shanghai";

    //数据库驱动
    private static String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";

    //数据库账号
    private static String USER_NAME = "root";

    //数据库密码
    private static String PASS_WORD = "lgq123456";

    //生成文件的父包名（一般是项目的基本路径）
    private static String PARENT_PACKAGE = "com.star";


    //实体类包名
    private static String ENTITY = "entity";

    //service包名
    private static String SERVICE = "service";

    //service impl包名
    private static String SERVICEIMPL = "service.impl";

    //mapper包名
    private static String MAPPER = "dao";

    //mapper xml包名
    private static String XML = "dao.mybatis";

    //controller包名
    private static String CONTROLLER = "controllers";






}
