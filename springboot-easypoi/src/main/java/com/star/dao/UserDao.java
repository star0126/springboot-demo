package com.star.dao;

import com.star.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author guoqing-li
 * @since 2021-02-19
 */
public interface UserDao extends BaseMapper<User> {

}
