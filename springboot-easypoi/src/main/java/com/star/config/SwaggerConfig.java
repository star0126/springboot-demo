package com.star.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author guoqing_li
 * @create 2021-02-22  15:12:16
 * @description Swagger2 配置文件
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket createRestApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .groupName(apiInfo().getTitle())
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.star.controller"))
        .paths(PathSelectors.any())
        .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("springboot-easypoi")
        .description("springboot-easypoi by 李国庆")
        .termsOfServiceUrl("https://www.baidu.com/")
        .contact(new Contact("李国庆", "", ""))
        .version("1.0")
        .build();
  }
}
