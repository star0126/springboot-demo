package com.star.application;

import com.star.service.UserService;
import com.star.utils.HttpServletUtil;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author guoqing_li
 * @create 2021-02-19  14:25:21
 * @description manage类的公共父类
 **/
public class Base {

  protected static final Logger logger = LoggerFactory.getLogger(Base.class);

  @Autowired
  UserService userService;

  OutputStream excelOutputStream(String fileName)
      throws IOException {
    HttpServletResponse response = HttpServletUtil.getResponse();
    if (null == response) {
      return null;
    }
    response.setCharacterEncoding("UTF-8");
    response.setHeader("content-Type", "application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
    return response.getOutputStream();
  }

}
