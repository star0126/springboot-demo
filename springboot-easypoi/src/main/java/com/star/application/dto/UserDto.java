package com.star.application.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2021-02-19  14:28:52
 * @description 用户DTO类
 **/
public class UserDto implements Serializable {

  /**
   * 用户ID
   */
  @Excel(name = "用户ID", width = 15)
  private String userId;

  /**
   * 用户昵称
   */
  @Excel(name = "昵称", width = 20)
  private String nickname;

  /**
   * 手机号
   */
  @Excel(name = "手机号", width = 15)
  private String mobile;

  /**
   * 用户登录时间
   */
  @Excel(name = "登录时间", format = "yyyy年MM月dd日 HH:mm:ss", width = 32)
  private LocalDateTime lastLoginTime;

  /**
   * 是否VIP
   */
  @Excel(name = "是否VIP", replace = {"是_Y", "否_N"}, width = 8)
  private String isVip;

  /**
   * ID码
   */
  @Excel(name = "身份码", width = 15)
  private String idAccount;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public LocalDateTime getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(LocalDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public String getIsVip() {
    return isVip;
  }

  public void setIsVip(String isVip) {
    this.isVip = isVip;
  }

  public String getIdAccount() {
    return idAccount;
  }

  public void setIdAccount(String idAccount) {
    this.idAccount = idAccount;
  }

  @Override
  public String toString() {
    return "UserDto{" +
        "userId='" + userId + '\'' +
        ", nickname='" + nickname + '\'' +
        ", mobile='" + mobile + '\'' +
        ", lastLoginTime=" + lastLoginTime +
        ", isVip='" + isVip + '\'' +
        ", idAccount='" + idAccount + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDto userDto = (UserDto) o;
    return Objects.equals(userId, userDto.userId) &&
        Objects.equals(nickname, userDto.nickname) &&
        Objects.equals(mobile, userDto.mobile) &&
        Objects.equals(lastLoginTime, userDto.lastLoginTime) &&
        Objects.equals(isVip, userDto.isVip) &&
        Objects.equals(idAccount, userDto.idAccount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, nickname, mobile, lastLoginTime, isVip, idAccount);
  }
}
