package com.star.application;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.star.application.dto.UserDto;
import com.star.entity.User;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author guoqing_li
 * @create 2021-02-18  18:18:58
 * @description Excel导出
 **/
@Service
public class ExcelManage extends Base {

  /**
   * 导出简单的xlsx格式的Excel文件
   */
  public void exportSimpleXlsx() throws IOException {
    // 查询数据，做数据导出准备
    List<User> userList = userService.list();
    List<UserDto> result = new ArrayList<>(userList.size());
    for (User user : userList){
      UserDto userDto = new UserDto();
      BeanUtils.copyProperties(user,userDto);
      result.add(userDto);
    }
    // 构建easypoi导出参数实体
    ExportParams exportParams = new ExportParams("简单数据导出测试", "导出测试", ExcelType.XSSF);
    // 获得导出数据的Excel的Workbook对象
    Workbook workbook = ExcelExportUtil.exportExcel(exportParams, UserDto.class, result);
    // 将数据写出到响应体的输出流
    String fileName = "简单用户数据导出测试.xlsx";
    OutputStream outputStream = this.excelOutputStream(fileName);
    workbook.write(outputStream);
    logger.info("写出{}到响应体输出流结束", fileName);
  }

  /**
   * 导入简单的Excel文件
   * @param file 请求中的上传文件
   * @throws IOException io异常
   */
  public void importSimpleXlsx(MultipartFile file) throws IOException {
    // 导入参数设置
    ImportParams params = new ImportParams();
    /* 表格标题行数，默认0，导入时需要跳过标题行 */
    params.setTitleRows(1);
    /* 表头行数，默认1，导入时需要跳过表头行 */
    params.setHeadRows(1);
    /* 是否检验excel内容 */
    params.setNeedVerify(true);
    // 解析获得Excel文件内容的list集合
    List<UserDto> userDtoList;
    try {
      userDtoList = ExcelImportUtil.importExcel(file.getInputStream(), UserDto.class, params);
    } catch (NoSuchElementException e) {
      throw new IOException("excel文件不能为空");
    } catch (Exception e) {
      throw new IOException(e.getMessage());
    }
    // 如果所得的list集合不为空则批量写入数据库
    if (!CollectionUtils.isEmpty(userDtoList)){
      List<User> saveList = new ArrayList<>(userDtoList.size());
      for (UserDto userDto: userDtoList){
        // 建议对一些必要非空的属性进行一些线性校验，以防止写入数据库时出现报错
        if (userDto==null || userDto.getUserId()==null){
          continue;
        }
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        saveList.add(user);
      }
      if (saveList.size()>0){
        userService.saveBatch(saveList, 500);
      }
    }
    logger.info("写出{}到响应体输出流结束", file.getResource().getFilename());
  }

}
