package com.star;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li
 * @create 2021-02-18  17:30:15
 * @description springboot整合easypoi启动类
 **/
@SpringBootApplication
@MapperScan("com.star.dao")
public class SpeingbootEasypoiApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpeingbootEasypoiApplication.class,args);
  }
}
