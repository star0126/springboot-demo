package com.star.service.impl;

import com.star.entity.User;
import com.star.dao.UserDao;
import com.star.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author guoqing-li
 * @since 2021-02-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

}
