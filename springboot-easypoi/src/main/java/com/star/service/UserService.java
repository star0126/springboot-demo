package com.star.service;

import com.star.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author guoqing-li
 * @since 2021-02-19
 */
public interface UserService extends IService<User> {

}
