package com.star.controller;

import com.star.application.ExcelManage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author guoqing_li
 * @create 2021-02-19  17:03:05
 * @description excel操作控制器
 **/
@Api(tags = "Excel导入导出")
@RestController
@RequestMapping("/excel")
public class ExcelController {

  @Autowired
  private ExcelManage excelExportManage;

  @ApiOperation(value = "简单数据导出测试接口(xlsx格式)")
  @GetMapping("/simple-export")
  public ResponseEntity<Void> simpleExport() throws IOException {
    excelExportManage.exportSimpleXlsx();
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @ApiOperation(value = "简单数据导入测试接口")
  @PostMapping("/simple-import")
  public ResponseEntity<Void> simpleImport(MultipartFile file) throws IOException {
    excelExportManage.importSimpleXlsx(file);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

}
