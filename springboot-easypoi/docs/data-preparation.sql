-- 数据准备SQL
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
  `nickname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `mobile` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '用户登录时间',
  `is_vip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N'  COMMENT '是否VIP',
  `id_account` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL  COMMENT 'ID码',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `uk_user_id_account`(`id_account`) USING BTREE,
  INDEX `idx_nickname`(`nickname`) USING BTREE,
  INDEX `idx_mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37753297 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqn3avm', 'ID66995862', 'ID66995862', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqn2emh', 'ID16292230', 'ID16292230', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbgcxjahz', 'ID41503951', 'ID41503951', '18855083499', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klasntq6zq', 'ID16430950', 'ID16430950', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klao1rywca', 'ID98400102', 'ID98400102', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkrvbjdouf', 'ID72447005', 'ID72447005', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkgkc7xwuf', 'ID67728666', 'ID67728666', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjzfayu3qw', 'ID86173051', '哲小易', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjpfqq7res', 'ID98955900', 'ID98955900', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjkzzhcezj', 'ID61219268', '大风起兮123', '18834549299', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjhz27ocaz', 'ID29409653', '瑶童', '18864113399', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('khr8shd7wp', 'ID80807384', 'ID80807384', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kf9e5gmaza', 'ID79674886', '许士恩', '18826152199', '2021-02-19 10:39:53', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kbxfwhvwxx', 'ID80963479', 'ID80963479', NULL, '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('k8nufrkkgr', 'ID40989180', 'zh123123', '18849391899', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('k7x9uc43ob', 'ID54133317', '您的牧野', '18818621099', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('k63jdeovkf', 'ID56407714', '熊道保', '18872231399', '2021-02-19 10:39:53', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqmwbnj', 'ID99585869', 'ID99585869', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqmiufg', 'ID54460888', 'ID54460888', '18894639199', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqm94vk', 'ID22490749', 'ID22490749', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqc23tm', 'ID10660555', 'ID10660555', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbm19ifcg', 'ID69729271', 'ID69729271', '18871271399', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbfr7sqbq', 'ID75138190', 'ID75138190', '18838954199', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klb2z4g5yi', 'ID60821970', 'ID60821970', '18888715399', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kla9z4yjwa', 'ID28435337', 'ID28435337', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kl9zf3fzgy', 'ID36228435', 'ID36228435', '18804128799', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kknb1le8rm', 'ID36579113', 'ID36579113', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjmf5hb3br', 'ID52220075', 'L木木H', NULL, '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kieflsshsw', 'ID96943247', 'ID96943247', '18803583399', '2021-02-19 10:39:52', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('ki3ijv97wb', 'ID63744717', 'ID63744717', '18837114699', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('khpmzg8wjm', 'ID17139625', '感恩有你520', '18814207399', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kch3uhdgdl', 'ID54807891', 'ID54807891', '18847835899', '2021-02-19 10:39:52', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqlv3rt', 'ID40044951', 'ID40044951', '18869142799', '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqlg2kn', 'ID81087912', 'ID81087912', NULL, '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klblhk5uro', 'ID40005712', 'ID40005712', '18879652899', '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkrtot2cvf', 'ID70584671', 'ID70584671', NULL, '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kklwc1n8vb', 'ID70294370', '宋万里', NULL, '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkjbb66rut', 'ID84164572', 'ID84164572', NULL, '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kiyjy187rx', 'ID12895174', '孙乐隆', '18896228599', '2021-02-19 10:39:51', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kh01zvzmgm', 'ID10647241', 'ID10647241', '18813356399', '2021-02-19 10:39:51', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqlasvb', 'ID50119621', 'ID50119621', NULL, '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboql8ptl', 'ID90173202', 'ID90173202', NULL, '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqkv5ha', 'ID16268433', 'ID16268433', '18865740099', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqd2ddk', 'ID29343559', 'ID29343559', NULL, '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klaygor8qb', 'ID57681815', 'ID57681815', '18842516599', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klag6ohhtf', 'ID92860931', 'ID92860931', '18829099599', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkdsgbabsn', 'ID13143334', 'ID13143334', '18812054499', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kk2jui9kju', 'ID69909809', 'ID69909809', '18803076699', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjwbzr00pk', 'ID35514763', 'ID35514763', NULL, '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjtvifz6ye', 'ID90070225', 'ID90070225', NULL, '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kgve7j65rn', 'ID22027676', '鹤鸣道人', '18821327099', '2021-02-19 10:39:50', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kb5wormprk', 'ID23788582', '蒂凝', '18810860799', '2021-02-19 10:39:50', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('jxbl4bi6nu', 'ID63619417', 'ID63619417', '18841903899', '2021-02-19 10:39:50', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqk3pju', 'ID40825588', 'ID40825588', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbn8hcolm', 'ID85958864', 'ID85958864', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbmbl3jrl', 'ID11789253', '三风六月', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klblru4jhk', 'ID85273474', 'ID85273474', '18875627799', '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klaarij2ky', 'ID37358423', 'ID37358423', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkor03ehqi', 'ID52889973', 'ID52889973', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkdfhfvgos', 'ID83909802', 'ID83909802', '18836736799', '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkdbgvghjf', 'ID35432949', 'ID35432949', '18821617899', '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kk6erd8kzv', 'ID10660231', 'ID10660231', '18875481299', '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjtndxu4rv', 'ID83576841', 'ID83576841', NULL, '2021-02-19 10:39:49', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kh32ds0dkv', 'ID32619391', '国际庄小超', '18832193299', '2021-02-19 10:39:49', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqchzfx', 'ID19186825', 'ID19186825', NULL, '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkpjg4wkcu', 'ID85485491', 'zyylinda', '18888834799', '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkor8ithia', 'ID13016840', 'ID13016840', '18833824399', '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkni3xxfsg', 'ID43179949', 'ID43179949', NULL, '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkdgtowwuo', 'ID10365417', 'ID10365417', '18862198199', '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjyek2eley', 'ID78680665', '老左的世界', '18822351199', '2021-02-19 10:39:48', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjbama1utq', 'ID46814179', 'ID46814179', '18877918199', '2021-02-19 10:39:48', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kh5qkjy9ev', 'ID19274124', '萍萍101', '18857140499', '2021-02-19 10:39:48', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kgg7atnzws', 'ID79279946', '慕夏二九', '18832124099', '2021-02-19 10:39:48', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kc47lmc2xz', 'ID18216148', '虎他娘', '18891895499', '2021-02-19 10:39:48', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('jphsgob2xa', 'ID06530325', '忧小赖', '18872729299', '2021-02-19 10:39:48', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqiwceh', 'ID44729174', 'ID44729174', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqiv2bu', 'ID19303462', 'ID19303462', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqiqwkz', 'ID34740917', 'ID34740917', '18842120999', '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqiioin', 'ID49295118', 'ID49295118', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbo6di9fn', 'ID68332014', 'ID68332014', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klblao12ub', 'ID47258907', 'ID47258907', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klb4ax5ugi', 'ID70402492', 'ID70402492', '18850834899', '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkp7vmudbt', 'ID34901495', 'ID34901495', NULL, '2021-02-19 10:39:47', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbg7we9pu', 'ID54933073', 'ID54933073', '18859338999', '2021-02-19 10:39:46', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klanpgajfz', 'ID77764640', 'ID77764640', NULL, '2021-02-19 10:39:46', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klagpusmwn', 'ID72235522', 'ID72235522', '18837100699', '2021-02-19 10:39:46', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkexnoyrgc', 'ID28359563', 'ID28359563', '18809575099', '2021-02-19 10:39:46', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kk20636dpd', 'ID82998314', '吸引seeing', '18873323799', '2021-02-19 10:39:46', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('k8jjay1eyo', 'ID26664884', 'zcg2666', '18839237999', '2021-02-19 10:39:46', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('k4ey10hgyd', 'ID13257731', '快乐流星雨', '18836167599', '2021-02-19 10:39:46', 'Y');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klboqgtxzo', 'ID41975501', 'ID41975501', NULL, '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbopuqpez', 'ID78590016', 'ID78590016', NULL, '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klbchwqgag', 'ID38170778', 'ID38170778', '18854085199', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klaykeojqp', 'ID52673622', 'ID52673622', '18812258999', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('klac6ijaps', 'ID60221941', '蓝精灵620安', '18883521999', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kla874u3aj', 'ID89537703', 'ID89537703', '18829715799', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kkhq85icmd', 'ID18375189', 'ID18375189', NULL, '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('kjsfg6ogyq', 'ID26608262', 'ID26608262', '18804171199', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('khr9sdapvw', 'ID72467280', 'ID72467280', '18879619199', '2021-02-19 10:39:45', 'N');
INSERT INTO `user`(user_id,id_account,nickname,mobile,last_login_time,is_vip) VALUES ('khogb88oac', 'ID10397832', '游要有方', '18861831299', '2021-02-19 10:39:45', 'Y');

