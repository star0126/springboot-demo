package com.star.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * @author guoqing_li   2022-02-17  23:19:04
 **/
@Component
public class TestJob extends QuartzJobBean {

    private static final Logger log = LoggerFactory.getLogger(TestJob.class);

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("---------- {} 开始执行发送顾客使用报告定时任务",simpleDateFormat.format(context.getFireTime()));
    }
}
