package com.star.config;

import com.star.quartz.TestJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author guoqing_li   2022-02-17  23:13:47
 * Quartz配置类
 **/
@Configuration
public class QuartzConfig {

    @Bean
    public JobDetail testJobDetail(){
        return JobBuilder.newJob(TestJob.class).withIdentity("testQuartz").storeDurably().build();
    }

    @Bean
    public Trigger testTrigger(){
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0/1 * * * * ? *");
        return TriggerBuilder.newTrigger().forJob(testJobDetail())
                .withIdentity("testTrigger")
                .withSchedule(scheduleBuilder)
                .build();
    }


}
