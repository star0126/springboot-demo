package com.star.util;

/**
 * @author guoqing_li
 * @create 2020-11-17  10:00:39
 * @description
 **/
public class MyCharUtil {

  /**
   * 判断是否是空白字符
   * 包括空格、制表符、全角空格和不间断空格
   * @param c 需判断的字符
   * @return 判断结果
   */
  public static boolean isBlankChar(int c) {
    return Character.isWhitespace(c)
        || Character.isSpaceChar(c)
        || c == '\ufeff'
        || c == '\u202a';
  }

}
