package com.star.util;

/**
 * @author guoqing_li
 * @create 2020-11-17  09:29:48
 * @description 字符串工具类
 **/
public class MyStrUtil {

  /**
   * 判断字符串是否为空
   * null值、空格、全角空格、制表符、换行符，等不可见字符都会被判定为空
   * @param str 要判断的字符串
   * @return 判断结果
   */
  public static boolean isBlank(CharSequence str) {
    int length;

    if ((str == null) || ((length = str.length()) == 0)) {
      return true;
    }

    for (int i = 0; i < length; i++) {
      // 只要有一个非空字符即为非空字符串
      if (!MyCharUtil.isBlankChar(str.charAt(i))) {
        return false;
      }
    }

    return true;
  }

}
