package com.star.es.model;

import java.util.Map;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2020-11-19  10:33:11
 * @description es中一条文档的模型
 **/
public class DocModel {

  private String _index;
  private String _type;
  private String _id;
  private Integer _version;
  private Integer _seq_no;
  private Integer _primary_term;
  private Boolean found;
  private Map<String,Object> _source;

  public DocModel() {
  }

  public String get_index() {
    return _index;
  }

  public void set_index(String _index) {
    this._index = _index;
  }

  public String get_type() {
    return _type;
  }

  public void set_type(String _type) {
    this._type = _type;
  }

  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }

  public Integer get_version() {
    return _version;
  }

  public void set_version(Integer _version) {
    this._version = _version;
  }

  public Integer get_seq_no() {
    return _seq_no;
  }

  public void set_seq_no(Integer _seq_no) {
    this._seq_no = _seq_no;
  }

  public Integer get_primary_term() {
    return _primary_term;
  }

  public void set_primary_term(Integer _primary_term) {
    this._primary_term = _primary_term;
  }

  public Boolean getFound() {
    return found;
  }

  public void setFound(Boolean found) {
    this.found = found;
  }

  public Map<String, Object> get_source() {
    return _source;
  }

  public void set_source(Map<String, Object> _source) {
    this._source = _source;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocModel docModel = (DocModel) o;
    return Objects.equals(_index, docModel._index) &&
        Objects.equals(_type, docModel._type) &&
        Objects.equals(_id, docModel._id) &&
        Objects.equals(_version, docModel._version) &&
        Objects.equals(_seq_no, docModel._seq_no) &&
        Objects.equals(_primary_term, docModel._primary_term) &&
        Objects.equals(found, docModel.found) &&
        Objects.equals(_source, docModel._source);
  }

  @Override
  public int hashCode() {
    return Objects.hash(_index, _type, _id, _version, _seq_no, _primary_term, found, _source);
  }

  @Override
  public String toString() {
    return "DocModel{" +
        "_index='" + _index + '\'' +
        ", _type='" + _type + '\'' +
        ", _id='" + _id + '\'' +
        ", _version=" + _version +
        ", _seq_no=" + _seq_no +
        ", _primary_term=" + _primary_term +
        ", found=" + found +
        ", _source=" + _source +
        '}';
  }
}
