package com.star.es.model;

import java.util.List;
import java.util.Objects;
import org.frameworkset.elasticsearch.entity.Shards;

/**
 * @author guoqing_li
 * @create 2020-11-19  11:08:18
 * @description es文档集合模型
 **/
public class DocListModel {

  private Integer took;
  private Boolean timed_out;
  private Shards _shards;
  private Hits hits;

  public DocListModel() {
  }

  public Integer getTook() {
    return took;
  }

  public void setTook(Integer took) {
    this.took = took;
  }

  public Boolean getTimed_out() {
    return timed_out;
  }

  public void setTimed_out(Boolean timed_out) {
    this.timed_out = timed_out;
  }

  public Shards get_shards() {
    return _shards;
  }

  public void set_shards(Shards _shards) {
    this._shards = _shards;
  }

  public Hits getHits() {
    return hits;
  }

  public void setHits(Hits hits) {
    this.hits = hits;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocListModel that = (DocListModel) o;
    return Objects.equals(took, that.took) &&
        Objects.equals(timed_out, that.timed_out) &&
        Objects.equals(_shards, that._shards) &&
        Objects.equals(hits, that.hits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(took, timed_out, _shards, hits);
  }

  @Override
  public String toString() {
    return "DocListModel{" +
        "took=" + took +
        ", timed_out=" + timed_out +
        ", _shards=" + _shards +
        ", hits=" + hits +
        '}';
  }

  public static class Shards{
    private Integer total;
    private Integer successful;
    private Integer skipped;
    private Integer failed;

    public Shards() {
    }

    public Integer getTotal() {
      return total;
    }

    public void setTotal(Integer total) {
      this.total = total;
    }

    public Integer getSuccessful() {
      return successful;
    }

    public void setSuccessful(Integer successful) {
      this.successful = successful;
    }

    public Integer getSkipped() {
      return skipped;
    }

    public void setSkipped(Integer skipped) {
      this.skipped = skipped;
    }

    public Integer getFailed() {
      return failed;
    }

    public void setFailed(Integer failed) {
      this.failed = failed;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Shards shards = (Shards) o;
      return Objects.equals(total, shards.total) &&
          Objects.equals(successful, shards.successful) &&
          Objects.equals(skipped, shards.skipped) &&
          Objects.equals(failed, shards.failed);
    }

    @Override
    public int hashCode() {
      return Objects.hash(total, successful, skipped, failed);
    }

    @Override
    public String toString() {
      return "Shards{" +
          "total=" + total +
          ", successful=" + successful +
          ", skipped=" + skipped +
          ", failed=" + failed +
          '}';
    }
  }

  public static class Hits{
    private Total total;
    private Integer max_score;
    private List<DocModel> hits;

    public Hits() {
    }

    public Total getTotal() {
      return total;
    }

    public void setTotal(Total total) {
      this.total = total;
    }

    public Integer getMax_score() {
      return max_score;
    }

    public void setMax_score(Integer max_score) {
      this.max_score = max_score;
    }

    public List<DocModel> getHits() {
      return hits;
    }

    public void setHits(List<DocModel> hits) {
      this.hits = hits;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Hits hits1 = (Hits) o;
      return Objects.equals(total, hits1.total) &&
          Objects.equals(max_score, hits1.max_score) &&
          Objects.equals(hits, hits1.hits);
    }

    @Override
    public int hashCode() {
      return Objects.hash(total, max_score, hits);
    }

    @Override
    public String toString() {
      return "Hits{" +
          "total=" + total +
          ", max_score=" + max_score +
          ", hits=" + hits +
          '}';
    }

  }

  public static class Total{
    private Integer value;
    private String relation;

    public Total() {
    }

    public Integer getValue() {
      return value;
    }

    public void setValue(Integer value) {
      this.value = value;
    }

    public String getRelation() {
      return relation;
    }

    public void setRelation(String relation) {
      this.relation = relation;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Total total = (Total) o;
      return Objects.equals(value, total.value) &&
          Objects.equals(relation, total.relation);
    }

    @Override
    public int hashCode() {
      return Objects.hash(value, relation);
    }

    @Override
    public String toString() {
      return "Total{" +
          "value=" + value +
          ", relation='" + relation + '\'' +
          '}';
    }
  }

}
