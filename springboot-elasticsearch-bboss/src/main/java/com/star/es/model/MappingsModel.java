package com.star.es.model;

import java.util.Map;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2020-11-20  09:36:56
 * @description es映射模型
 **/
public class MappingsModel {

  private Properties mappings;

  public MappingsModel() {
  }

  public Properties getMappings() {
    return mappings;
  }

  public void setMappings(Properties mappings) {
    this.mappings = mappings;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MappingsModel that = (MappingsModel) o;
    return Objects.equals(mappings, that.mappings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mappings);
  }

  @Override
  public String toString() {
    return "MappingsModel{" +
        "mappings=" + mappings +
        '}';
  }

  public static class Properties{

    private Map<String,String> properties;

    public Properties() {
    }

    public Map<String, String> getProperties() {
      return properties;
    }

    public void setProperties(Map<String, String> properties) {
      this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Properties that = (Properties) o;
      return Objects.equals(properties, that.properties);
    }

    @Override
    public int hashCode() {
      return Objects.hash(properties);
    }

    @Override
    public String toString() {
      return "Properties{" +
          "properties=" + properties +
          '}';
    }
  }

}
