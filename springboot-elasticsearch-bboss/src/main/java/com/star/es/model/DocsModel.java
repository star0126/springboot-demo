package com.star.es.model;

import java.util.List;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2020-11-19  14:02:51
 * @description es多条文档集模型
 **/
public class DocsModel {

  private List<DocModel> docs;

  public DocsModel() {
  }

  public List<DocModel> getDocs() {
    return docs;
  }

  public void setDocs(List<DocModel> docs) {
    this.docs = docs;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocsModel docsModel = (DocsModel) o;
    return Objects.equals(docs, docsModel.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    return "DocsModel{" +
        "docs=" + docs +
        '}';
  }
}
