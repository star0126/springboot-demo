package com.star.es;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.star.es.model.DocListModel;
import com.star.es.model.DocModel;
import com.star.es.model.DocsModel;
import com.star.es.model.MappingsModel;
import com.star.exceptions.BusinessException;
import com.star.exceptions.PrimaryKeyNotFoundException;
import com.star.util.MyStrUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.client.ClientInterface;
import org.frameworkset.elasticsearch.client.ClientUtil;
import org.frameworkset.elasticsearch.entity.ESIndice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author guoqing_li
 * @create 2020-11-16  18:08:44
 * @description elasticsearch客户端管理类
 **/
@Service
public class EsManage {

  @Autowired
  private BBossESStarter bbossESStarter;

  private static final String format = "yyyy-MM-dd HH:mm:ss";


  /**
   * 获得es中的索引信息
   * @return 索引信息集合
   */
  public List<ESIndice> getIndexes(){
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.getIndexes();
  }

  /**
   * 添加一条文档并指定ID
   * @param indexName 索引名
   * @param docObject 文档对象
   * @param docId 文档ID
   * @param dateFormat 文档中时间类型值的格式化形式，如：yyyy-MM-dd HH:mm:ss
   * @return 请求的响应结果
   */
  public String addDoc(String indexName,Object docObject,String docId,String dateFormat){
    String path = indexName.concat("/_doc/").concat(docId);
    String body = JSONObject.toJSONStringWithDateFormat(docObject, MyStrUtil.isBlank(dateFormat) ? format : dateFormat);
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.executeHttp(path, body, ClientUtil.HTTP_POST);
  }

  /**
   * 批量添加文档(单条文档使用map集合表示)并指定ID
   * @param indexName 索引名
   * @param docs map类型文档的list集合
   * @param idKey 文档的ID的map key名
   * @param dateFormat 文档中时间类型值的格式化形式，如：yyyy-MM-dd HH:mm:ss
   * @return 请求的响应结果
   */
  public String addMapDocs(String indexName, List<Map<String,Object>> docs,String idKey,String dateFormat){
    if (docs.isEmpty()) return null;
    String path = "_bulk";
    StringBuilder stringBuilder = new StringBuilder();
    for (Map<String,Object> doc : docs){
      if (null==doc){
        throw new BusinessException("es文档不能为null");
      }
      Object id = doc.get(idKey);
      if (id==null || MyStrUtil.isBlank(id.toString())){
        throw new PrimaryKeyNotFoundException("es文档主键未找到");
      }
      stringBuilder.append("{ \"index\" : { \"_index\" : \"").append(indexName).append("\", \"_id\" : \"").append(id).append("\" } }\n");
      stringBuilder.append(JSONObject.toJSONStringWithDateFormat(doc,MyStrUtil.isBlank(dateFormat) ? format : dateFormat)).append("\n");
    }
    String body = stringBuilder.toString();
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.executeHttp(path,body,ClientUtil.HTTP_POST);
  }

  /**
   * 根据ID删除文档
   * @param indexName 索引名
   * @param docId 文档ID
   * @return 响应结果
   */
  public String delDoc(String indexName,String docId){
    String path = indexName.concat("/_doc/").concat(docId);
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.executeHttp(path, ClientUtil.HTTP_DELETE);
  }

  /**
   * 根据文档ID集合批量删除文档
   * @param indexName 索引名
   * @param ids 文档ID集合
   * @return 响应结果
   */
  public String delDocs(String indexName,String[] ids){
    if (null==ids || ids.length<=0) return null;
    String path = "_bulk";
    StringBuilder stringBuilder = new StringBuilder();
    for (String id:ids){
      stringBuilder.append("{ \"delete\" : { \"_index\" : \"").append(indexName).append("\", \"_id\" : \"").append(id).append("\" } }\n");
    }
    String body = stringBuilder.toString();
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.executeHttp(path,body,ClientUtil.HTTP_POST);
  }

  /**
   * 判断索引是否存在
   * @param indexName 索引名
   * @return 判断结果，索引不存在会抛出404异常，所以try catch
   */
  public Object indexIsExist(String indexName){
    ClientInterface restClient = bbossESStarter.getRestClient();
    try {
      restClient.executeHttp(indexName,ClientUtil.HTTP_HEAD);
      return true;
    }catch (Exception e){
      return false;
    }
  }

  /**
   * 获取索引的settings
   * @param indexName 索引名
   * @return 索引settings json
   */
  public String getIndexSettings(String indexName){
    ClientInterface restClient = bbossESStarter.getRestClient();
    return restClient.executeHttp(indexName+"/_settings", ClientUtil.HTTP_GET);
  }

  /**
   * 获取索引映射信息
   * @param indexName 索引名
   * @return 索引映射信息
   */
  public MappingsModel getIndexMapping(String indexName){
    String path = indexName.concat("/_mapping");
    ClientInterface restClient = bbossESStarter.getRestClient();
    String response = restClient.executeHttp(path, ClientUtil.HTTP_GET);
    if (null==response) return null;
    HashMap<String, MappingsModel> mappings =
        JSONObject.parseObject(response, new TypeReference<HashMap<String, MappingsModel>>() {});
    if (null==mappings) return null;
    return mappings.get(indexName);
  }

  /**
   * 获取索引字段信息
   * @param indexName 索引名
   * @return 字段信息列表
   */
  public Set<String> getIndexFields(String indexName){
    MappingsModel indexMapping = this.getIndexMapping(indexName);
    if (null==indexMapping
        || null==indexMapping.getMappings()
        || null==indexMapping.getMappings().getProperties()){
      return null;
    }
    return indexMapping.getMappings().getProperties().keySet();
  }

  /**
   * 根据文档ID获取文档
   * @param indexName 索引名
   * @param docId 文档ID
   * @return 文档模型
   */
  public DocModel getDocById(String indexName,String docId){
    String path = indexName.concat("/_doc/").concat(docId);
    ClientInterface restClient = bbossESStarter.getRestClient();
    String doc = restClient.executeHttp(path, ClientUtil.HTTP_GET);
    if (null==doc) return null;
    return JSONObject.parseObject(doc, DocModel.class);
  }

  /**
   * 根据文档ID获取文档的source内容
   * @param indexName 索引名
   * @param docId 文档ID
   * @return 文档Map
   */
  public Map<String,Object> getDocSourceById(String indexName,String docId){
    String path = indexName.concat("/_doc/").concat(docId).concat("/_source");
    ClientInterface restClient = bbossESStarter.getRestClient();
    String doc = restClient.executeHttp(path, ClientUtil.HTTP_GET);
    if (null==doc) return null;
    return JSONObject.parseObject(doc, new TypeReference<HashMap<String, Object>>() {});
  }

  /**
   * 根据文档id数组批量获取文档
   * @param indexName 索引名
   * @param ids 文档ID数组
   * @return 文档集合
   */
  public List<DocModel> mgetDocByIds(String indexName,String[] ids){
    if (ids==null || ids.length<=0) return null;
    String path = indexName.concat("/_mget");
    Map<String,String[]> idsMap = new HashMap<>();
    idsMap.put("ids", ids);
    ClientInterface restClient = bbossESStarter.getRestClient();
    String docs = restClient.executeHttp(path, JSONObject.toJSONString(idsMap), ClientUtil.HTTP_POST);
    if (null==docs) return null;
    DocsModel docsModel = JSONObject.parseObject(docs, DocsModel.class);
    if (docsModel==null) return null;
    return docsModel.getDocs();
  }

  public Object mgetDocSourceByIds(String indexName,String[] ids){
    if (ids==null || ids.length<=0) return null;
    String path = indexName.concat("/_mget");
    Map<String,Object>[] docs = new Map[ids.length];
    for (int i = 0; i < ids.length; i++) {
      Map<String,Object> idMap = new HashMap<>();
      idMap.put("_id",ids[i]);
      idMap.put("_source",false);
      docs[i]=idMap;
    }
    Map<String,Map<String,Object>[]> docsMap = new HashMap<>();
    docsMap.put("docs", docs);
    ClientInterface restClient = bbossESStarter.getRestClient();
    String response = restClient.executeHttp(path, JSONObject.toJSONString(docsMap), ClientUtil.HTTP_POST);
    if (null==response) return null;
    return JSONObject.parseObject(response,new TypeReference<HashMap<String, Object>>() {});
  }

  /**
   * 使用match_all查询获取全匹配文档集合
   * @param indexName 索引名
   * @param page 第几页
   * @param size 页大小
   * @return 文档集合
   */
  public List<DocModel> getMatchAllDocs(String indexName,Integer page,Integer size){
    String path = indexName.concat("/_search");
    StringBuilder stringBuilder = new StringBuilder("{\"query\": {\"match_all\": {}},\"from\": ");
    stringBuilder.append(page==null ? 0 : (page-1)*size);
    stringBuilder.append(",\"size\": ");
    stringBuilder.append(size==null ? 10 : size);
    stringBuilder.append("}");
    ClientInterface restClient = bbossESStarter.getRestClient();
    String response = restClient.executeHttp(path, stringBuilder.toString(), ClientUtil.HTTP_POST);
    if (null==response) return null;
    DocListModel docListModel = JSONObject.parseObject(response, DocListModel.class);
    if (docListModel==null || docListModel.getHits()==null || docListModel.getHits().getHits()==null){
      return null;
    }
    return docListModel.getHits().getHits();
  }

  /**
   * 使用match_all查询获取全匹配文档集合
   * @return 文档集合
   */
  public List<DocModel> getMatchAllDocs(String indexName){
    return this.getMatchAllDocs(indexName,null,null);
  }

  /**
   * 使用match_all查询获取全匹配文档集合中每条文档的source集合
   * @param indexName 索引名
   * @param page 第几页
   * @param size 页大小
   * @return 文档source集合
   */
  public List<Map<String,Object>> getMatchAllDocsSource(String indexName,Integer page,Integer size){
    List<DocModel> matchAllDocs = this.getMatchAllDocs(indexName,page,size);
    if (null==matchAllDocs) return null;
    List<Map<String,Object>> result = new ArrayList<>();
    for (DocModel docModel:matchAllDocs){
      Map<String, Object> source = docModel.get_source();
      if (source==null) continue;
      result.add(source);
    }
    return result;
  }

  /**
   * 使用match_all查询获取全匹配文档集合中每条文档的source集合
   * @param indexName 索引名
   * @return 文档source集合
   */
  public List<Map<String,Object>> getMatchAllDocsSource(String indexName){
    List<DocModel> matchAllDocs = this.getMatchAllDocs(indexName);
    if (null==matchAllDocs) return null;
    List<Map<String,Object>> result = new ArrayList<>();
    for (DocModel docModel:matchAllDocs){
      Map<String, Object> source = docModel.get_source();
      if (source==null) continue;
      result.add(source);
    }
    return result;
  }

}
