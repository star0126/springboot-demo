package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li
 * @create 2020-11-13  17:07:27
 * @description
 **/
@SpringBootApplication
public class SpringbootElasticsearchBbossApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootElasticsearchBbossApplication.class,args);
  }
}
