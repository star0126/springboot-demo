package com.star.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2020-11-16  16:04:27
 * @description
 **/
public class People implements Serializable {

  private String Id;
  private String about;
  private String address;
  private Integer age;
  private Date birthday;
  private String citizenship;
  private Double height;
  private String name;
  private String nation;
  private String nickname;
  private String sex;
  private Double weight;

  public People() {
  }

  public String getId() {
    return Id;
  }

  public void setId(String id) {
    Id = id;
  }

  public String getAbout() {
    return about;
  }

  public void setAbout(String about) {
    this.about = about;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getCitizenship() {
    return citizenship;
  }

  public void setCitizenship(String citizenship) {
    this.citizenship = citizenship;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNation() {
    return nation;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    People people = (People) o;
    return Objects.equals(Id, people.Id) &&
        Objects.equals(about, people.about) &&
        Objects.equals(address, people.address) &&
        Objects.equals(age, people.age) &&
        Objects.equals(birthday, people.birthday) &&
        Objects.equals(citizenship, people.citizenship) &&
        Objects.equals(height, people.height) &&
        Objects.equals(name, people.name) &&
        Objects.equals(nation, people.nation) &&
        Objects.equals(nickname, people.nickname) &&
        Objects.equals(sex, people.sex) &&
        Objects.equals(weight, people.weight);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(Id, about, address, age, birthday, citizenship, height, name, nation, nickname, sex,
            weight);
  }

  @Override
  public String toString() {
    return "People{" +
        "Id='" + Id + '\'' +
        ", about='" + about + '\'' +
        ", address='" + address + '\'' +
        ", age=" + age +
        ", birthday=" + birthday +
        ", citizenship='" + citizenship + '\'' +
        ", height=" + height +
        ", name='" + name + '\'' +
        ", nation='" + nation + '\'' +
        ", nickname='" + nickname + '\'' +
        ", sex='" + sex + '\'' +
        ", weight=" + weight +
        '}';
  }
}
