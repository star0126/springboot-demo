package com.star.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author guoqing_li
 * @create 2020-11-16  11:56:51
 * @description
 **/
public class AddDocsResponse implements Serializable {

  private Integer took;
  private Boolean errors;
  private List<String> items;

  public AddDocsResponse() {
  }

  public Integer getTook() {
    return took;
  }

  public void setTook(Integer took) {
    this.took = took;
  }

  public Boolean getErrors() {
    return errors;
  }

  public void setErrors(Boolean errors) {
    this.errors = errors;
  }

  public List<String> getItems() {
    return items;
  }

  public void setItems(List<String> items) {
    this.items = items;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddDocsResponse that = (AddDocsResponse) o;
    return Objects.equals(took, that.took) &&
        Objects.equals(errors, that.errors) &&
        Objects.equals(items, that.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(took, errors, items);
  }

  @Override
  public String toString() {
    return "AddDocsResponse{" +
        "took=" + took +
        ", errors=" + errors +
        ", items=" + items +
        '}';
  }
}
