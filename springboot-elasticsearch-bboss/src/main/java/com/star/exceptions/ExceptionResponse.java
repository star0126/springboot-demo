package com.star.exceptions;


/**
 * @author guoqing_li
 * @create 2020-11-17  11:16:40
 * @description 异常响应
 **/

public class ExceptionResponse {

  private Object message;
  private Integer code;

  public ExceptionResponse() {
  }

  public ExceptionResponse(Object message, Integer code) {
    this.message = message;
    this.code = code;
  }

  public Object getMessage() {
    return message;
  }

  public void setMessage(Object message) {
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  @Override
  public String toString() {
    return "ExceptionResponse{" +
        "message=" + message +
        ", code=" + code +
        '}';
  }
}
