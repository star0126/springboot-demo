package com.star.exceptions;

/**
 * @author guoqing_li
 * @create 2020-11-17  11:19:01
 * @description 通用异常
 **/
public class BusinessException  extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private Integer code;

  public BusinessException(String message) {
    super(message);
  }

  public BusinessException(Integer code, String message) {
    this(code, message, true);
  }

  public BusinessException(Integer code, String message, boolean propertiesKey) {
    super(message);
    this.setCode(code);
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }
}
