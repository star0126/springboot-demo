package com.star.exceptions;

import bboss.org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author guoqing_li
 * @create 2020-11-17  11:11:46
 * @description 全局异常处理
 **/
@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * 主键未找到异常 500
   */
  @ExceptionHandler(PrimaryKeyNotFoundException.class)
  @ResponseBody
  ResponseEntity<ExceptionResponse> primaryKeyNotFoundExceptionHandler(BusinessException e) {
    ExceptionResponse exceptionResponse = new  ExceptionResponse();
    exceptionResponse.setCode(e.getCode());
    exceptionResponse.setMessage(e.getMessage());
    return ResponseEntity.badRequest().body(exceptionResponse);
  }

  /**
   * 400异常，验证失败
   */
  @ExceptionHandler(BusinessException.class)
  @ResponseBody
  ResponseEntity<ExceptionResponse> businessExceptionHandler(BusinessException e) {
    ExceptionResponse exceptionResponse = new  ExceptionResponse();
    exceptionResponse.setCode(HttpStatus.BAD_REQUEST.value());
    exceptionResponse.setMessage(e.getMessage());
    return ResponseEntity.badRequest().body(exceptionResponse);
  }

  /**
   * 409异常，无效的请求数据格式.
   */
  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseBody
  ResponseEntity handleConflictException(IllegalArgumentException e) {
    return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
  }

  /**
   * 404异常，资源不存在.
   */
  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseBody
  ResponseEntity handleNotFoundException(ResourceNotFoundException e) {
    return ResponseEntity.notFound().build();
  }

}
