package com.star.exceptions;

/**
 * @author guoqing_li
 * @create 2020-11-17  11:24:40
 * @description 主键未找到异常
 **/
public class PrimaryKeyNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private Integer code = 500;
  private String message;

  public PrimaryKeyNotFoundException(String message){
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
