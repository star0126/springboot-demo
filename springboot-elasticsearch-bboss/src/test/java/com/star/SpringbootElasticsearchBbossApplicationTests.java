package com.star;

import com.alibaba.fastjson.JSONObject;
import com.star.dto.AddDocsResponse;
import com.star.es.EsManage;
import com.star.es.model.DocModel;
import com.star.es.model.MappingsModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.client.ClientInterface;
import org.frameworkset.elasticsearch.entity.ESIndice;
import org.frameworkset.elasticsearch.entity.IndexField;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author guoqing_li
 * @create 2020-11-13  17:21:40
 * @description 测试类
 **/
@SpringBootTest
public class SpringbootElasticsearchBbossApplicationTests {

  @Autowired
  private BBossESStarter bbossESStarter;

  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  @Autowired
  private EsManage esManage;

  @Test
  void contextLoads(){
  }

  /**
   * es联通测试
   * 获取es中已存在的索引列表并打印
   */
  @Test
  void connectionTest(){
    List<ESIndice> indexes = esManage.getIndexes();
    indexes.forEach(v -> System.out.println(v.getIndex()));
  }

  /**
   * es测试 获取Elasticsearch信息
   */
  @Test
  void getEsInfoTest(){
    ClientInterface restClient = bbossESStarter.getRestClient();
    Map clusterInfo = restClient.getClusterInfo();
    System.out.println("------------------------esInfo------------------------");
    clusterInfo.forEach((k,v)-> System.out.println(k+" "+v));
    String clusterSettings = restClient.getClusterSettings();
    System.out.println("------------------------esSettingsInfo------------------------");
    System.out.println(clusterSettings);
    String elasticsearchVersion = restClient.getElasticsearchVersion();
    System.out.println("------------------------es version------------------------");
    System.out.println(elasticsearchVersion);
  }

  /**
   * es测试 获取es的索引信息
   */
  @Test
  void getMappingTest(){
    String indexName = "people";
    System.out.println("--------------------------mapping--------------------------");
    MappingsModel indexMapping = esManage.getIndexMapping(indexName);
    System.out.println(indexMapping.getMappings().getProperties());
    System.out.println("--------------------------fields--------------------------");
    Set<String> fields = esManage.getIndexFields(indexName);
    fields.forEach(System.out::println);
    System.out.println("--------------------------indexSetting--------------------------");
    String indiceSetting = esManage.getIndexSettings(indexName);
    System.out.println(indiceSetting);
  }

  /**
   * es测试 添加单条文档并且自定义文档ID
   * @throws ParseException
   */
  @Test
  void addDocTest() throws ParseException {
    String indexName = "people";
    Map<String,Object> beanMap = new HashMap<>();
    beanMap.put("about","对待工作认真负责，善于沟通、协调有较强的组织潜力与团队精神；活泼开朗、乐观上进、"
        + "有爱心并善于施教并行；上进心强、勤于学习能不断提高自身的潜力与综合素质。在未来的工"
        + "作中，我将以充沛的精力，刻苦钻研的精神来努力工作，稳定地提高自己的工作潜力，与公司"
        + "同步发展。");
    beanMap.put("age",30);
    beanMap.put("citizenship","中国");
    beanMap.put("height",176);
    beanMap.put("birthday",simpleDateFormat.parse("1990-01-20 12:00:00"));
    beanMap.put("name","杨七");
    beanMap.put("nation","苗族");
    beanMap.put("nickname","小七子");
    beanMap.put("sex","女");
    beanMap.put("weight",58.8);
    String id = UUID.randomUUID().toString().replace("-","");
    beanMap.put("id",id);
    String response = esManage.addDoc(indexName, beanMap, id, null);
    System.out.println(response);
  }

  /**
   * es测试 添加多条文档并且使用自定义文档ID
   * @throws ParseException
   */
  @Test
  void addDocsTest() throws ParseException {
    String indexName = "people";
    Map<String,Object> people1 = new HashMap<String, Object>();
    people1.put("id",UUID.randomUUID().toString().replace("-",""));
    people1.put("about","热情随和，活波开朗，具有进取精神和团队精神，有较强的动手潜力。良好协调沟通潜力，"
        + "适应力强，反应快、用心、灵活，爱创新!提高自己，适应工作的需要。所以我期望找一份与"
        + "自身知识结构相关的工作，具有必须的社会交往潜力，具有优秀的组织和协调潜力。在学习中,"
        + "我注重理论与实践的结合，己具备了相当的实践操作潜力。熟练操作计算机办公软件。很强的"
        + "事业心和职责感使我能够应对任何困难和挑战。");
    people1.put("address","山东省济南市济南历下区");
    people1.put("age",23);
    people1.put("citizenship","中国");
    people1.put("height",182);
    people1.put("birthday",simpleDateFormat.parse("1997-09-12 10:10:05"));
    people1.put("name","王小二");
    people1.put("nation","汉族");
    people1.put("nickname","小二子");
    people1.put("sex","男");
    people1.put("weight",62.4);

    Map<String,Object> people2 = new HashMap<String, Object>();
    people2.put("id",UUID.randomUUID().toString().replace("-",""));
    people2.put("about","同时本，人有较强的沟通潜力及管理潜力，期望能尽快收到面试通知，应对面与您详细交谈,"
        + "凭借多年的丰富阅历与实战经验，我敢保证，我将给您提交一份满意的答卷。");
    people2.put("address","山东省济南市济南长清区");
    people2.put("age",23);
    people2.put("citizenship","中国");
    people2.put("height",175);
    people2.put("birthday",simpleDateFormat.parse("1997-05-20 06:10:05"));
    people2.put("name","王二小");
    people2.put("nation","汉族");
    people2.put("nickname","二仔");
    people2.put("sex","女");
    people2.put("weight",54.4);
    List<Map<String,Object>> list = new ArrayList<>();
    list.add(people1);
    list.add(people2);

    String path = esManage.addMapDocs(indexName, list, "ids", null);
    System.out.println(path);

  }

  @Test
  void delDoc(){
    String indexName = "people";
    String response = esManage.delDoc(indexName,"4f4d97b109384967866e6e20fc7ef9d5");
    System.out.println(response);
  }

  @Test
  void delDocs(){
    String indexName = "people";
    String[] ids = {"4f4d97b109384967866e6e20fc7ef9d5","ffd850db2c46403fa3530b74d9343fd5"};
    String s = esManage.delDocs(indexName, ids);
    System.out.println(s);
  }

  /**
   * es测试 根据文档ID修改文档，非覆盖修改
   */
  @Test
  void updDocByIdTest(){
    String indexName = "people";
    Map<String,Object> map = new HashMap<>();
    map.put("about","本人自学潜力强，善于思考，吃苦耐劳，有良好的沟通潜力，善于与他人相处，富有团队、合作精神，"
        + "热爱运动。但人非完人，自己在某些方面还是有必须的不足，比如知识，社会经验等；但是我相信这些都是能够"
        + "透过自己努力的学习来提高的，我也正朝着这个方向努力！能吃苦耐劳，愿从基层做起；以群众利益为第一原则，遵守公司的规章制度");
    ClientInterface restClient = bbossESStarter.getRestClient();
    String updateDocument = restClient
        .updateDocument(indexName, "9670e4353ae84d1f8f315ca4773a6981", map);
    System.out.println(updateDocument);
  }

  /**
   * es测试 根据文档IDs修改文档集合，非覆盖修改
   */
  @Test
  void updDocsByIdTest(){
    String indexName = "people";
    Map<String,Object> peo1 = new HashMap<>();
    peo1.put("about","本人性格开朗，为人细心，做事一丝不苟， 能吃苦耐劳，工作脚踏实地，有较强的职责心，"
        + "具有团队合作精神，又具有较强的独立工作潜力，思维活跃，能独挡一面。");
    peo1.put("ID","bacc40f9141840209534940adbd3a235");

    Map<String,Object> peo2 = new HashMap<>();
    peo2.put("about","热情随和，活波开朗，具有进取精神和团队精神，有较强的动手潜力。良好协调沟通潜力，"
        + "适应力强，反应快、用心、灵活，爱创新！提高自己，适应工作的需要。所以我期望找一份与"
        + "自身知识结构相关的工作，具有必须的社会交往潜力，具有优秀的组织和协调潜力。在学习中，"
        + "我注重理论与实践的结合，己具备了相当的实践操作潜力。熟练操作计算机办公软件。很强的"
        + "事业心和职责感使我能够应对任何困难和挑战。");
    peo2.put("ID","efe69dc0d50e4a7880420f3cb3a2663b");

    Map<String,Object> peo3 = new HashMap<>();
    peo3.put("about","本人有较强的沟通潜力及管理潜力，期望能尽快收到面试通知，应对面与您详细交谈，"
        + "凭借多年的丰富阅历与实战经验，我敢保证，我将给您提交一份满意的答卷。");
    peo3.put("ID","83bd2cb0b7744191b3eb056945b54039");

    List<Map> list = new ArrayList<>();
    list.add(peo1);
    list.add(peo2);
    list.add(peo3);

    ClientInterface restClient = bbossESStarter.getRestClient();
    String result = restClient.updateDocumentsWithIdKey(indexName, list, "ID");
    AddDocsResponse addDocsResponse = JSONObject.parseObject(result, AddDocsResponse.class);
    if (addDocsResponse.getErrors()){
      System.err.println(result);
    }else {
      System.out.println(result);
    }
  }

  @Test
  void indexIsExist(){
    Object people = esManage.indexIsExist("people");
    System.out.println(people);
  }

  /**
   * es测试 根据文档ID查询单条文档
   */
  @Test
  void findDocById(){
    String indexName = "kettle-data";
    DocModel doc = esManage.getDocById(indexName, "WORKec6286f8151b4d6fb677f456a0969059");
    System.out.println(doc);
    System.out.println("-----------------------------------------------------------------------");
    Map<String, Object> docmap = esManage.getDocSourceById(indexName, "WORKec6286f8151b4d6fb677f456a0969059");
    System.out.println(docmap);
  }

  @Test
  void findDocByMatchAll(){
    String indexName = "kettle-data";
    List<DocModel> matchAllDocs = esManage.getMatchAllDocs(indexName);
    System.out.println(matchAllDocs);
    System.out.println("-----------------------------------------------------------------------");
    List<Map<String, Object>> matchAllDocsSource = esManage.getMatchAllDocsSource(indexName);
    matchAllDocsSource.forEach(System.out::println);
  }

  @Test
  void findDocsByIds(){
    String indexName = "kettle-data";
    String[] ids = new String[]{"WORKec6286f8151b4d6fb677f456a0969059",
        "WORK21b6b566d446475b9fa71c94e6b668fa"};
    List<DocModel> docModels = esManage.mgetDocByIds(indexName, ids);
    docModels.forEach(System.out::println);
    Object o = esManage.mgetDocSourceByIds(indexName, ids);
  }

}
/*
id
about
address
age
birthday
citizenship
height
name
nation
nickname
sex
weight
*/