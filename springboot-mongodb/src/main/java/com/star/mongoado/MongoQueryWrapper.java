//package com.star.mongoado;
//
//import java.util.Collection;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//
///**
// * @author guoqing_li
// * @create 2020-12-10  14:36:59
// * @description mongo简单查询条件包装类
// **/
//public class MongoQueryWrapper {
//
//  private Criteria criteria;
//
//  public MongoQueryWrapper(){
//    this.criteria = new Criteria();
//  }
//
//  private Criteria and(String key){
//    this.criteria = this.criteria.and(key);
//    return this.criteria;
//  }
//
//  protected Criteria getCriteria(){
//    return this.criteria;
//  }
//
//  /**
//   * 等于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper eq(String field,Object value){
//    this.and(field).is(value);
//    return this;
//  }
//
//  /**
//   * 等于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper eq(Object value){
//    this.criteria.is(value);
//    return this;
//  }
//
//  /**
//   * 不等于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper neq(String field,Object value){
//    this.and(field).ne(value);
//    return this;
//  }
//
//  /**
//   * 不等于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper neq(Object value){
//    this.criteria.ne(value);
//    return this;
//  }
//
//  /**
//   * 大于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gt(String field,Object value){
//    this.and(field).gt(value);
//    return this;
//  }
//
//  /**
//   * 大于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gt(Object value){
//    this.criteria.gt(value);
//    return this;
//  }
//
//  /**
//   * 大于等于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gte(String field,Object value){
//    this.and(field).gte(value);
//    return this;
//  }
//
//  /**
//   * 大于等于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gte(Object value){
//    this.criteria.gte(value);
//    return this;
//  }
//
//  /**
//   * 小于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper lt(String field,Object value){
//    this.and(field).lt(value);
//    return this;
//  }
//
//  /**
//   * 小于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper lt(Object value){
//    this.criteria.lt(value);
//    return this;
//  }
//
//  /**
//   * 小于等于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper lte(String field,Object value){
//    this.and(field).lte(value);
//    return this;
//  }
//
//  /**
//   * 小于等于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper lte(Object value){
//    this.criteria.lte(value);
//    return this;
//  }
//
//  /**
//   * 大于beginValue且小于endValue
//   * 注意beginValue应当小于endValue
//   * @param field  mongo字段名
//   * @param beginValue 开始匹配值，应当与字段类型相对应
//   * @param endValue 截至匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gtAndLt(String field,Object beginValue,Object endValue){
//    this.and(field).gt(beginValue).lt(endValue);
//    return this;
//  }
//
//  /**
//   * 大于beginValue且小于等于endValue
//   * 注意beginValue应当小于等于endValue
//   * @param field  mongo字段名
//   * @param beginValue 开始匹配值，应当与字段类型相对应
//   * @param endValue 截至匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gtAndLte(String field,Object beginValue,Object endValue){
//    this.and(field).gt(beginValue).lte(endValue);
//    return this;
//  }
//
//  /**
//   * 大于等于beginValue且小于endValue
//   * 注意beginValue应当小于等于endValue
//   * @param field  mongo字段名
//   * @param beginValue 开始匹配值，应当与字段类型相对应
//   * @param endValue 截至匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gteAndLt(String field,Object beginValue,Object endValue){
//    this.and(field).gte(beginValue).lt(endValue);
//    return this;
//  }
//
//  /**
//   * 大于等于beginValue且小于等于endValue
//   * 注意beginValue应当小于等于endValue
//   * @param field  mongo字段名
//   * @param beginValue 开始匹配值，应当与字段类型相对应
//   * @param endValue 截至匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper gteAndLte(String field,Object beginValue,Object endValue){
//    this.and(field).gte(beginValue).lte(endValue);
//    return this;
//  }
//
//  /**
//   * in 存在于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper in(String field,Object... value){
//    this.and(field).in(value);
//    return this;
//  }
//
//  /**
//   * in 存在于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper in(Object... value){
//    this.criteria.in(value);
//    return this;
//  }
//
//  /**
//   * in 存在于
//   * @param field mongo字段名
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper in(String field, Collection<?> values){
//    this.and(field).in(values);
//    return this;
//  }
//
//  /**
//   * in 存在于
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper in(Collection<?> values){
//    this.criteria.in(values);
//    return this;
//  }
//
//  /**
//   * nin 不存在于
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper nin(String field,Object... value){
//    this.and(field).nin(value);
//    return this;
//  }
//
//  /**
//   * nin 不存在于
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper nin(Object... value){
//    this.criteria.in(value);
//    return this;
//  }
//
//  /**
//   * nin 不存在于
//   * @param field mongo字段名
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper nin(String field,Collection<?> values){
//    this.and(field).nin(values);
//    return this;
//  }
//
//  /**
//   * nin 不存在于
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper nin(Collection<?> values){
//    this.criteria.in(values);
//    return this;
//  }
//
//  /**
//   * all 全存在
//   * 指定的匹配值必须全存在于字段值中，用于匹配mongo中的集合
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper all(String field,Object... value){
//    this.and(field).all(value);
//    return this;
//  }
//
//  /**
//   * all 全存在
//   * 指定的匹配值必须全存在于字段值中，用于匹配mongo中的集合
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper all(Object... value){
//    this.criteria.all(value);
//    return this;
//  }
//
//  /**
//   * all 全存在
//   * 指定的匹配值必须全存在于字段值中，用于匹配mongo中的集合
//   * @param field mongo字段名
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper all(String field,Collection<?> values){
//    this.and(field).all(values);
//    return this;
//  }
//
//  /**
//   * all 全存在
//   * 指定的匹配值必须全存在于字段值中，用于匹配mongo中的集合
//   * @param values 匹配值(集合)，其元素类型应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper all(Collection<?> values){
//    this.criteria.all(values);
//    return this;
//  }
//
//  /**
//   * 正则前缀匹配
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexPrefix(String field,Object value){
//    this.and(field).regex("^".concat(value.toString()));
//    return this;
//  }
//
//  /**
//   * 正则前缀匹配
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexPrefix(Object value){
//    this.criteria.regex("^".concat(value.toString()));
//    return this;
//  }
//
//  /**
//   * 正则后缀匹配
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexSuffix(String field,Object value){
//    this.and(field).regex(value.toString().concat("$"));
//    return this;
//  }
//
//  /**
//   * 正则后缀匹配
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexSuffix(Object value){
//    this.criteria.regex(value.toString().concat("$"));
//    return this;
//  }
//
//  /**
//   * 正则全匹配
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexMatch(String field,Object value){
//    this.and(field).regex("^".concat(value.toString()).concat("$"));
//    return this;
//  }
//
//  /**
//   * 正则全匹配
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regexMatch(Object value){
//    this.criteria.regex("^".concat(value.toString()).concat("$"));
//    return this;
//  }
//
//  /**
//   * 正则匹配
//   * @param field mongo字段名
//   * @param value 匹配值，应当与字段类型相对应
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regex(String field,Object value){
//    this.and(field).regex(value.toString());
//    return this;
//  }
//
//  /**
//   * 正则匹配
//   * @param value 匹配值，应当与字段类型相对应，这里默认使用前一条件的field做匹配
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper regex(Object value){
//    this.criteria.regex(value.toString());
//    return this;
//  }
//
//  /**
//   * or条件
//   * @param criteria 匹配值，应当为Criteria对象，示例如下：
//   *                   Criteria.where("user_name").is("张三"),Criteria.where("user_name").is("李四")
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper or(Criteria... criteria){
//    this.criteria.orOperator(criteria);
//    return this;
//  }
//
//  /**
//   * no or条件
//   * @param criteria 匹配值，应当为Criteria对象，示例如下：
//   *                   Criteria.where("user_name").is("张三"),Criteria.where("user_name").is("李四")
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper nor(Criteria... criteria){
//    this.criteria.norOperator(criteria);
//    return this;
//  }
//
//  /**
//   * and条件
//   * @param criteria 匹配值，应当为Criteria对象，示例如下：
//   *                   Criteria.where("user_name").is("张三"),Criteria.where("user_name").is("李四")
//   * @return MongoQueryWrapper
//   */
//  public MongoQueryWrapper and(Criteria... criteria){
//    this.criteria.andOperator(criteria);
//    return this;
//  }
//
//  /**
//   * query构建
//   * @return query对象
//   */
//  public MongoQuery buildQuery(){
//    return new MongoQuery(Query.query(this.criteria));
//  }
//
//}
