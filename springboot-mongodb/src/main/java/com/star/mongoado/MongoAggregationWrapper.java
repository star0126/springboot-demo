//package com.star.mongoado;
//
//import java.util.ArrayList;
//import java.util.List;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.domain.Sort.Order;
//import org.springframework.data.mongodb.core.aggregation.Aggregation;
//import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
//import org.springframework.data.mongodb.core.aggregation.GroupOperation;
//
///**
// * @author guoqing_li
// * @create 2020-12-11  10:23:07
// * @description mongo聚合查询条件包装类
// **/
//public class MongoAggregationWrapper {
//
//  private List<AggregationOperation> aggregationOperations = new ArrayList<>(6);
//
//  private GroupOperation group;
//
//  /**
//   * 条件过滤
//   * @param queryWrapper 条件封装对象
//   * @return this
//   */
//  public MongoAggregationWrapper match(MongoQueryWrapper queryWrapper){
//    aggregationOperations.add(Aggregation.match(queryWrapper.getCriteria()));
//    return this;
//  }
//
//  /**
//   * 排序
//   * @param orders 排序条件
//   * @return this
//   */
//  public MongoAggregationWrapper order(Order... orders){
//    aggregationOperations.add(Aggregation.sort(Sort.by(orders)));
//    return this;
//  }
//
//  /**
//   * 分页
//   * @param page 第几页
//   * @param size 页大小
//   * @return this
//   */
//  public MongoAggregationWrapper limit(long page,int size){
//    aggregationOperations.add(Aggregation.skip((long)(page-1)*size));
//    aggregationOperations.add(Aggregation.limit(size));
//    return this;
//  }
//
//  /**
//   * 分组统计
//   * @param alias 别名
//   * @param fields 分组字段名
//   * @return this
//   */
//  public MongoAggregationWrapper count(String alias, String... fields){
//    aggregationOperations.add(Aggregation.group(fields).count().as(alias));
//    return this;
//  }
//
//
//}
