//package com.star.mongoado;
//
//import org.springframework.data.domain.Sort;
//import org.springframework.data.domain.Sort.Order;
//import org.springframework.data.mongodb.core.query.Query;
//
///**
// * @author guoqing_li
// * @create 2020-12-10  18:12:04
// * @description mongo普通查询条件
// **/
//public class MongoQuery {
//
//  private Query query;
//
//  public MongoQuery(Query query){
//    this.query=query;
//  }
//
//  /**
//   * mongo普通查询分页
//   * @param page 第几页
//   * @param size 页大小
//   * @return MongoQuery
//   */
//  public MongoQuery limit(long page,int size){
//    this.query.skip((page-1)*size).limit(size);
//    return this;
//  }
//
//  /**
//   * mongo普通查询排序
//   * @param orders 排序对象，示例如下：
//   *                  Order.asc("user_id")
//   *                  Order.desc("user_id")
//   * @return MongoQuery
//   */
//  public MongoQuery order(Order... orders){
//    this.query.with(Sort.by(orders));
//    return this;
//  }
//
//  /**
//   * 构建Query
//   * @return Query
//   */
//  public Query build(){
//    return this.query;
//  }
//
//}
