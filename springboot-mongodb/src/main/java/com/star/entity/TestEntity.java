package com.star.entity;

import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Objects;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-06-17  14:19:03
 * @version: v1.0
 * @description: 用户
 **/
@Document(collection = "people")
public class TestEntity implements Serializable {

    @MongoId
    private String userId;

    @Field("user_name")
    private String name;

    @Field("user_sex")
    private String sex;

    @Field("user_age")
    private Integer age;

    @Field("user_address")
    private String address;

    @Field("user_birthday")
    private Date birthday;

    public TestEntity() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestEntity user = (TestEntity) o;
        return Objects.equals(userId, user.userId) &&
            Objects.equals(name, user.name) &&
            Objects.equals(sex, user.sex) &&
            Objects.equals(age, user.age) &&
            Objects.equals(address, user.address) &&
            Objects.equals(birthday, user.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, name, sex, age, address, birthday);
    }

    @Override
    public String toString() {
        return "User{" +
            "userId='" + userId + '\'' +
            ", name='" + name + '\'' +
            ", sex='" + sex + '\'' +
            ", age=" + age +
            ", address='" + address + '\'' +
            ", birthday=" + birthday +
            '}';
    }
}
