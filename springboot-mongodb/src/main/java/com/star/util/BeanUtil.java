package com.star.util;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * @author guoqing_li
 * @create 2020-11-30  16:16:39
 * @description Bean工具方法
 **/
public class BeanUtil {

  public static <T, R> SerializedLambda doSerializationFunction(SerializationFunction<T, R> func) {
    Object sl = null;
    try {
      // 直接调用writeReplace
      Method writeReplace = func.getClass().getDeclaredMethod("writeReplace");
      writeReplace.setAccessible(true);
      //反射调用
      sl = writeReplace.invoke(func);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      e.printStackTrace();
    }
    return (SerializedLambda) sl;
  }

  /**
   * 根据get/set方法的方法名获取属性名，必须是标准的get/set方法名
   * @param name 方法名
   * @return 属性名
   */
  public static String methodToProperty(String name) {
    if (null==name){
      return null;
    }
    if (name.startsWith("is")) {
      name = name.substring(2);
    } else {
      if (!name.startsWith("get") && !name.startsWith("set")) {
      }
      name = name.substring(3);
    }

    if (name.length() == 1 || name.length() > 1 && !Character.isUpperCase(name.charAt(1))) {
      name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
    }

    return name;
  }

  public static <T, R> String methodToProperty(SerializationFunction<T, R> func) {
    SerializedLambda serializedLambda = doSerializationFunction(func);
    if (null!=serializedLambda){
      String implMethodName = serializedLambda.getImplMethodName();
      return methodToProperty(implMethodName);
    }
    return null;
  }

}
