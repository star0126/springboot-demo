package com.star.util;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @author guoqing_li
 * @create 2020-11-30  16:21:27
 * @description  支持序列化的 Function
 *               来自mybatis-plus的 {com.baomidou.mybatisplus.core.toolkit.support.SFunction}
 **/
public interface SerializationFunction<T, R> extends Function<T, R>, Serializable {

}
