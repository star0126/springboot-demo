package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-06-17  13:44:53
 * @version: v1.0
 * @description: springboot整合mongodb启动器
 **/
@SpringBootApplication
public class SpringbootMongodbApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootMongodbApplication.class,args);
    }
}
