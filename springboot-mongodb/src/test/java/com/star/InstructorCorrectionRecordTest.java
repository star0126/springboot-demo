package com.star;

import com.mongodb.client.result.UpdateResult;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * @author guoqing_li
 * @create 2021-03-10  15:30:41
 * @description 测试
 **/
@SpringBootTest
public class InstructorCorrectionRecordTest {

  @Autowired
  private MongoTemplate mongoTemplate;

  /**
   * 从mongo中查询讲师ID为j5j61ngp的讲师2021-01-09批改的数量
   */
  @Test
  void get(){
    Criteria criteria = new Criteria();
    criteria.and("user_id").is("j5j61ngp").and("correction_time").is("2021-01-09");
    Query query = new Query(criteria);
    Map<String,Object> result = mongoTemplate.findOne(query, Map.class, "instructor_correction_record");
    if (result!=null){
      System.out.println();
      System.out.println("***********************************************");
      result.forEach((k,v)-> System.out.println(k+":"+v));
      System.out.println("***********************************************");
    }
  }


  /**
   * 将讲师ID为j5j61ngp的讲师2021-01-09批改的数量减1，
   * mongo中不存在 user_id='j5j61ngp',correction_time='2021-01-09'的数据，则不做任何操作
   */
  @Test
  void updSub(){
    Criteria criteria = new Criteria();
    criteria.and("user_id").is("j5j61ngp").and("correction_time").is("2021-01-09");
    Query query = new Query(criteria);

    Update update = new Update();
    update.inc("correction_number",-1);

    UpdateResult result = mongoTemplate.updateMulti(query, update, "instructor_correction_record");
    System.out.println(result.toString());
  }

  /**
   * 将讲师ID为j5j61ngp的讲师2021-01-09批改的数量加1，
   * mongo中不存在 user_id='j5j61ngp',correction_time='2021-01-09'的数据的时候添加一条数据
   */
  @Test
  void updAdd(){
    Criteria criteria = new Criteria();
    criteria.and("user_id").is("j5j61ngp").and("correction_time").is("2021-01-09");
    Query query = new Query(criteria);

    Update update = new Update();
    update.inc("correction_number",1);

    UpdateResult result = mongoTemplate.upsert(query, update, "instructor_correction_record");
    System.out.println(result.toString());
  }

}
