package com.star;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.star.entity.TestEntity;
import com.star.util.BeanUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.*;
import org.springframework.data.mongodb.core.query.Update;


/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-05-27  18:08:37
 * @version: v1.0
 * @description:
 **/
@SpringBootTest
public class SpringbootMongodbApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

// +++++++++++++++++++++++++++++++++++++++++++++++ 增 +++++++++++++++++++++++++++++++++++++++++++++++
    /**
     * 单个文档写入，使用实体类的方式
     * 如果需要指定文档ID则需要在对应属性上注解 @Field("_id") 或者 @MongoId
     * 只能用于写入数据，如果向文档中写入_id已存在的文档将会抛出DuplicateKeyException异常
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoInsertOneTest1() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TestEntity people = new TestEntity();
        people.setAddress("河北石家庄");
        people.setAge(22);
        people.setBirthday(simpleDateFormat.parse("1998-01-20 15:38:16"));
        people.setName("赵四");
        people.setUserId(UUID.randomUUID().toString());
        people.setSex("男");
        TestEntity insert = mongoTemplate.insert(people);
        System.out.println(insert);
    }

    /**
     * 单个文档写入，使用map结果集方式
     * 在map中如果需要使用自己指定的id则需要在map中包含有名为 "_id" 的key
     * 只能用于写入数据，如果向文档中写入_id已存在的文档将会抛出DuplicateKeyException异常
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoInsertOneTest2() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String,Object> map = new HashMap<>();
        map.put("_id",UUID.randomUUID().toString());
        map.put("user_name","郝时");
        map.put("user_sex","男");
        map.put("user_age",20);
        map.put("user_address","河南安阳");
        map.put("user_birthday",simpleDateFormat.parse("2000-09-20 13:32:10"));
        Map<String, Object> people = mongoTemplate.insert(map, "people");
        System.out.println(people);
    }

    /**
     * 文档批量写入，使用对象集合的方式，并且在对象中指定了集合名、id字段
     * 只能用于写入数据，如果向文档中写入_id已存在的文档将会抛出DuplicateKeyException异常
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoBatchInsertTest1() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TestEntity people1 = new TestEntity();
        people1.setAddress("山西晋阳");
        people1.setAge(26);
        people1.setBirthday(simpleDateFormat.parse("1994-05-16 06:50:51"));
        people1.setName("拾遗");
        people1.setUserId(UUID.randomUUID().toString());
        people1.setSex("男");

        TestEntity people2 = new TestEntity();
        people2.setAddress("山西河曲");
        people2.setAge(24);
        people2.setBirthday(simpleDateFormat.parse("1996-09-20 11:45:36"));
        people2.setName("拾贰");
        people2.setUserId(UUID.randomUUID().toString());
        people2.setSex("女");
        Collection<TestEntity> testEntities = mongoTemplate
            .insertAll(Arrays.asList(people1, people2));
        System.out.println(Arrays.toString(testEntities.toArray()));
    }

    /**
     * 文档批量写入，使用map集合的方式，并且在map中指定了 "_id"
     * 只能用于写入数据，如果向文档中写入_id已存在的文档将会抛出DuplicateKeyException异常
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoBatchInsertTest2() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String,Object> map1 = new HashMap<>();
        map1.put("_id",UUID.randomUUID().toString());
        map1.put("user_name","拾叁");
        map1.put("user_sex","女");
        map1.put("user_age",20);
        map1.put("user_address","山东烟台");
        map1.put("user_birthday",simpleDateFormat.parse("2000-10-20 14:23:10"));

        Map<String,Object> map2 = new HashMap<>();
        map2.put("_id",UUID.randomUUID().toString());
        map2.put("user_name","拾肆");
        map2.put("user_sex","女");
        map2.put("user_age",20);
        map2.put("user_address","山东章丘");
        map2.put("user_birthday",simpleDateFormat.parse("2000-07-16 19:26:26"));
        Collection<Map<String, Object>> people = mongoTemplate
            .insert(Arrays.asList(map1, map2), "people");
        System.out.println(Arrays.toString(people.toArray()));

    }

    /**
     * 文档存在则覆盖修改(不传值字段则置为null)，文档不存在则写入
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoSaveTest1() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TestEntity people = new TestEntity();
        people.setAge(15);
        people.setBirthday(simpleDateFormat.parse("2005-10-21 12:36:50"));
        people.setName("赵鹫");
        people.setUserId("fbd473b6-b8c5-42ac-9781-2262742161bd");
        people.setSex("男");
        TestEntity save = mongoTemplate.save(people);
        System.out.println(save);
    }

// +++++++++++++++++++++++++++++++++++++++++++++++ 删 +++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 根据条件删除
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用remove的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoRemoveTest1(){
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).is(20);
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName))).is("20岁");
        Query query = new Query(criteria);
        DeleteResult remove = mongoTemplate.remove(query,TestEntity.class);
        System.out.println(remove);
    }

    /**
     * 根据条件删除
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用remove的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoRemoveTest2(){
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getUserId))).is("097cd3d9-875d-4e8c-b4ba-eb691e7c9adc");
        Query query = new Query(criteria);
        DeleteResult remove = mongoTemplate.remove(query,TestEntity.class);
        System.out.println(remove);
    }

// +++++++++++++++++++++++++++++++++++++++++++++++ 改 +++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 覆盖修改 save 方法参见增方法
     */

    /**
     * 修改一条数据
     * 修改年龄是20的数据中的一条，将其姓名置为 "first-20"
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoUpdFirstTest1(){
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).is(20);
        Query query = new Query(criteria);
        Update update = new Update();
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName)),"first-20");
        UpdateResult people = mongoTemplate.updateFirst(query, update, TestEntity.class);
        System.out.println(people);
    }

    /**
     * 修改一条数据
     * 修改年龄是20的数据中的一条，将其姓名置为 "20岁第一"
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoUpdFirstTest2(){
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getUserId))).is("f64473ed-856b-421a-a2cf-80f0dc7228e9");
        Query query = new Query(criteria);
        Update update = new Update();
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName)),"20岁第一");
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, TestEntity.class);
        System.out.println(updateResult);
    }

    /**
     * 批量修改数据
     * 批量修改年龄是20的数据，将其姓名置为 "20岁" ，将其生日置为 "2000-10-10 10:10:10"
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoUpdMultiTest1() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).is(20);
        Query query = new Query(criteria);
        Update update = new Update();
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName)),"20岁");
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)),simpleDateFormat.parse("2000-10-10 10:10:10"));
        UpdateResult people = mongoTemplate.updateMulti(query, update, TestEntity.class);
        System.out.println(people);
    }

    /**
     * 存在则修改，不存在则新增
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoUpsertTest(){
        Criteria criteria = new Criteria();
        criteria.and("_id").is("f64473ed-856b-421a-a2cf-80f0dc7228e9");
        Query query = new Query(criteria);
        Update update = new Update();
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName)),"小诗");
        update.set(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getSex)),"女");
        UpdateResult people = mongoTemplate.upsert(query, update, TestEntity.class);
        System.out.println(people);
    }



// +++++++++++++++++++++++++++++++++++++++++++++++ 查 +++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 查询全部
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindAll1(){
        List<TestEntity> all = mongoTemplate.findAll(TestEntity.class);
        if (null!=all){
            all.forEach(e -> System.out.println(e.toString()));
        }
    }

    /**
     * 查询全部
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindAll2(){
        List<Map> people = mongoTemplate.findAll(Map.class, "people");
        if (null!=people){
            people.forEach(e -> System.out.println(e.toString()));
        }
    }

    /**
     * 查询全部
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindAll3(){
        List<Map> people = mongoTemplate.findAll(Map.class, "people");
        if (null!=people){
            people.forEach(e -> System.out.println(e.toString()));
        }
    }

    /**
     * 根据id查询文档
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindById1(){
        TestEntity byId = mongoTemplate
            .findById("ffda006c-c7ac-46f6-bf60-70ddd49f2e99", TestEntity.class);
        System.out.println(byId);
    }

    /**
     * 根据id查询文档
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindById2(){
        Map people = mongoTemplate
            .findById("dbedd24f-3ef0-4c79-af23-ee2ae77bd1d6", Map.class, "people");
        System.out.println(people);
    }

    /**
     * 根据条件查询一条文档，如果查到多条只返回一条，如果查不到数据则返回null
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoFindOneTest1() throws ParseException {
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAddress))).is("山东章丘");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))).is(simpleDateFormat.parse("2000-07-16 19:26:26"));
        Query query = new Query(criteria);
        TestEntity one = mongoTemplate.findOne(query, TestEntity.class);
        System.out.println(one);
    }

    /**
     * 根据条件查询一条文档，如果查到多条只返回一条，如果查不到数据则返回null
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoFindOneTest2() throws ParseException {
        Criteria criteria = new Criteria();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getUserId))).is("f64473ed-856b-421a-a2cf-80f0dc7228e9");
        Query query = new Query(criteria);
        TestEntity people = mongoTemplate.findOne(query, TestEntity.class);
        System.out.println(people);
    }

    /**
     * 条件+正则模糊匹配+排序查询
     * 查询性别是女，年龄大于等于20，姓名前缀为拾，地名中带有山，userId后缀为f且忽略大小写的文档，并按照年龄降序排序
     * 如果需要全匹配则需要将字符置于^和$之间，例如：
     *      criteria.and("name").regex("^拾一$");
     * 便是查询 name=拾一 的人
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindTest1(){
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getSex))).is("女");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).gte(20);
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName))).regex("^拾");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAddress))).regex("山");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getUserId))).regex("f$","i");
        Query query = new Query(criteria).with(Sort.by(Direction.DESC,Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))));
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * 条件+排序查询
     * 查询性别为男的，生日在1995-2000年之间，年龄大于22小于等于25的文档，并按照年龄做升序排序
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindTest2() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getSex))).is("男");
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)))
            .gte(simpleDateFormat.parse("1995-01-01 00:00:00"))
            .lt(simpleDateFormat.parse("2000-01-01 00:00:00"));
//        Criteria[] criterias = {
//            // 生日大于等于1995-01-01 00:00:00
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)))
//                .gte(simpleDateFormat.parse("1995-01-01 00:00:00")),
//            // 生日小于2000-01-01 00:00:00
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)))
//                .lt(simpleDateFormat.parse("2000-01-01 00:00:00")),
//            // 年龄大于22
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge)))
//                .gt(22),
//            // 年龄小于等于25
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge)))
//                .lte(25)
//        };
//        List<Criteria> list = new ArrayList<>();
//        list.add(
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)))
//                .gte(simpleDateFormat.parse("1995-01-01 00:00:00"))
//        );
//        list.add(
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)))
//                .lt(simpleDateFormat.parse("2000-01-01 00:00:00"))
//        );
//        list.add(
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge)))
//                .gt(22)
//        );
//        list.add(
//            Criteria
//                .where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge)))
//                .lte(25)
//        );
//        criteria.andOperator(list.toArray(new Criteria[0]));
//        criteria.andOperator(
//            // 生日大于等于1995-01-01 00:00:00
//            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))).gte(simpleDateFormat.parse("1995-01-01 00:00:00")),
//            // 生日小于2000-01-01 00:00:00
//            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))).lt(simpleDateFormat.parse("2000-01-01 00:00:00")),
//            // 年龄大于22
//            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).gt(22),
//            // 年龄小于等于25
//            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).lte(25)
//        );
        Query query = new Query(criteria).with(Sort.by(Direction.ASC,Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))));
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * 条件+排序+返回指定字段
     * 查询性别为男的，生日在1995-2000年之间，年龄大于22小于等于25的文档，并按照生日做降序排序，且只返回id、姓名、生日字段
     * 注意：查询结果中返回或者不返回文档id字段时都要使用_id，否则可能会不生效；建议返回map结果集
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     * @throws ParseException
     */
    @Test
    void mongoFindTest3() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Criteria criteria = new Criteria();
        criteria.and(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getSex))).is("男");
        criteria.andOperator(
            // 生日大于等于1995-01-01 00:00:00
            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))).gte(simpleDateFormat.parse("1995-01-01 00:00:00")),
            // 生日小于2000-01-01 00:00:00
            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))).lt(simpleDateFormat.parse("2000-01-01 00:00:00")),
            // 年龄大于22
            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).gt(22),
            // 年龄小于等于25
            Criteria.where(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getAge))).lte(25)
        );

        Query query = new Query(criteria);
        query.with(Sort.by(Direction.DESC,Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))));
        query.fields().include(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getName)));
        query.fields().include(Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday)));
        query.fields().include("_id");
        List<TestEntity> list = mongoTemplate.find(query, TestEntity.class);
        System.out.println(list.toString());
    }


    /**
     * 排序+分页
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoFindTest4() {
        int page = 2;
        int size = 5;
        Query query = new Query();

        query.with(Sort.by(new Order(Direction.DESC,"user_age"),new Order(Direction.ASC,"user_sex")));
//        query.with(Sort.by(Direction.DESC,Objects.requireNonNull(BeanUtil.methodToProperty(TestEntity::getBirthday))));
        query.skip((page-1)*size).limit(size);
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * in nin查询
     * 查询年龄是20、15，且性别不是男的文档数据
     */
    @Test
    void mongoFindTest5() {
        Criteria criteria = new Criteria();
        criteria.and("user_age").in(20,15).and("user_sex").nin("男");
        Query query = new Query(criteria);
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * 不等于查询
     */
    @Test
    void mongoFindTest6() {
        Criteria criteria = new Criteria();
        criteria.and("user_age").ne(20);
        Query query = new Query(criteria);
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * or条件查询
     */
    @Test
    void mongoFindTest7() {
        Criteria criteria = new Criteria();
        List<Criteria> or = new ArrayList<>();
        or.add(Criteria.where("user_age").is(20));
        or.add(Criteria.where("user_address").is("山西河曲"));
        criteria.orOperator(or.toArray(new Criteria[0]));
        Query query = new Query(criteria);
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        testEntities.forEach(e -> System.out.println(e.toString()));
    }

    /**
     * 数据总量的查询
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoCountTest1(){
        long people = mongoTemplate.count(new Query(), "people");
        System.out.println(people);
    }

    /**
     * 聚合查询
     * 查询 年龄大于20，按照地名分组统计，按照统计结果排序，只获取地名和统计数，且分页返回
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoAggregationTest1(){
        int page = 1;
        int size = 25;
        Criteria criteria = new Criteria();
        criteria.and("user_age").gt(20);
        Aggregation aggregation = Aggregation.newAggregation(
            // 条件过滤
            Aggregation.match(criteria),
            // 按照地名分组
            Aggregation.group("user_address").count().as("num"),
            // 字段映射修改
            Aggregation.project("num").andExclude("_id")
                .and("_id").as("user_address"),
            // 排序
            Aggregation.sort(Direction.DESC,"num"),
            // 分页
            Aggregation.skip((long)(page-1)*size),
            Aggregation.limit(size)
        );
        AggregationResults<Map> people = mongoTemplate.aggregate(aggregation, "people", Map.class);
        List<Map> mappedResults = people.getMappedResults();
        mappedResults.forEach(m -> System.out.println(m.toString()));

    }


    /**
     * 求age字段的平均值与和
     *
     * 如果想要属性名的方式生效，则在使用mongoTemplate调用方法的时候必须要传入映射实体的Class对象，
     * 否则条件值必须指定为mongo字段名
     */
    @Test
    void mongoAggregationTest2(){
        Aggregation aggregation = Aggregation.newAggregation(
            // 求和以及平均值
            Aggregation.group().sum("user_age").as("sum")
                .avg("user_age").as("avg"),
            // 字段映射修改
            Aggregation.project("sum","avg").andExclude("_id")
        );
        AggregationResults<Map> people = mongoTemplate
            .aggregate(aggregation, "people", Map.class);
        Map uniqueMappedResult = people.getUniqueMappedResult();
        System.out.println(uniqueMappedResult);
    }

    @Test
    void testFindAndModify(){
//        Criteria criteria = Criteria.where("course_id").is("course0001")
//            .and("course_channel_id").is("stage002")
//            .and("lock_time").lt(System.currentTimeMillis() - 330000L);
//        Query query = new Query(criteria).with(Sort.by(Sort.Order.asc("job_submit_time"))).skip(0).limit(1);
//        long time = System.currentTimeMillis();
//        Update update = new Update();
//        update.set("lock_time",time);
//
//        Map result = mongoTemplate.findAndModify(query, update, Map.class, "vip_send_jobcompletion");
//        System.out.println(result);

        int page = 1;
        int size = 100;
        Aggregation aggregation = Aggregation.newAggregation(
            // 按照地名分组
            Aggregation.group("course_id").count().as("num"),
            // 排序
            Aggregation.sort(Direction.DESC,"num"),
            // 分页
            Aggregation.skip((long)(page-1)*size),
            Aggregation.limit(size)
        );
        AggregationResults<Map> people = mongoTemplate.aggregate(aggregation, "test_send_jobcompletion", Map.class);
        List<Map> mappedResults = people.getMappedResults();
        mappedResults.forEach(m -> System.out.println(m.toString()));


    }

    @Test
    void testExists(){
        List<String> courseIds = new ArrayList<>();

        Aggregation aggregation = Aggregation.newAggregation(
            // 条件过滤
            Aggregation.match(Criteria.where("course_id").in(courseIds).and("lock_time").lte(System.currentTimeMillis() - 330000)),
            // 按照地名分组
            Aggregation.group("course_id").first("course_id").as("course_id"),
            Aggregation.project("course_id").andExclude("_id")
        );
        AggregationResults<Object> vip_send_jobcompletion = mongoTemplate
            .aggregate(aggregation, "vip_send_jobcompletion", Object.class);
        List<Object> mappedResults = vip_send_jobcompletion.getMappedResults();
        mappedResults.forEach(m -> System.out.println(m.toString()));

    }

    @Test
    void testFindOne(){
        Aggregation aggregation = Aggregation.newAggregation(
            // 限定查询范围
            Aggregation.match(
                Criteria.where("course_id").is("ijmiqxxv")
                    .and("lock_time").lte(System.currentTimeMillis() - 330000L)
            ),
            // 根据courseChannelId分组并且取courseChannelId、channelStartTime字段的值
        /*注意这里的courseChannelId、courseChannelId、channelStartTime必须是VipSendJobcompletion的属性名，
          提取channelStartTime是为了进行排序*/
            Aggregation.group("courseChannelId")
                .first("courseChannelId").as("courseChannelId")
                .first("channelStartTime").as("channelStartTime"),
            // 根据channelStartTime进行排序
            Aggregation.sort(Direction.ASC,"channelStartTime"),
            // 只返回courseChannelId
            Aggregation.project("courseChannelId").andExclude("_id")
        );
        AggregationResults<Map> aggregate = mongoTemplate.aggregate(aggregation, "vip_send_jobcompletion", Map.class);
        String courseChannelIds = aggregate.getMappedResults().stream().map(m -> {
            Object id = m.get("courseChannelId");
            if (id != null) return id.toString();
            return null;
        }).filter(Objects::nonNull).collect(Collectors.joining(","));
        System.out.println(courseChannelIds);
    }

}
