//package com.star;
//
//import com.star.entity.TestEntity;
//import com.star.mongoado.MongoQuery;
//import com.star.mongoado.MongoQueryWrapper;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.List;
//import java.util.Map;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort.Order;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//
///**
// * @author guoqing_li
// * @create 2020-12-10  15:14:20
// * @description mongo操作测试类
// **/
//@SpringBootTest
//public class MongoadoTests {
//
//  @Autowired
//  private MongoTemplate mongoTemplate;
//
//// -------------------------------- 查 --------------------------------
//
//  /**
//   * 测试查询性别男、年龄大于等于20小于等于26的文档数据
//   */
//  @Test
//  void mongoFindTest1() {
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery query = mongoQueryWrapper.eq("user_sex", "男")
//                                    .gte("user_age", 20)
//                                    .lte(26)
//                                    .buildQuery();
//
//    List<TestEntity> people = mongoTemplate.find(query.build(), TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//  /**
//   * 测试查询性别男、年龄大于等于20小于等于26的文档数据，使用第二种方式
//   */
//  @Test
//  void mongoFindTest2() {
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery query = mongoQueryWrapper.eq("user_sex", "男")
//        .gteAndLte("user_age",20,26)
//        .buildQuery();
//    List<TestEntity> people = mongoTemplate.find(query.build(), TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//  /**
//   * 测试查询性别不为男，年龄大于等于20的文档数据
//   */
//  @Test
//  void mongoFindTest3() {
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery query = mongoQueryWrapper.neq("user_sex", "男")
//        .gte("user_age", 20)
//        .buildQuery();
//
//    List<TestEntity> people = mongoTemplate.find(query.build(), TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//  /**
//   * 测试查询id为指定数据的文档
//   */
//  @Test
//  void mongoFindTest4() {
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery query = mongoQueryWrapper.eq("_id", "6abe2758-3a32-4ab4-a896-01a7b9312d57")
//        .buildQuery();
//    List<TestEntity> people = mongoTemplate.find(query.build(), TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//  /**
//   * 测试根据时间范围查询
//   * @throws ParseException
//   */
//  @Test
//  void mongoFindTest5() throws ParseException {
//
//    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery query = mongoQueryWrapper
//        .gtAndLte("user_birthday", simpleDateFormat.parse("1999-01-01 00:00:00"),
//            simpleDateFormat.parse("2005-01-01 00:00:00")).buildQuery();
//    Query build = query.limit(1, 2).build();
//    List<Map> list = mongoTemplate.find(build, Map.class, "people");
//    list.forEach(System.out::println);
//  }
//
//  /**
//   * 测试排序+分页查询
//   */
//  @Test
//  void mongoFindTest6(){
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery mongoQuery = mongoQueryWrapper.buildQuery();
//    Query query = mongoQuery
//        .order(Order.asc("user_age"), Order.asc("user_birthday")).limit(1, 5)
//        .build();
//    List<TestEntity> people = mongoTemplate.find(query, TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//  /**
//   * 测试or条件查询
//   */
//  @Test
//  void mongoFindTest7(){
//    MongoQueryWrapper mongoQueryWrapper = new MongoQueryWrapper();
//    MongoQuery mongoQuery = mongoQueryWrapper.eq("user_sex", "女")
//        .or(Criteria.where("user_age").is(20), Criteria.where("user_age").is(15)).buildQuery();
//    Query query = mongoQuery.order(Order.desc("user_age")).build();
//    List<TestEntity> people = mongoTemplate.find(query, TestEntity.class, "people");
//    people.forEach(System.out::println);
//  }
//
//}
