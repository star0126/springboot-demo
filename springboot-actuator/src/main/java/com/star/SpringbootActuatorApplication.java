package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  18:13:58
 * @description springboot启动器
 **/
@SpringBootApplication
public class SpringbootActuatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootActuatorApplication.class,args);
    }
}
