#!/usr/bin/env bash

mvn clean install package -Dmaven.test.skip=true
echo 'springboot-actuator 项目打包完成'

docker rmi springboot-actuator:latest

docker build -t springboot-actuator:latest .
echo 'springboot-actuator 镜像创建完成'

docker run -d --name "springboot-actuator" -e JAVA_OPTS='-Xmx500M -Xms300M -XX:MaxMetaspaceSize=192M -XX:MetaspaceSize=192M' springboot-actuator:latest
echo 'springboot-actuator 容器启动完成'