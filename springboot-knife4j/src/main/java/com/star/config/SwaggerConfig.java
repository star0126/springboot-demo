package com.star.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author guoqing_li
 * @create 2020-09-24  11:15:23
 * @description Swagger2 配置文件
 * 访问接口文档的地址为 http://localhost:8080/doc.html
 **/
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //分组名称
                .groupName("功能区")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.star.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("springboot-knife4j")
                .description("springboot-knife4j by 李国庆")
                .termsOfServiceUrl("https://www.baidu.com/")
                .contact(new Contact("李国庆", "", ""))
                .version("1.0")
                .build();
    }

}
