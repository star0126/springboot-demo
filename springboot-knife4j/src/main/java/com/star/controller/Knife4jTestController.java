package com.star.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author guoqing_li
 * @create 2020-09-24  11:07:05
 * @description Knife4j测试控制器
 **/
@Api(tags = "Knife4j文档测试控制器")
@Validated
@ApiSort(1)
@RestController
@RequestMapping("/knife4j/test")
public class Knife4jTestController {

    @ApiOperation(value = "测试接口")
    @ApiOperationSupport(order = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "用户ID", name = "x-currUserId", required = true,paramType = "header"),
            @ApiImplicitParam(value = "作业ID",name = "jobcompletionid", required = true,paramType = "path"),
            @ApiImplicitParam(value = "推荐类型",name = "recommendType",
                    allowableValues="PROGRESS,EXCELLENT,STRIVE,BOARDPAINTING,CREATIVE",
                    required = true,paramType = "query"),
            @ApiImplicitParam(value = "当前页",name = "page", paramType = "query"),
            @ApiImplicitParam(value = "页大小",name = "size", paramType = "query")
    })
    @PostMapping(value = "/{jobcompletionid}",headers = "x-currUserId")
    public ResponseEntity<?> test(
            @RequestHeader(value = "x-currUserId") String userId ,
            @PathVariable(value = "jobcompletionid") String jobId,
            @RequestParam String recommendType,
            @RequestParam(required = false,defaultValue = "1") Integer page,
            @RequestParam(required = false,defaultValue = "5") Integer size){

        return ResponseEntity.ok().body(userId+"--"+jobId+"--"+recommendType+"--"+page+"--"+size);
    }

}
