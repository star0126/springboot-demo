package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li
 * @create 2020-09-24  10:58:42
 * @description springboot启动类
 **/
@SpringBootApplication
public class SpringbootKnife4jApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootKnife4jApplication.class,args);
    }
}
