package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dapeng-liguoqing
 * @create 2020-07-23  09:59:06
 * @description springboot启动类
 **/
@SpringBootApplication
public class SpringbootJdbctemplateApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootJdbctemplateApplication.class,args);
    }
}
