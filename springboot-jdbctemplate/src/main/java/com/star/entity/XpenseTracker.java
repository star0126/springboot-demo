package com.star.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 顾客消费记录表
 * </p>
 *
 * @author guoqing-li
 * @since 2020-07-07
 */
public class XpenseTracker implements Serializable {

    private static final long serialVersionUID=1L;

    private String id;

    private String userId;

    private String cardId;

    private String cardItemId;

    private String cardItemType;

    private BigDecimal consumeMoney;

    private Integer consumeFrequency;

    private LocalDateTime consumeTime;

    private String consumeType;

    private BigDecimal rebate;

    private String consumeInfo;

    public XpenseTracker() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardItemId() {
        return cardItemId;
    }

    public void setCardItemId(String cardItemId) {
        this.cardItemId = cardItemId;
    }

    public String getCardItemType() {
        return cardItemType;
    }

    public void setCardItemType(String cardItemType) {
        this.cardItemType = cardItemType;
    }

    public BigDecimal getConsumeMoney() {
        return consumeMoney;
    }

    public void setConsumeMoney(BigDecimal consumeMoney) {
        this.consumeMoney = consumeMoney;
    }

    public Integer getConsumeFrequency() {
        return consumeFrequency;
    }

    public void setConsumeFrequency(Integer consumeFrequency) {
        this.consumeFrequency = consumeFrequency;
    }

    public LocalDateTime getConsumeTime() {
        return consumeTime;
    }

    public void setConsumeTime(LocalDateTime consumeTime) {
        this.consumeTime = consumeTime;
    }

    public String getConsumeType() {
        return consumeType;
    }

    public void setConsumeType(String consumeType) {
        this.consumeType = consumeType;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public String getConsumeInfo() {
        return consumeInfo;
    }

    public void setConsumeInfo(String consumeInfo) {
        this.consumeInfo = consumeInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XpenseTracker that = (XpenseTracker) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(cardId, that.cardId) &&
                Objects.equals(cardItemId, that.cardItemId) &&
                Objects.equals(cardItemType, that.cardItemType) &&
                Objects.equals(consumeMoney, that.consumeMoney) &&
                Objects.equals(consumeFrequency, that.consumeFrequency) &&
                Objects.equals(consumeTime, that.consumeTime) &&
                Objects.equals(consumeType, that.consumeType) &&
                Objects.equals(rebate, that.rebate) &&
                Objects.equals(consumeInfo, that.consumeInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, cardId, cardItemId, cardItemType, consumeMoney, consumeFrequency, consumeTime, consumeType, rebate, consumeInfo);
    }

    @Override
    public String toString() {
        return "XpenseTracker{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardItemId='" + cardItemId + '\'' +
                ", cardItemType='" + cardItemType + '\'' +
                ", consumeMoney=" + consumeMoney +
                ", consumeFrequency=" + consumeFrequency +
                ", consumeTime=" + consumeTime +
                ", consumeType='" + consumeType + '\'' +
                ", rebate=" + rebate +
                ", consumeInfo='" + consumeInfo + '\'' +
                '}';
    }
}
