package com.star.service;

import com.star.entity.XpenseTracker;

import java.util.List;
import java.util.Map;

/**
 * @author dapeng-liguoqing
 * @create 2020-07-23  10:17:02
 * @description jdbcTemplate服务管理类
 **/
public interface JdbcTemplateService {

    /**
     * 写入数据库
     */
    int insert(XpenseTracker xpenseTracker);

    /**
     * 写入数据库并返回主键
     * 仅仅适用于主键自增的时候，如果不是自增主键则返回主键不管用
     */
    Object insertObtainPri(String index);

    /**
     * 批量写入数据库
     */
    int insertBatch(List<XpenseTracker> xpenseTrackers);

    /**
     * 分段批处理写入数据库
     */
    int insertMultipleBatches(List<XpenseTracker> xpenseTrackers,int batchSize);

    /**
     * 删除数据
     */
    int delete(String pri);

    /**
     * 批量删除数据
     */
    int deleteBatch(List<String> pris);

    /**
     * 查询数据库
     */
    Object select(String pri);

    /**
     * 查询并返回map
     */
    Map<String,Object> selectObtainMap(String pri);

    /**
     * 查询返回list集合
     */
    List<Object> selectObtainList(List<String> pris);

    /**
     * 批量查询
     */
    List<Object> selectBatch(List<String> pris);


}
