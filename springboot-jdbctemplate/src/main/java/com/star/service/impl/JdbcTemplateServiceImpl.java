package com.star.service.impl;

import com.star.entity.XpenseTracker;
import com.star.service.JdbcTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author dapeng-liguoqing
 * @create 2020-07-23  10:18:34
 * @description jdbcTemplate服务实现类
 **/
@Service
public class JdbcTemplateServiceImpl implements JdbcTemplateService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int insert(XpenseTracker xpenseTracker) {
        String sql = "insert into xpense_tracker(`id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`," +
                "`consume_money`,`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info`) values " +
                "(?,?,?,?,?,?,?,?,?,?,?)";


        PreparedStatementSetter preparedStatementSetter = ps -> {
            ps.setObject(1, UUID.randomUUID().toString());
            ps.setObject(2,xpenseTracker.getUserId());
            ps.setObject(3,xpenseTracker.getCardId());
            ps.setObject(4,xpenseTracker.getCardItemId());
            ps.setObject(5,xpenseTracker.getCardItemType());
            ps.setObject(6,xpenseTracker.getConsumeMoney());
            ps.setObject(7,xpenseTracker.getConsumeFrequency());
            ps.setObject(8,xpenseTracker.getConsumeTime());
            ps.setObject(9,xpenseTracker.getRebate());
            ps.setObject(10,xpenseTracker.getConsumeType());
            ps.setObject(11,xpenseTracker.getConsumeInfo());
        };
        return jdbcTemplate.update(sql,preparedStatementSetter);
    }

    @Override
    public Object insertObtainPri(String index) {
        String sql ="insert into sys_dict_index(`dict_index`) values (?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        PreparedStatementCreator preparedStatementCreator = con -> {
            //定义主键，但是只适用于主键自增的情况
            PreparedStatement preparedStatement = con.prepareStatement(sql, new String[]{"id"});
            preparedStatement.setObject(1, index);
            return preparedStatement;
        };
        jdbcTemplate.update(preparedStatementCreator, keyHolder);
        return keyHolder.getKey();
    }

    @Override
    public int insertBatch(List<XpenseTracker> xpenseTrackers) {
        List<XpenseTracker> newXpenseTrackers = xpenseTrackers.stream().filter(Objects::nonNull).collect(Collectors.toList());
        if (newXpenseTrackers.size()==0) return 0;

        String sql = "insert into xpense_tracker(`id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`," +
                "`consume_money`,`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info`) values " +
                "(?,?,?,?,?,?,?,?,?,?,?)";

        BatchPreparedStatementSetter batchPreparedStatementSetter = new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                XpenseTracker xpenseTracker = newXpenseTrackers.get(i);
                ps.setObject(1, UUID.randomUUID().toString());
                ps.setObject(2,xpenseTracker.getUserId());
                ps.setObject(3,xpenseTracker.getCardId());
                ps.setObject(4,xpenseTracker.getCardItemId());
                ps.setObject(5,xpenseTracker.getCardItemType());
                ps.setObject(6,xpenseTracker.getConsumeMoney());
                ps.setObject(7,xpenseTracker.getConsumeFrequency());
                ps.setObject(8,xpenseTracker.getConsumeTime());
                ps.setObject(9,xpenseTracker.getRebate());
                ps.setObject(10,xpenseTracker.getConsumeType());
                ps.setObject(11,xpenseTracker.getConsumeInfo());
            }

            @Override
            public int getBatchSize() {
                return newXpenseTrackers.size();
            }
        };

        int[] ints = jdbcTemplate.batchUpdate(sql, batchPreparedStatementSetter);
        return ints.length;
    }

    @Override
    public int insertMultipleBatches(List<XpenseTracker> xpenseTrackers, int batchSize) {
        List<XpenseTracker> newXpenseTrackers = xpenseTrackers.stream().filter(Objects::nonNull).collect(Collectors.toList());
        if (newXpenseTrackers.size()==0) return 0;

        String sql = "insert into xpense_tracker(`id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`," +
                "`consume_money`,`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info`) values " +
                "(?,?,?,?,?,?,?,?,?,?,?)";

        ParameterizedPreparedStatementSetter<XpenseTracker> preparedStatementSetter = (ps,argument)->{
            ps.setObject(1, UUID.randomUUID().toString());
            ps.setObject(2,argument.getUserId());
            ps.setObject(3,argument.getCardId());
            ps.setObject(4,argument.getCardItemId());
            ps.setObject(5,argument.getCardItemType());
            ps.setObject(6,argument.getConsumeMoney());
            ps.setObject(7,argument.getConsumeFrequency());
            ps.setObject(8,argument.getConsumeTime());
            ps.setObject(9,argument.getRebate());
            ps.setObject(10,argument.getConsumeType());
            ps.setObject(11,argument.getConsumeInfo());
        };

        int[][] ints = jdbcTemplate.batchUpdate(sql, newXpenseTrackers, batchSize, preparedStatementSetter);
        int count = 0;
        for (int[] intArr:ints) count += intArr.length;
        return count;
    }

    @Override
    public int delete(String pri) {
        String sql = "delete from xpense_tracker where id=?";

        PreparedStatementSetter preparedStatementSetter = (ps)->{
            ps.setObject(1,pri);
        };

        int delete = jdbcTemplate.update(sql, preparedStatementSetter);
        return delete;
    }

    @Override
    public int deleteBatch(List<String> pris) {

        StringBuffer stringBuffer = new StringBuffer("delete from xpense_tracker ");
        stringBuffer.append("where id in (");

        for (int i=0; i<pris.size(); i++){
            stringBuffer.append("?");
            if (i+1!=pris.size()) stringBuffer.append(",");
        }
        stringBuffer.append(")");
        String sql = stringBuffer.toString();

        PreparedStatementSetter preparedStatementSetter = (ps)->{
            for (int i=0; i<pris.size(); i++) ps.setObject(i+1,pris.get(i));
        };

        int delete = jdbcTemplate.update(sql, preparedStatementSetter);
        return delete;
    }

    @Override
    public Object select(String pri) {
        String sql = "select `id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`,`consume_money`," +
                "`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info` from xpense_tracker where id in (?)";
        PreparedStatementSetter preparedStatementSetter = (ps)->{
          ps.setObject(1,pri);
        };
        ResultSetExtractor<XpenseTracker> resultSetExtractor = (rs)->{
            XpenseTracker xpenseTracker = new XpenseTracker();
            rs.next();
            xpenseTracker.setId(rs.getString("id"));
            xpenseTracker.setUserId(rs.getString("user_id"));
            xpenseTracker.setCardId(rs.getString("card_id"));
            xpenseTracker.setCardItemId(rs.getString("card_item_id"));
            xpenseTracker.setConsumeMoney(rs.getBigDecimal("consume_money"));
            xpenseTracker.setConsumeFrequency(rs.getInt("consume_frequency"));
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            xpenseTracker.setConsumeTime(LocalDateTime.parse(rs.getString("consume_time"),dateTimeFormatter));
            xpenseTracker.setRebate(rs.getBigDecimal("rebate"));
            xpenseTracker.setConsumeType(rs.getString("consume_type"));
            xpenseTracker.setConsumeInfo(rs.getString("consume_info"));
            return xpenseTracker;
        };

        XpenseTracker xpenseTracker = jdbcTemplate.query(sql, preparedStatementSetter, resultSetExtractor);

        return xpenseTracker;
    }

    @Override
    public Map<String, Object> selectObtainMap(String pri) {

        String sql = "select `id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`,`consume_money`," +
                "`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info` from xpense_tracker where id=(?)";

        PreparedStatementCreator preparedStatementCreator = con -> {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            return preparedStatement;
        };

        PreparedStatementSetter preparedStatementSetter = (ps)->{
            ps.setObject(1,pri);
        };

        ResultSetExtractor<Map<String, Object>> resultSetExtractor = (rs)->{
            Map<String,Object> map = new HashMap<>();
            rs.next();
            map.put("id",rs.getString("id"));
            map.put("user_id",rs.getString("user_id"));
            map.put("card_id",rs.getString("card_id"));
            map.put("card_item_id",rs.getString("card_item_id"));
            map.put("consume_money",rs.getBigDecimal("consume_money"));
            map.put("consume_frequency",rs.getInt("consume_frequency"));
            map.put("consume_time",rs.getString("consume_time"));
            map.put("rebate",rs.getBigDecimal("rebate"));
            map.put("consume_type",rs.getString("consume_type"));
            map.put("consume_info",rs.getString("consume_info"));
            return map;
        };

        Map<String, Object> query = jdbcTemplate.query(preparedStatementCreator, preparedStatementSetter, resultSetExtractor);
//        Map<String, Object> query = (Map<String, Object>)jdbcTemplate.query(sql, preparedStatementSetter, resultSetExtractor);
        return query;
    }

    @Override
    public List<Object> selectObtainList(List<String> pris) {

        StringBuffer stringBuffer = new StringBuffer("select `id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`,`consume_money`," +
                "`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info` from xpense_tracker");
        stringBuffer.append(" where id in (");
        for (int i=1; i<=pris.size(); i++){
            stringBuffer.append("?");
            if (i!=pris.size()) stringBuffer.append(",");
        }
        stringBuffer.append(")");
        String sql = stringBuffer.toString();

        PreparedStatementSetter preparedStatementSetter = (ps)->{
            for (int y=1; y<=pris.size(); y++) ps.setObject(y,pris.get(y-1));
        };


        RowMapper<Object> rowMapper = (rs,rowNum)->{
            Map<String,Object> map = new HashMap<>();
            map.put("id",rs.getString("id"));
            map.put("user_id",rs.getString("user_id"));
            map.put("card_id",rs.getString("card_id"));
            map.put("card_item_id",rs.getString("card_item_id"));
            map.put("consume_money",rs.getBigDecimal("consume_money"));
            map.put("consume_frequency",rs.getInt("consume_frequency"));
            map.put("consume_time",rs.getString("consume_time"));
            map.put("rebate",rs.getBigDecimal("rebate"));
            map.put("consume_type",rs.getString("consume_type"));
            map.put("consume_info",rs.getString("consume_info"));
            return map;
        };


        List<Object> query = jdbcTemplate.query(sql, preparedStatementSetter, rowMapper);

        return query;
    }

    @Override
    public List<Object> selectBatch(List<String> pris){

        List<SqlParameter> sqlParameters = new ArrayList<>();
        StringBuffer stringBuffer = new StringBuffer("select `id`,`user_id`,`card_id`,`card_item_id`,`card_item_type`,`consume_money`," +
                "`consume_frequency`,`consume_time`,`rebate`,`consume_type`,`consume_info` from xpense_tracker");
        stringBuffer.append(" where id in (");
        for (int i=1; i<=pris.size(); i++){
            stringBuffer.append("?");
            if (i!=pris.size()) stringBuffer.append(",");
            SqlParameter sqlParameter = new SqlParameter(Types.VARCHAR);
            sqlParameters.add(sqlParameter);
        }
        stringBuffer.append(")");
        String sql = stringBuffer.toString();

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreatorFactory(sql, sqlParameters).newPreparedStatementCreator(pris);

        RowMapper<Object> rowMapper = (rs,rowNum)->{
            Map<String,Object> map = new HashMap<>();
            map.put("id",rs.getString("id"));
            map.put("user_id",rs.getString("user_id"));
            map.put("card_id",rs.getString("card_id"));
            map.put("card_item_id",rs.getString("card_item_id"));
            map.put("consume_money",rs.getBigDecimal("consume_money"));
            map.put("consume_frequency",rs.getInt("consume_frequency"));
            map.put("consume_time",rs.getString("consume_time"));
            map.put("rebate",rs.getBigDecimal("rebate"));
            map.put("consume_type",rs.getString("consume_type"));
            map.put("consume_info",rs.getString("consume_info"));
            return map;
        };

        List<Object> query = jdbcTemplate.query(preparedStatementCreator, rowMapper);
        return query;
    }
}
