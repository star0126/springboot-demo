package com.star;

import com.star.entity.XpenseTracker;
import com.star.service.JdbcTemplateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author dapeng-liguoqing
 * @create 2020-07-28  11:32:39
 * @description
 **/
@SpringBootTest
public class SpringbootJdbctemplateTests {

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    @Test
    void contextLoads(){
        long s = 600000L+ SECONDS.toMillis(1);
        System.out.println(s);
    }

    @Test
    void testInsert(){
        XpenseTracker xpenseTracker = new XpenseTracker();
        xpenseTracker.setUserId("111111");
        xpenseTracker.setCardId("aaaaaa");
        xpenseTracker.setCardItemId("fsdfsa23s");
        xpenseTracker.setCardItemType("消费卡");
        xpenseTracker.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker.setConsumeFrequency(1);
        xpenseTracker.setConsumeTime(LocalDateTime.now());
        xpenseTracker.setRebate(new BigDecimal("90.00"));
        xpenseTracker.setConsumeType("消费");
        xpenseTracker.setConsumeInfo("购物消费");
        int insert = jdbcTemplateService.insert(xpenseTracker);
        System.out.println(insert);
    }

    @Test
    void testInsertObtainPri(){
        Object pri = jdbcTemplateService.insertObtainPri("idsss");
        System.out.println(pri);
    }

    @Test
    void testInsertBatch(){
        XpenseTracker xpenseTracker = new XpenseTracker();
        xpenseTracker.setUserId("555555");
        xpenseTracker.setCardId("eeeeee");
        xpenseTracker.setCardItemId("dfgsr21");
        xpenseTracker.setCardItemType("消费卡");
        xpenseTracker.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker.setConsumeFrequency(1);
        xpenseTracker.setConsumeTime(LocalDateTime.now());
        xpenseTracker.setRebate(new BigDecimal("90.00"));
        xpenseTracker.setConsumeType("消费");
        xpenseTracker.setConsumeInfo("购物消费");

        XpenseTracker xpenseTracker2 = new XpenseTracker();
        xpenseTracker2.setUserId("666666");
        xpenseTracker2.setCardId("ffffffff");
        xpenseTracker2.setCardItemId("qecdjk20");
        xpenseTracker2.setCardItemType("消费卡");
        xpenseTracker2.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker2.setConsumeFrequency(1);
        xpenseTracker2.setConsumeTime(LocalDateTime.now());
        xpenseTracker2.setRebate(new BigDecimal("90.00"));
        xpenseTracker2.setConsumeType("消费");
        xpenseTracker2.setConsumeInfo("购物消费");

        List<XpenseTracker> list = new ArrayList<>();
        list.add(null);
        list.add(xpenseTracker);
        list.add(xpenseTracker2);
        int i = jdbcTemplateService.insertBatch(list);
        System.out.println(i);
    }

    @Test
    void testInsertMultipleBatches(){
        XpenseTracker xpenseTracker = new XpenseTracker();
        xpenseTracker.setUserId("1212121212");
        xpenseTracker.setCardId("565464646");
        xpenseTracker.setCardItemId("dasdh78qad");
        xpenseTracker.setCardItemType("消费卡");
        xpenseTracker.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker.setConsumeFrequency(1);
        xpenseTracker.setConsumeTime(LocalDateTime.now());
        xpenseTracker.setRebate(new BigDecimal("90.00"));
        xpenseTracker.setConsumeType("消费");
        xpenseTracker.setConsumeInfo("购物消费");

        XpenseTracker xpenseTracker2 = new XpenseTracker();
        xpenseTracker2.setUserId("1313131313");
        xpenseTracker2.setCardId("sdasdc");
        xpenseTracker2.setCardItemId("5sds5as");
        xpenseTracker2.setCardItemType("消费卡");
        xpenseTracker2.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker2.setConsumeFrequency(1);
        xpenseTracker2.setConsumeTime(LocalDateTime.now());
        xpenseTracker2.setRebate(new BigDecimal("90.00"));
        xpenseTracker2.setConsumeType("消费");
        xpenseTracker2.setConsumeInfo("购物消费");

        XpenseTracker xpenseTracker3 = new XpenseTracker();
        xpenseTracker3.setUserId("1414141414");
        xpenseTracker3.setCardId("efsfd654w");
        xpenseTracker3.setCardItemId("sdaf48qwe4");
        xpenseTracker3.setCardItemType("消费卡");
        xpenseTracker3.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker3.setConsumeFrequency(1);
        xpenseTracker3.setConsumeTime(LocalDateTime.now());
        xpenseTracker3.setRebate(new BigDecimal("90.00"));
        xpenseTracker3.setConsumeType("消费");
        xpenseTracker3.setConsumeInfo("购物消费");

        XpenseTracker xpenseTracker4 = new XpenseTracker();
        xpenseTracker4.setUserId("1515151515");
        xpenseTracker4.setCardId("Df3f5sd1ds12");
        xpenseTracker4.setCardItemId("cdfsd46254s");
        xpenseTracker4.setCardItemType("消费卡");
        xpenseTracker4.setConsumeMoney(new BigDecimal("100.00"));
        xpenseTracker4.setConsumeFrequency(1);
        xpenseTracker4.setConsumeTime(LocalDateTime.now());
        xpenseTracker4.setRebate(new BigDecimal("90.00"));
        xpenseTracker4.setConsumeType("消费");
        xpenseTracker4.setConsumeInfo("购物消费");

        List<XpenseTracker> list = new ArrayList<>();
        list.add(null);
        list.add(xpenseTracker);
        list.add(xpenseTracker2);
        list.add(xpenseTracker3);
        list.add(xpenseTracker4);
        int i = jdbcTemplateService.insertMultipleBatches(list, 2);
        System.out.println(i);
    }

    @Test
    void testSelect(){
        Object select = jdbcTemplateService.select("08e93e1c-e621-4a41-afd4-6420ab89f672");
        System.out.println(select.toString());
    }

    @Test
    void testSelectObtainMap(){
        Object select = jdbcTemplateService.selectObtainMap("08e93e1c-e621-4a41-afd4-6420ab89f672");
        System.out.println(select.toString());
    }

    @Test
    void testSelectObtainList(){
        List<String> list = new ArrayList<>();
        list.add("08e93e1c-e621-4a41-afd4-6420ab89f672");
        list.add("e7ed2a1d-6053-4566-b86c-ad136225af37");
        List<Object> select = jdbcTemplateService.selectObtainList(list);
        System.out.println(select.toString());
    }

    @Test
    void testSelectBatch(){
        List<String> list = new ArrayList<>();
        list.add("08e93e1c-e621-4a41-afd4-6420ab89f672");
        list.add("e7ed2a1d-6053-4566-b86c-ad136225af37");
        List<Object> select = jdbcTemplateService.selectBatch(list);
        System.out.println(select.toString());
    }

    @Test
    void testDelete(){
        int delete = jdbcTemplateService.delete("a483f49d-a432-4734-a664-f58ccc946c3c");
        System.out.println(delete);
    }

    @Test
    void testDeleteBatch(){
        List<String> list = new ArrayList<>();
        list.add("5ef23aff-dccc-4eeb-bc98-e9e92fccb580");
        list.add("d71ef565-0cbd-4d76-8d88-996190be0bab");
        int deleteBatch = jdbcTemplateService.deleteBatch(list);
        System.out.println(deleteBatch);
    }

}
