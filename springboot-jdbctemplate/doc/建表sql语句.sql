DROP TABLE IF EXISTS `xpense_tracker`;
CREATE TABLE `xpense_tracker`  (
   `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消费记录id',
   `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员用户id',
   `card_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员卡号id',
   `card_item_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡项id',
   `card_item_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员卡卡项种类',
   `consume_money` decimal(10, 2) NOT NULL COMMENT '消费金额',
   `consume_frequency` int(11) NULL DEFAULT NULL COMMENT '消费频次',
   `consume_time` datetime NOT NULL COMMENT '消费时间',
   `rebate` decimal(10, 2) NOT NULL COMMENT '消费折扣',
   `consume_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消费类型',
   `consume_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消费事项',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '顾客消费记录表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

DROP TABLE IF EXISTS `sys_dict_index`;
CREATE TABLE `sys_dict_index`  (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '索引',
    `dict_index` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典索引',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典索引表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;