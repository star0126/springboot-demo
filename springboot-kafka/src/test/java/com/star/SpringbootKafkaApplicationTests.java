package com.star;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-06-19  17:23:53
 * @version: v1.0
 * @description: 测试
 **/
@SpringBootTest
public class SpringbootKafkaApplicationTests {

    @Test
    void contextLoads(){

    }

}
