package com.star;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-06-20 23:29:32
 * @version: v1.0
 * @description: kafka 生产者  需要注意的是kafka的生产者和消费者一般不会同时出现在一个项目中！！！
 **/
@Component
public class KafkaProducer {

    private static final String TOPIC = "star-test";

    @Resource
    private KafkaTemplate<String,String> kafkaTemplate;

    /**
     * 发送消息
     * @param key 消息的键
     * @param messageBody 消息体
     */
    public void sendMessage(String key,String messageBody){
        kafkaTemplate.send(TOPIC,key,messageBody);
    }
}
