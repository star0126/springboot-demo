package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-06-19  17:20:10
 * @version: v1.0
 * @description: springboot整合kafka启动器
 **/
@SpringBootApplication
public class SpringbootKafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootKafkaApplication.class,args);
    }
}
