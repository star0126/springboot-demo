package com.star;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-06-22  10:11:47
 * @version: v1.0
 * @description: kafka控制器
 **/
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {

    @Autowired
    private KafkaProducer kafkaProducer;

    @GetMapping("/send")
    public String sendMessage(){
        Order order = new Order();
        order.setId(UUID.randomUUID().toString().replace("-",""));
        order.setName("大额订单");
        order.setCreateTime(new Date());
        kafkaProducer.sendMessage("big-order",JSON.toJSONString(order));
        return "success";
    }

}
