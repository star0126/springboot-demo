package com.star;

import com.alibaba.fastjson.JSON;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: guoqing-li
 * @create: 2020-06-20 23:28:57
 * @version: v1.0
 * @description: kafka 消费者   需要注意的是kafka的生产者和消费者一般不会同时出现在一个项目中！！！
 **/
@Component
public class KafkaConsumer {

    @KafkaListener(topics = "star-test",groupId = "springboot_kafka_demo")
    public void consumer(ConsumerRecord consumerRecord){
        String topic = consumerRecord.topic();
        int partition = consumerRecord.partition();
        String timestampType = consumerRecord.timestampType().toString();
        Date date = new Date(consumerRecord.timestamp());
        Object key = consumerRecord.key();
        Order order = JSON.parseObject((String) consumerRecord.value(), Order.class);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("我的kafka消息消费："+consumerRecord.toString());
        System.out.println("主题："+topic);
        System.out.println("分区："+partition);
        System.out.println(timestampType+"："+simpleDateFormat.format(date));
        System.out.println("key："+key);
        System.out.println("消息体为："+order.toString());
    }

}
