package com.star.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guoqing_li   2021-11-21  16:01:26
 * 错误测试控制器
 **/
@RestController
@RequestMapping("/error")
public class ErrorTestController {

    @GetMapping("/1")
    public void error1(){
        int a = 10 / 0 ;
    }

}
