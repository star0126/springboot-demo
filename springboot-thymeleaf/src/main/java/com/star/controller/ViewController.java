package com.star.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author guoqing_li   2021-11-19  15:33:23
 * 视图控制器
 **/
@RestController
@RequestMapping("/view")
public class ViewController {

    private static final Logger log = LoggerFactory.getLogger(ViewController.class);

    @GetMapping("/go-test")
    public ModelAndView test(ModelAndView mv){
        mv.setViewName("/test");
        mv.addObject("msg","欢迎使用Thymeleaf!");
        return mv;
    }

}
