package com.star.util;

import com.star.config.propertie.JwtProperties;
import com.star.exception.MyJwtException;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @author guoqing_li   2021-07-05  15:18:32
 * json web token 工具类
 **/
public class JwtUtil {

    private static Logger log = LoggerFactory.getLogger(JwtUtil.class);


    public static String createToken(JwtProperties jwtProperties){
        try {
            Date now = new Date();
            JwtBuilder jwtBuilder = Jwts.builder()
                    // 设置头部type元素为JWT
                    .setHeaderParam("typ", "JWT")
                    // 设置签名算法和签名密匙
                    .signWith(SignatureAlgorithm.HS256, jwtProperties.getSecret())
                    // 设置Token签发者
                    .setIssuer(jwtProperties.getIssuer())
                    // 设置Token接收者
                    .setAudience(jwtProperties.getAudience())
                    // 设置Token签发时间
                    .setIssuedAt(now)
                    // 设置Token开始生效时间
                    .setNotBefore(now)
                    // 设置Token过期时间
                    .setExpiration(new Date(now.getTime()+jwtProperties.getExpirationTime()));
            return jwtBuilder.compact();
        }catch (Exception e){
            log.error("生成token错误",e);
            throw new MyJwtException("生成token错误");
        }
    }


    public static Claims parseToken(String token,JwtProperties jwtProperties){
        try {
            return Jwts.parser().setSigningKey(jwtProperties.getSecret()).parseClaimsJws(token).getBody();
        }catch (ExpiredJwtException e){
            log.error("token过期",e);
            throw new MyJwtException("token过期");
        }catch (Exception e){
            log.error("token解析异常，token:{}",token);
            throw new MyJwtException("token解析错误");
        }
    }

}
