package com.star.exception;

/**
 * @author guoqing_li   2021-07-05  13:49:07
 **/
public class MyJwtException extends RuntimeException{

    private int code;

    public MyJwtException(String message){
        super(message);
        this.code=4001;
    }

    public int getCode() {
        return code;
    }

    private void setCode(int code) {
        this.code = code;
    }
}
