package com.star.config;

import com.star.config.interceptor.JwtInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author guoqing_li   2021-07-05  15:09:18
 * jwt拦截器配置类
 **/
@Configuration
public class JwtInterceptorConfig implements WebMvcConfigurer {

    @Resource
    private JwtInterceptor jwtInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptor)
                // 拦截的请求，表示拦截/jwt下所有
                .addPathPatterns("/jwt/**")
                // 不拦截的请求，登录接口
                .excludePathPatterns("/jwt/login");
    }

}
