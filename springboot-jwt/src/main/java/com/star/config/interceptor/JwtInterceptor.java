package com.star.config.interceptor;

import com.star.infrastructure.StaticKey;
import com.star.config.propertie.JwtProperties;
import com.star.exception.MyJwtException;
import com.star.util.JwtUtil;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author guoqing_li   2021-07-05  14:54:59
 * JWT拦截器
 **/
@Component
public class JwtInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private JwtProperties jwtProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        // 在拦截器中，如果请求为OPTIONS请求，则返回true，表示可以正常访问，然后就会收到真正的GET/POST请求
        if (HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
            return true;
        }
        try {
            String token = request.getHeader(StaticKey.AUTHORIZATION).substring(7);
            if ("".equals(token)){ throw new MyJwtException("token缺失"); }
            JwtUtil.parseToken(token,jwtProperties);
            return true;
        } catch (MyJwtException e){
            e.printStackTrace();
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(e.getMessage());
            return false;
        } catch (Exception e){
            e.printStackTrace();
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("token错误");
            return false;
        }

    }

}
