package com.star.config.propertie;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author guoqing_li   2021-07-05  11:12:23
 **/
@Component
@ConfigurationProperties(prefix = "my-config.jwt")
public class JwtProperties {

    /**
     * 签名密匙(base64编码)
     */
    private String secret;

    /**
     * 过期时长(毫秒)
     */
    private int expirationTime = 0;

    /**
     * jwt签发者
     */
    private String issuer;

    /**
     * jwt接收者
     */
    private String audience;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public int getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(int expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }
}
