package com.star.controller;

import com.star.config.propertie.JwtProperties;
import com.star.util.JwtUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author guoqing_li   2021-07-05  10:02:09
 **/
@RestController
@RequestMapping("/jwt")
public class JwtController {

    @Resource
    private JwtProperties jwtProperties;


    @PostMapping("/login")
    public String login(){
        String token = JwtUtil.createToken(jwtProperties);
        return token;
    }

    @PostMapping("/test")
    public String test(HttpServletRequest request){
        String authorization = request.getHeader("Authorization");
        return authorization;
    }
    

}
