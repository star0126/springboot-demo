package com.star.infrastructure;

/**
 * @author guoqing_li   2021-07-05  14:59:44
 **/
public class StaticKey {

    public static final String AUTHORIZATION = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";

}
