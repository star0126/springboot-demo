package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li   2021-07-05  13:58:45
 **/
@SpringBootApplication
public class SpringbootJwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootJwtApplication.class,args);
    }
}
