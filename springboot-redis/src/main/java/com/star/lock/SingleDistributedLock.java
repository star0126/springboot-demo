package com.star.lock;

/**
 * @author guoqing_li
 * @create 2021-01-25  11:27:03
 * @description redis单机分布式锁 示例
 *              redis分布式锁官方文档：https://redis.io/topics/distlock
 *
 * redis分布式锁（单机redis版本）：
 * （1）主要利用redis单线程处理指令的特点实现
 * （2）需要保证加锁、解锁操作的原子性
 * （3）需要保证锁的互斥性
 * （4）需要保证没有死锁的情况
 *
 **/
public class SingleDistributedLock {



}
