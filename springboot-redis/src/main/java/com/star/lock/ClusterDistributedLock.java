package com.star.lock;

/**
 * @author guoqing_li
 * @create 2021-01-25  13:54:28
 * @description redis集群分布式锁 示例
 *              redis分布式锁官方文档：https://redis.io/topics/distlock
 *
 * redis分布式锁（集群redis版本）：
 * （1）主要利用redis单线程处理指令的特点实现
 * （2）需要保证加锁、解锁操作的原子性
 * （3）需要保证锁的互斥性
 * （4）需要保证没有死锁的情况
 * （5）需要保证有一定的容灾能力(即主节点崩溃其它子节点被提升为主节点后依然能够保证锁不失效)
 *
 **/
public class ClusterDistributedLock {

}
