package com.star.client;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Range;
import org.springframework.data.domain.Range.Bound;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.BitFieldSubCommands.BitFieldType;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.RedisGeoCommands.DistanceUnit;
import org.springframework.data.redis.connection.RedisStringCommands.BitOperation;
import org.springframework.data.redis.connection.RedisZSetCommands.Limit;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.connection.stream.StreamReadOptions;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions.ScanOptionsBuilder;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * @author guoqing_li
 * @create 2021-01-04  16:36:57
 * @description redis操作管理类
 **/

@Component
@Validated
public class RedisService {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private RedisTemplate<String, Object> redisTemplate;

  /*解除派作业锁脚本对象*/
  private DefaultRedisScript<Boolean> sendjobUnlockScript;

  /**
   * 解除派作业锁脚本加载
   * 在加载RedisService类的时候运行并且只会执行一次，用于初始化加载lua脚本，加载顺序在依赖注入之后
   */
  @PostConstruct
  private void initSendjobUnlockScript(){
    sendjobUnlockScript = new DefaultRedisScript<Boolean>();
    sendjobUnlockScript.setResultType(Boolean.class);
    sendjobUnlockScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("sendjob-unlock.lua")));
  }


  // ========================================key相关操作========================================

  /**
   * 匹配查询redis中的key，如果不传值将默认查全部key，
   * 如果传多个值将按照顺序匹配查询规则且只按照第一个匹配到的规则查询，这里的匹配查询是全字符匹配而不是单字符匹配
   *
   * @param pre 前置字符
   * @param mid 中间字符
   * @param suf 后置字符
   * @return 规则匹配查询得到的redis缓存的key集合
   */
  public Set<String> keys(String pre, String mid, String suf) {
    StringBuilder stringBuffer = new StringBuilder();
    if (null != pre && !"".equals(pre)) {
      stringBuffer.append(pre).append("*");
    } else if (null != mid && !"".equals(mid)) {
      stringBuffer.append("*").append(pre).append("*");
    } else if (null != suf && !"".equals(suf)) {
      stringBuffer.append("*").append(suf);
    } else {
      stringBuffer.append("*");
    }
    return redisTemplate.keys(stringBuffer.toString());
  }

  /**
   * 查询redis中的key，通过增量迭代的方式逐次返回key值，较之于keys * 可以减少redis阻塞风险
   * 这里将游标封装在方法内直接驱动游标将所有数据都获取到，如需要游标逐次遍历需另外编写方法实现
   * @param pattern  匹配模式，如： * 则表示查询全部，abcd*  则表示以abcd为前缀
   * @param count    游标移动一次(迭代一次)查询的数量，该值建议不要设置太大
   * @return  匹配的key值
   */
  public Set<String> scan(@NotBlank String pattern,int count){
    try {
      return redisTemplate.execute((RedisCallback<Set<String>>) connection -> {
        Set<String> keys = new HashSet<String>();
        try {
          Cursor<byte[]> cursor = connection.scan(new ScanOptionsBuilder().match(pattern).count(count).build());
          while (cursor.hasNext()) { keys.add(new String(cursor.next(), StandardCharsets.UTF_8)); }
          return keys;
        } catch (Exception e) {
          logger.warn("redisService.redisTemplate.execute异常：", e);
          return keys;
        }
      });
    }catch (Exception e){
      logger.warn("redisService.redisTemplate.execute异常：", e);
      return new HashSet<String>(0);
    }
  }

  /**
   * 根据key获取其值
   *
   * @param key redis的key
   * @return key所对应的value值
   */
  public Object getValueByKey(@NotBlank String key) {
    DataType type = redisTemplate.type(key);
    if (null == type){
      return null;
    }
    if (type.equals(DataType.STRING)) {
      return this.strGet(key);
    } else if (type.equals(DataType.LIST)) {
      return this.listGet(key);
    } else if (type.equals(DataType.HASH)) {
      return this.hashGet(key);
    } else if (type.equals(DataType.SET)) {
      return this.setGet(key);
    } else if (type.equals(DataType.ZSET)) {
      return this.zsetGet(key);
    }
    return null;
  }

  /**
   * 为redis中的某个key设置过期时间，这个key应当是存在的
   * @param key redis的key
   * @param time redis过期时间
   * @param timeUnit 过期时间单位
   * @return 操作结果
   */
  public boolean expire(@NotBlank String key,long time, @NotNull TimeUnit timeUnit){
    try {
      if (time>0){
        Boolean result = redisTemplate.expire(key, time, timeUnit);
        return result == null ? false : result;
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.expire异常：", e);
      return false;
    }
  }

  /**
   * 获取key对应的过期时间
   * @param key redis的key
   * @param timeUnit 时间单位
   * @return 过期时间
   */
  public Long getExpire(@NotBlank String key, TimeUnit timeUnit){
    try {
      if (null==timeUnit){
        return redisTemplate.getExpire(key);
      }
      return redisTemplate.getExpire(key, timeUnit);
    }catch (Exception e){
      logger.warn("redisService.getExpire异常：", e);
      return null;
    }
  }

  /**
   * 根据redis的key删除redis缓存
   * @param key redis的key
   * @return 删除结果
   */
  public boolean delete(@NotEmpty String... key){
    try {
      Long delete = redisTemplate.delete(Arrays.asList(key));
      return null != delete && delete > 0;
    }catch (Exception e){
      logger.warn("redisService.delete异常：", e);
      return false;
    }
  }

  // ========================================String相关操作========================================

  /**
   * 根据redis的key获取其value值
   * @param key redis的key
   * @return value值
   */
  public Object strGet(@NotBlank String key){
    try {
      return redisTemplate.opsForValue().get(key);
    }catch (Exception e){
      logger.warn("redisService.strGet异常：", e);
      return null;
    }
  }

  /**
   * 根据redis的keys批量获取values值，values值的顺序和key的顺序一一对应
   * @param key redis的key
   * @return values列表，如果异常或没有值则返回含null值的空集合，如：[null, null]
   */
  public List<Object> strMultiGet(@NotEmpty String... key){
    try {
      return redisTemplate.opsForValue().multiGet(Arrays.asList(key));
    }catch (Exception e){
      logger.warn("redisService.strMultiGet异常：", e);
      return new ArrayList<>();
    }
  }

  /**
   * 根据redis的key获取其对应的value值，然后使用新的value值覆盖旧值
   * @param key redis的key
   * @param value redis的value
   * @return key对应的旧的value值，如不存在则为null
   */
  public Object strGetAndSet(@NotBlank String key,@NotNull Object value){
    try {
      return redisTemplate.opsForValue().getAndSet(key, value);
    }catch (Exception e){
      logger.warn("redisService.strGetAndSet异常：", e);
      return null;
    }
  }

  /**
   * 向redis中写入String类型的kv缓存
   * @param key redis的key
   * @param value redis的value
   * @return 写入结果
   */
  public boolean strSet(@NotBlank String key,@NotNull Object value){
    try {
      redisTemplate.opsForValue().set(key,value);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSet异常：", e);
      return false;
    }
  }

  /**
   * 向redis中写入String类型的kv缓存并设置过期时间
   * @param key redis的key
   * @param value redis的value
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @return 写入结果
   */
  public boolean strSet(@NotBlank String key,@NotNull Object value, long time, @NotNull TimeUnit timeUnit){
    try {
      redisTemplate.opsForValue().set(key,value,time,timeUnit);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSet异常：", e);
      return false;
    }
  }

  /**
   * 向redis中写入String类型的kv缓存并设置以秒为单位的过期时间
   * @param key redis的key
   * @param value redis的value
   * @param time 过期时间（以秒为单位）
   * @return 写入结果
   */
  public boolean strSet(@NotBlank String key,@NotNull Object value, long time){
    try {
      redisTemplate.opsForValue().set(key,value,time,TimeUnit.SECONDS);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSet异常：", e);
      return false;
    }
  }

  /**
   * 如果不存在，则向redis中写入kv缓存
   * @param key redis的key
   * @param value redis的value
   * @return 写入结果
   */
  public boolean strSetIfAbsent(@NotBlank String key,@NotNull Object value){
    try {
      redisTemplate.opsForValue().setIfAbsent(key,value);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfAbsent异常：", e);
      return false;
    }
  }

  /**
   * 如果不存在，则向redis中写入kv缓存并设置过期时间
   * @param key redis中的key
   * @param value redis中的value
   * @param time 过期时间
   * @param timeUnit 时间类型
   * @return 写入结果
   */
  public boolean strSetIfAbsent(@NotBlank String key,@NotNull Object value, long time, @NotNull TimeUnit timeUnit){
    try {
      redisTemplate.opsForValue().setIfAbsent(key,value,time,timeUnit);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfAbsent异常：", e);
      return false;
    }
  }


  /**
   * 如果不存在，则向redis中写入kv缓存并设置以秒为单位的过期时间
   * @param key redis中的key
   * @param value redis中的value
   * @param time 过期时间
   * @return 写入结果
   */
  public boolean strSetIfAbsent(@NotBlank String key,@NotNull Object value, long time){
    try {
      redisTemplate.opsForValue().setIfAbsent(key,value,time,TimeUnit.SECONDS);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfAbsent异常：", e);
      return false;
    }
  }

  /**
   * 如果不存在，则批量向redis中写入kv缓存
   * @param kvMap 批量kv键值集(map中的key会作为redis的k，value会作为redis的v)
   * @return 写入结果
   */
  public boolean strMultiSetIfAbsent(Map<String,Object> kvMap){
    try {
      Boolean result = redisTemplate.opsForValue().multiSetIfAbsent(kvMap);
      return result == null ? false : result;
    }catch (Exception e){
      logger.warn("redisService.strMultiSetIfAbsent异常：", e);
      return false;
    }
  }

  /**
   * 如果缓存中存在，则向redis中写入kv缓存
   * @param key redis中的key
   * @param value redis中的value
   * @return 写入结果
   */
  public boolean strSetIfPresent(@NotBlank String key,@NotNull Object value){
    try {
      redisTemplate.opsForValue().setIfPresent(key,value);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfPresent异常：", e);
      return false;
    }
  }

  /**
   * 如果缓存中存在，则向redis中写入kv缓存并设置过期时间
   * @param key redis中的key
   * @param value redis中的value
   * @param time 过期时间
   * @param timeUnit 时间类型
   * @return 写入结果
   */
  public boolean strSetIfPresent(@NotBlank String key,@NotNull Object value, long time, @NotNull TimeUnit timeUnit){
    try {
      redisTemplate.opsForValue().setIfPresent(key,value,time,timeUnit);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfPresent异常：", e);
      return false;
    }
  }


  /**
   * 如果缓存中存在，则向redis中写入kv缓存并设置以秒为单位的过期时间
   * @param key redis中的key
   * @param value redis中的value
   * @param time 过期时间
   * @return 写入结果
   */
  public boolean strSetIfPresent(@NotBlank String key,@NotNull Object value, long time){
    try {
      redisTemplate.opsForValue().setIfPresent(key,value,time,TimeUnit.SECONDS);
      return true;
    }catch (Exception e){
      logger.warn("redisService.strSetIfPresent异常：", e);
      return false;
    }
  }


  // ========================================List相关操作========================================

  /**
   * 获取redis中key所对应的全部List类型缓存值
   * @param key redis的key
   * @return List类型的value值，如果异常或没有值则返回空集合
   */
  public List<Object> listGet(@NotBlank String key){
    try {
      return redisTemplate.opsForList().range(key, 0, -1);
    }catch (Exception e){
      logger.warn("redisService.listGet异常：", e);
      return new ArrayList<>();
    }
  }

  /**
   * 获取redis中key所对应的指定下标范围间的List类型缓存值
   * @param key redis的key
   * @param start 开始下标
   * @param end 结束下标
   * @return List类型的value值，如果异常或没有值则返回空集合
   */
  public List<Object> listGet(@NotBlank String key,long start, long end){
    try {
      return redisTemplate.opsForList().range(key, start, end);
    }catch (Exception e){
      logger.warn("redisService.listGet异常：", e);
      return new ArrayList<>();
    }
  }

  /**
   * 获取redis中key所对应的指定下标所对应的List类型缓存值
   * @param key redis的key
   * @param index 下标
   * @return 指定下标所对应的值
   */
  public Object listGetIndex(@NotBlank String key,long index){
    try {
      return redisTemplate.opsForList().index(key,index);
    }catch (Exception e){
      logger.warn("redisService.listGetIndex异常：", e);
      return null;
    }
  }

  /**
   * 将指定key的List类型缓存中指定下标的值设置为Value
   * @param key redis的key
   * @param index 下标
   * @param value 要设置的value值
   * @return 设置结果
   */
  public boolean listSetIndex(@NotBlank String key,long index,@NotNull Object value){
    try {
      redisTemplate.opsForList().set(key,index,value);
      return true;
    }catch (Exception e){
      logger.warn("redisService.listSetIndex异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加数据，如果缓存不存在则创建
   * @param key redis的key
   * @param value 向List右侧追加的数据
   * @return 追加结果
   */
  public boolean listRightAdd(@NotBlank String key,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().rightPush(key, value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRightAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加数据并且设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param value 向List右侧追加的数据
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @return 追加结果
   */
  public boolean listRightAdd(@NotBlank String key,@NotNull Object value,long time,@NotNull TimeUnit timeUnit){
    try {
      Long result = redisTemplate.opsForList().rightPush(key, value);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listRightAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存中第一个与指定参考值referValue相同的缓存值右侧插入新的缓存值value，
   * referValue右侧的其它会顺位移动
   * @param key redis的键
   * @param referValue 写入新值的参考值
   * @param value 要写入的新值
   * @return 写入结果
   */
  public boolean listRightAdd(@NotBlank String key,@NotNull Object referValue,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().rightPush(key,referValue,value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRightAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加全部value数据，如果缓存不存在则创建
   * @param key redis的key
   * @param values 向List右侧追加的数据
   * @return 写入结果
   */
  public boolean listRightAddAll(@NotBlank String key,@NotEmpty Object... values){
    try {
      Long result = redisTemplate.opsForList().rightPushAll(key, values);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRightAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加全部value数据并设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @param values 向List右侧追加的数据
   * @return 写入结果
   */
  public boolean listRightAddAll(@NotBlank String key,long time,@NotNull TimeUnit timeUnit,@NotEmpty Object... values){
    try {
      Long result = redisTemplate.opsForList().rightPushAll(key, values);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listRightAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加全部value数据，如果缓存不存在则创建
   * @param key redis的key
   * @param values 向List右侧追加的数据
   * @return 写入结果
   */
  public boolean listRightAddAll(@NotBlank String key,@NotEmpty Collection<Object> values){
    try {
      Long result = redisTemplate.opsForList().rightPushAll(key, values);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRightAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的右侧追加全部value数据并设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @param values 向List右侧追加的数据
   * @return 写入结果
   */
  public boolean listRightAddAll(@NotBlank String key,long time,@NotNull TimeUnit timeUnit,@NotEmpty Collection<Object> values){
    try {
      Long result = redisTemplate.opsForList().rightPushAll(key, values);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listRightAddAll异常：", e);
      return false;
    }
  }

  /**
   * 如果缓存存在，则向指定key的List类型缓存的右侧追加数据
   * @param key redis的key
   * @param value 向List右侧追加的数据
   * @return 追加结果
   */
  public boolean listRightAddIfPresent(@NotBlank String key,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().rightPushIfPresent(key, value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRightAddIfPresent异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加数据，如果缓存不存在则创建
   * @param key redis的key
   * @param value 向List左侧追加的数据
   * @return 追加结果
   */
  public boolean listLeftAdd(@NotBlank String key,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().leftPush(key, value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listLeftAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加数据并且设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param value 向List左侧追加的数据
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @return 追加结果
   */
  public boolean listLeftAdd(@NotBlank String key,@NotNull Object value,long time,@NotNull TimeUnit timeUnit){
    try {
      Long result = redisTemplate.opsForList().leftPush(key, value);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listLeftAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存中第一个与指定参考值referValue相同的缓存值左侧插入新的缓存值value，
   * referValue左侧的其它值会顺位移动
   * @param key redis的键
   * @param referValue 写入新值的参考值
   * @param value 要写入的新值
   * @return 写入结果
   */
  public boolean listLeftAdd(@NotBlank String key,@NotNull Object referValue,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().leftPush(key,referValue,value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listLeftAdd异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加全部value数据，如果缓存不存在则创建
   * @param key redis的key
   * @param values 向List左侧追加的数据
   * @return 写入结果
   */
  public boolean listLeftAddAll(@NotBlank String key,@NotEmpty Object... values){
    try {
      Long result = redisTemplate.opsForList().leftPushAll(key, values);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listLeftAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加全部value数据并设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @param values 向List左侧追加的数据
   * @return 写入结果
   */
  public boolean listLeftAddAll(@NotBlank String key,long time,@NotNull TimeUnit timeUnit,@NotEmpty Object... values){
    try {
      Long result = redisTemplate.opsForList().leftPushAll(key, values);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listLeftAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加全部value数据，如果缓存不存在则创建
   * @param key redis的key
   * @param values 向List左侧追加的数据
   * @return 写入结果
   */
  public boolean listLeftAddAll(@NotBlank String key,@NotEmpty Collection<Object> values){
    try {
      Long result = redisTemplate.opsForList().leftPushAll(key, values);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listLeftAddAll异常：", e);
      return false;
    }
  }

  /**
   * 向指定key的List类型缓存的左侧追加全部value数据并设置过期时间，如果缓存不存在则创建
   * @param key redis的key
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @param values 向List左侧追加的数据
   * @return 写入结果
   */
  public boolean listLeftAddAll(@NotBlank String key,long time,@NotNull TimeUnit timeUnit,@NotEmpty Collection<Object> values){
    try {
      Long result = redisTemplate.opsForList().leftPushAll(key, values);
      if (result != null && result > 0){
        return this.expire(key,time,timeUnit);
      }
      return false;
    }catch (Exception e){
      logger.warn("redisService.listLeftAddAll异常：", e);
      return false;
    }
  }

  /**
   * 如果缓存存在，则向指定key的List类型缓存的左侧追加数据
   * @param key redis的key
   * @param value 向List左侧追加的数据
   * @return 追加结果
   */
  public boolean listLeftAddIfPresent(@NotBlank String key,@NotNull Object value){
    try {
      Long result = redisTemplate.opsForList().leftPushIfPresent(key, value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listLeftAddIfPresent异常：", e);
      return false;
    }
  }

  /**
   * 从指定key的List类型的redis缓存的左侧弹栈(获取并删除)一个数据
   * @param key redis的key
   * @return 弹栈的数据
   */
  public Object listLeftPop(String key){
    try {
      return redisTemplate.opsForList().leftPop(key);
    }catch (Exception e){
      logger.warn("redisService.listLeftPop异常：", e);
      return null;
    }
  }

  /**
   * 从指定key的List类型的redis缓存的左侧弹栈(获取并删除)一个数据
   * 如果key中没有数据可以弹栈则保持请求等到有数据可以弹栈或到达指定时长为止
   * @param key redis的key
   * @param time 等待时长
   * @param timeUnit 时间单位
   * @return 弹栈的数据
   */
  public Object listLeftPop(String key,long time, TimeUnit timeUnit){
    try {
      return redisTemplate.opsForList().leftPop(key,time,timeUnit);
    }catch (Exception e){
      logger.warn("redisService.listLeftPop异常：", e);
      return null;
    }
  }

  /**
   * 从指定key的List类型的redis缓存的右侧弹栈(获取并删除)一个数据
   * @param key redis的key
   * @return 弹栈的数据
   */
  public Object listRightPop(String key){
    try {
      return redisTemplate.opsForList().rightPop(key);
    }catch (Exception e){
      logger.warn("redisService.listRightPop异常：", e);
      return null;
    }
  }

  /**
   * 从指定key的List类型的redis缓存的右侧弹栈(获取并删除)一个数据
   * 如果key中没有数据可以弹栈则保持请求等到有数据可以弹栈或到达指定时长为止
   * @param key redis的key
   * @param time 等待时长
   * @param timeUnit 时间单位
   * @return 弹栈的数据
   */
  public Object listRightPop(String key,long time, TimeUnit timeUnit){
    try {
      return redisTemplate.opsForList().rightPop(key,time,timeUnit);
    }catch (Exception e){
      logger.warn("redisService.listRightPop异常：", e);
      return null;
    }
  }

  /**
   * 从List类型的指定key的redis缓存中删除count个和value相同的缓存数据
   * @param key redis的key
   * @param count 删除的数量
   * @param value 要删除的值
   * @return 删除结果
   */
  public boolean listRemove(String key, long count, Object value){
    try {
      Long result = redisTemplate.opsForList().remove(key, count, value);
      return result != null && result > 0;
    }catch (Exception e){
      logger.warn("redisService.listRemove异常：", e);
      return false;
    }
  }

  /**
   * 获取指定的List类型的缓存大小
   * @param key redis的key
   * @return 缓存大小
   */
  public long listSize(@NotBlank String key){
    try {
      Long size = redisTemplate.opsForList().size(key);
      return size==null ? 0 : size;
    }catch (Exception e){
      logger.warn("redisService.listSize异常：", e);
      return 0;
    }
  }

  /**
   * 对指定key的List类似缓存做修剪，只保留下标为start到下标为end之间的数据
   * @param key redis的key
   * @param start 开始下标
   * @param end 结束下标
   * @return 修剪结果
   */
  public boolean listTrim(@NotBlank String key,long start,long end){
    try {
      redisTemplate.opsForList().trim(key,start,end);
      return true;
    }catch (Exception e){
      logger.warn("redisService.listTrim异常：", e);
      return false;
    }
  }


  // ========================================Hash相关操作========================================

  /**
   * 获取指定的hash类型的key的全部缓存数据
   * @param key redis的key
   * @return hash类型缓存数据，如果不存在则返回空map
   */
  public Map<Object,Object> hashGet(@NotBlank String key){
    try {
      return redisTemplate.opsForHash().entries(key);
    }catch (Exception e){
      logger.warn("redisService.hashGet异常：", e);
      return new HashMap<>(0);
    }
  }

  /**
   * 获取指定的hash类型的缓存中某个hk对应的hv值
   * @param key redis的key
   * @param hk redis的hash类型value中的hashKey
   * @return hk对应的hv值
   */
  public Object hashGet(@NotBlank String key,@NotBlank String hk){
    try {
      return redisTemplate.opsForHash().get(key, hk);
    }catch (Exception e){
      logger.warn("redisService.hashGet异常：", e);
      return null;
    }
  }

  /**
   * 根据hk集合批量获取指定的hash类型的缓存中的hv值
   * @param key redis的key
   * @param hk redis的hash类型value中的hashKey
   * @return hk对应的hv值集合
   */
  public List<Object> hashMultiGet(@NotBlank String key,@NotEmpty Collection<Object> hk){
    try {
      return redisTemplate.opsForHash().multiGet(key,hk);
    }catch (Exception e){
      logger.warn("redisService.hashMultiGet异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 获取指定的hash类型的缓存中的所有hk的集合
   * @param key redis的key
   * @return hk集合，如果key不存在则返回空Set
   */
  public Set<Object> hashHks(@NotBlank String key){
    try {
      return redisTemplate.opsForHash().keys(key);
    }catch (Exception e){
      logger.warn("redisService.hashHks异常：", e);
      return new HashSet<>(0);
    }
  }

  /**
   * 获取指定的hash类型的缓存中的所有hv的集合
   * @param key redis的key
   * @return hv集合，如果key不存在则返回空List
   */
  public List<Object> hashHvs(@NotBlank String key){
    try {
      return redisTemplate.opsForHash().values(key);
    }catch (Exception e){
      logger.warn("redisService.hashHvs异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 向redis的hash类型中写入缓存数据
   * @param key redis的key
   * @param hk redis的value的hk
   * @param hv redis的value的hv
   * @return 结果
   */
  public boolean hashPut(@NotBlank String key,@NotNull Object hk,@NotNull Object hv){
    try {
      redisTemplate.opsForHash().put(key,hk,hv);
      return true;
    }catch (Exception e){
      logger.warn("redisService.hashPut异常：", e);
      return false;
    }
  }

  /**
   * 向redis的hash类型中写入缓存数据并设置过期时间
   * @param key redis的key
   * @param hk redis的value的hk
   * @param hv redis的value的hv
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @return 结果
   */
  public boolean hashPut(@NotBlank String key,@NotNull Object hk,@NotNull Object hv,long time,@NotNull TimeUnit timeUnit){
    try {
      redisTemplate.opsForHash().put(key,hk,hv);
      if (!this.expire(key,time,timeUnit)){
        this.hashRemove(key, hk);
        return false;
      }
      return true;
    }catch (Exception e){
      logger.warn("redisService.hashPut异常：", e);
      return false;
    }
  }

  /**
   * 向redis的hash类型中写入缓存数据
   * @param key redis的key
   * @param value redis的hash类型value
   * @return 结果
   */
  public boolean hashPut(@NotBlank String key,@NotEmpty Map<Object,Object> value){
    try {
      redisTemplate.opsForHash().putAll(key,value);
      return true;
    }catch (Exception e){
      logger.warn("redisService.hashPut异常：", e);
      return false;
    }
  }

  /**
   * 向redis的hash类型中写入缓存数据并设置过期时间
   * @param key redis的key
   * @param value redis的hash类型value
   * @param time 过期时间
   * @param timeUnit 时间单位
   * @return 结果
   */
  public boolean hashPut(@NotBlank String key,@NotEmpty Map<Object,Object> value,long time,@NotNull TimeUnit timeUnit){
    try {
      redisTemplate.opsForHash().putAll(key,value);
      if (!this.expire(key,time,timeUnit)){
        for (Object hk : value.keySet()){
          this.hashRemove(key,hk);
        }
        return false;
      }
      return true;
    }catch (Exception e){
      logger.warn("redisService.hashPut异常：", e);
      return false;
    }
  }


  /**
   * 根据指定hks删除hash类型中的缓存
   * @param key redis的key
   * @param hks redis的value中的hk
   * @return 删除结果
   */
  public boolean hashRemove(@NotBlank String key,@NotEmpty Object... hks){
    try {
      Long result = redisTemplate.opsForHash().delete(key, hks);
      return result > 0;
    }catch (Exception e){
      logger.warn("redisService.hashRemove异常：", e);
      return false;
    }
  }

  /**
   * 判断hash类型的缓存中是否存在指定的hk
   * @param key redis的key
   * @param hk redis的value中的hk
   * @return 判断结果
   */
  public boolean hashHasKey(@NotBlank String key,@NotNull Object hk){
    try {
      Boolean result = redisTemplate.opsForHash().hasKey(key, hk);
      return result==null ? false : result;
    }catch (Exception e){
      logger.warn("redisService.hashHasKey异常：", e);
      return false;
    }
  }


  // ========================================Set相关操作========================================

  /**
   * 从redis的set类型缓存中获取数据
   * @param key redis的key
   * @return set缓存数据
   */
  public Set<Object> setGet(@NotBlank String key){
    try {
      return redisTemplate.opsForSet().members(key);
    }catch (Exception e){
      logger.warn("redisService.setGet异常：", e);
      return new HashSet<>(0);
    }
  }

  /**
   * 向redis的set类型缓存中写入数据
   * @param key redis的key
   * @param values redis的value
   * @return 写入结果
   */
  public boolean setAdd(@NotBlank String key,@NotNull Object... values){
    try {
      Long result = redisTemplate.opsForSet().add(key, values);
      return result!=null && result>0;
    }catch (Exception e){
      logger.warn("redisService.setAdd异常：", e);
      return false;
    }
  }


  /**
   * 判断value值是否包含在指定的set缓存中
   * @param key     redis的key
   * @param value   要判断的value
   * @return    判断结果
   * */
  public boolean setIsContains(@NotBlank String key,@NotNull Object value){
    try {
      Boolean member = redisTemplate.opsForSet().isMember(key, value);
      return member!=null ? member : false;
    }catch (Exception e){
      logger.warn("redisService.setIsContains异常：", e);
      return false;
    }
  }


  // ========================================ZSet相关操作========================================

  /**
   * 从redis的zset类型缓存中取出数据
   * @param key redis的key
   * @return 获取到的数据
   */
  public Set<Object> zsetGet(@NotBlank String key){
    try {
      return redisTemplate.opsForZSet().range(key, 0,-1);
    }catch (Exception e){
      logger.warn("redisService.zsetGet异常：", e);
      return new HashSet<>(0);
    }
  }

  /**
   * 向redis的zset类型缓存中写入数据
   * @param key     redis的key
   * @param value   缓存值
   * @param score   权重分数
   * @return  写入结果
   */
  public boolean zsetAdd(@NotBlank String key,@NotNull Object value,double score){
    try {
      Boolean result = redisTemplate.opsForZSet().add(key, value, score);
      return result==null ? false : result;
    }catch (Exception e){
      logger.warn("redisService.zsetAdd异常：", e);
      return false;
    }
  }

  // ========================================redis扩展类型 Bitmap相关操作========================================

  /**
   * 向redis的bitmap类型缓存中写入数据
   * @param key     redis的key
   * @param offset  bitmap下标偏移量
   * @param value   bit位值(false为0，true为1)
   * @return  写入前的bit位值（bit为1时是true，bit为0时是false）
   */
  public boolean bitmapSet(@NotBlank String key,long offset,boolean value){
    try {
      Boolean result = redisTemplate.opsForValue().setBit(key, offset, value);
      return result==null ? false : result;
    }catch (Exception e){
      logger.warn("redisService.bitmapSet异常：", e);
      return false;
    }
  }

  /**
   * 根据key和bitmap下标偏移量获得bit值的布尔值
   * @param key     redis的key
   * @param offset  bitmap下标偏移量
   * @return  布尔值（bit为1时是true，bit为0时是false）
   */
  public boolean bitmapGet(@NotBlank String key,long offset){
    try {
      Boolean result = redisTemplate.opsForValue().getBit(key, offset);
      return result==null ? false : result;
    }catch (Exception e){
      logger.warn("redisService.bitmapGet异常：", e);
      return false;
    }
  }

  /**
   * 统计指定key中bit值为1的bit位数量
   * @param key redis的key
   * @return bit值为1的bit位数量
   */
  public long bitmapCount(@NotBlank String key){
    try {
      Long count = redisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes()));
      return count==null ? 0L:count;
    }catch (Exception e){
      logger.warn("redisService.bitmapCount异常：", e);
      return 0L;
    }
  }

  /**
   * 统计指定key中指定下标偏移量间bit值为1的bit位数量
   * @param key     redis的key
   * @param start   下标偏移量的开始值
   * @param end     下标偏移量的结束值
   * @return    bit值为1的bit位数量
   */
  public long bitmapCount(@NotBlank String key, long start, long end){

    try {
      Long count = redisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes(),start,end));
      return count==null ? 0L:count;
    }catch (Exception e){
      logger.warn("redisService.bitmapCount异常：", e);
      return 0L;
    }
  }


  /**
   * 从指定key缓存中获取指定bit值第一次出现的下标
   * @param key       redis的key
   * @param bitValue  bit值的布尔形式(false为0，true为1)
   * @return  下标值
   */
  public long bitPos(String key, boolean bitValue){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitPos(key.getBytes(), bitValue));
      return result==null ? 0L:result;
    }catch (Exception e){
      logger.warn("redisService.bitPos异常：", e);
      return 0L;
    }
  }

  /**
   * 从指定key缓存中获取指定下标区间内指定bit值第一次出现的下标
   * @param key       redis的key
   * @param bitValue  bit值的布尔形式(false为0，true为1)
   * @param start     下标开始位置(包含)
   * @param end       下标结束位置(包含)
   * @return    下标值
   */
  public long bitPos(String key, boolean bitValue,long start, long end){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitPos(key.getBytes(), bitValue, Range.closed(start,end) ));
      return result==null ? 0L:result;
    }catch (Exception e){
      logger.warn("redisService.bitPos异常：", e);
      return 0L;
    }
  }

  /**
   * 将多个bitmap缓存通过and关系计算合并到一个新的缓存中
   * 长度不同将以0填充，直到最长缓存的长度
   * @param destKey  目标缓存key
   * @param keys     要进行计算合并的缓存的key
   * @return 存储在目标键中的字符串的大小，等于最长输入字符串的大小
   */
  public long bitOpAnd(@NotBlank String destKey, @NotNull String... keys){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(BitOperation.AND, destKey.getBytes(), Arrays.stream(keys).map(String::getBytes).toArray(byte[][]::new)));
      return result==null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.bitOpAnd异常：", e);
      return 0L;
    }
  }

  /**
   * 将多个bitmap缓存通过or关系计算合并到一个新的缓存中
   * 长度不同将以0填充，直到最长缓存的长度
   * @param destKey  目标缓存key
   * @param keys     要进行计算合并的缓存的key
   * @return 存储在目标键中的字符串的大小，等于最长输入字符串的大小
   */
  public long bitOpOr(@NotBlank String destKey, @NotNull String... keys){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(BitOperation.OR, destKey.getBytes(), Arrays.stream(keys).map(String::getBytes).toArray(byte[][]::new)));
      return result==null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.bitOpOr异常：", e);
      return 0L;
    }
  }

  /**
   * 将多个bitmap缓存通过xor关系计算合并到一个新的缓存中
   * 长度不同将以0填充，直到最长缓存的长度
   * @param destKey  目标缓存key
   * @param keys     要进行计算合并的缓存的key
   * @return 存储在目标键中的字符串的大小，等于最长输入字符串的大小
   */
  public long bitOpXor(@NotBlank String destKey, @NotNull String... keys){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(BitOperation.XOR, destKey.getBytes(), Arrays.stream(keys).map(String::getBytes).toArray(byte[][]::new)));
      return result==null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.bitOpXor异常：", e);
      return 0L;
    }
  }

  /**
   * 将多个bitmap缓存通过not关系计算合并到一个新的缓存中
   * 长度不同将以0填充，直到最长缓存的长度。执行反位操作，只需一元运算
   * @param destKey  目标缓存key
   * @param key      要进行计算合并的缓存的key
   * @return 存储在目标键中的字符串的大小，等于最长输入字符串的大小
   */
  public long bitOpNot(@NotBlank String destKey, @NotBlank String key){
    try {
      Long result = redisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(BitOperation.NOT, destKey.getBytes(), key.getBytes()));
      return result==null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.bitOpNot异常：", e);
      return 0L;
    }
  }


  // BITFIELD key [GET type offset][SET type offset value][INCRBY type offset increment][OVERFLOW WRAP|SAT|FAIL]

  /**
   * 从缓存中获取从指定偏移量开始的32位十进制有符号整数
   * @param key     redis的key
   * @param offset  下标起始偏移量
   * @return  32位十进制有符号整数
   */
  public List<Long> bitFieldGetI32(@NotBlank String key,long offset){
    try {
      List<Long> result = redisTemplate.opsForValue().bitField(key, BitFieldSubCommands.create().get(BitFieldType.INT_32).valueAt(offset));
      return result==null ? new ArrayList<>(0) : result;
    }catch (Exception e){
      logger.warn("redisService.bitFieldGetI32异常：", e);
      return new ArrayList<>(0);
    }
  }


  /**
   * 从缓存中获取从指定偏移量开始的8位十进制无符号整数
   * @param key     redis的key
   * @param offset  下标起始偏移量
   * @return  8位十进制无符号整数
   */
  public List<Long> bitFieldGetU8(@NotBlank String key,long offset){
    try {
      List<Long> result = redisTemplate.opsForValue().bitField(key, BitFieldSubCommands.create().get(BitFieldType.UINT_8).valueAt(offset));
      return result==null ? new ArrayList<>(0) : result;
    }catch (Exception e){
      logger.warn("redisService.bitFieldGetU8异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 将缓存中从指定偏移量开始的8位十进制有符号整数的二进制形式设置为给定值value
   * @param key     redis的key
   * @param offset  下标起始偏移量
   * @param value   要设置的值
   * @return  从指定偏移量开始的8位十进制有符号整数在设置前的值
   */
  public List<Long> bitFieldSetI8(@NotBlank String key,long offset,long value){
    try {
      List<Long> result = redisTemplate.opsForValue().bitField(key, BitFieldSubCommands.create().set(BitFieldType.INT_8).valueAt(offset).to(value));
      return result==null ? new ArrayList<>(0) : result;
    }catch (Exception e){
      logger.warn("redisService.bitFieldSetI8异常：", e);
      return new ArrayList<>(0);
    }
  }

  // ========================================redis扩展类型 HyperLogLog相关操作========================================

  /**
   * 将指定元素value写入到HyperLogLog缓存中
   * 可以将该操作简单等同为是向一个set集合中添加数据，在缓存中只保留了不重复的数据，只是HyperLogLog中并不存储实际的数据
   * @param key     redis的key
   * @param values  要写入缓存的元素
   * @return    如果有元素被写入则返回1，否则返回0
   */
  public long hyperLogLogAdd(@NotBlank String key,@NotNull Object... values){
    try {
      Long result = redisTemplate.opsForHyperLogLog().add(key, values);
      return result == null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.hyperLogLogAdd异常：", e);
      return 0L;
    }
  }

  /**
   * 获取给定key对应的HyperLogLog缓存的基数值，如果指定多个HyperLogLog缓存则返回基数估值之和
   * @param keys  redis的key
   * @return  基数值(集合中不同值的数目)，有 0.81%的标准误差
   */
  public long hyperLogLogCount(@NotNull String... keys){
    try {
      Long size = redisTemplate.opsForHyperLogLog().size(keys);
      return size == null ? 0L : size;
    }catch (Exception e){
      logger.warn("redisService.hyperLogLogCount异常：", e);
      return 0L;
    }
  }


  /**
   * 通过并集计算将多个HyperLogLog缓存合并到一个指定key的HyperLogLog缓存中
   * @param destKey     合并结果缓存key
   * @param sourceKeys  待合并的缓存的key
   * @return
   */
  public long hyperLogLogMerge(@NotBlank String destKey, @NotNull String... sourceKeys){
    try {
      Long result = redisTemplate.opsForHyperLogLog().union(destKey, sourceKeys);
      return result == null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.hyperLogLogMerge异常：", e);
      return 0L;
    }
  }

  // ========================================redis扩展类型 GEO相关操作========================================

  /**
   * 将给定的经纬度、地名存储到redis的GEO缓存中
   * 使用Geohash对经纬度进行编码
   * @param key     redis的key
   * @param x       经度 有效经度为-180至180，东经为正数，西经为负数
   * @param y       纬度 有效纬度为-85.05112878至85.05112878，北纬为正数，南纬为负数
   * @param member  地名
   * @return 新添加到键里面的空间元素数量，不包括那些已经存在但是被更新的元素
   */
  public long geoAdd(@NotBlank String key,double x, double y,String member ){
    try {
      Long result = redisTemplate.opsForGeo().add(key, new Point(x, y), member);
      return result == null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.geoAdd异常：", e);
      return 0L;
    }
  }

  /**
   * 将给定的经纬度、地名map存储到redis的GEO缓存中
   * 使用Geohash对经纬度进行编码
   * @param key         redis的key
   * @param memberMap   地名+经纬度map。Point是用于表示经纬度的对象，如：new Point(经度值, 纬度值)
   * @return  新添加到键里面的空间元素数量，不包括那些已经存在但是被更新的元素
   */
  public long geoAdd(@NotBlank String key,Map<Object, Point> memberMap){
    try {
      Long result = redisTemplate.opsForGeo().add(key, memberMap);
      return result == null ? 0L : result;
    }catch (Exception e){
      logger.warn("redisService.geoAdd异常：", e);
      return 0L;
    }
  }

  /**
   * 从GEO缓存中获取指定位置的地名和经纬度值
   * @param key      redis的key
   * @param members  地名
   * @return  地名:经纬度对象 的Map
   */
  public Map<String,Point> geoGet(String key,String... members){
    try {
      List<Point> points = redisTemplate.opsForGeo().position(key, members);
      if (null==points) return new HashMap<>(0);
      int size = points.size();
      Map<String,Point> result = new HashMap<>(size);
      for (int i = 0; i < size; i++) {
        if (points.get(i)==null) continue;
        result.put(members[i],points.get(i));
      }
      return result;
    }catch (Exception e){
      logger.warn("redisService.geoGet异常：", e);
      return new HashMap<>(0);
    }
  }

  /**
   * 从GEO缓存中获取指定位置的地名和GeoHash值
   * @param key      redis的key
   * @param members  地名
   * @return  地名:GeoHash值 的Map
   */
  public Map<String,String> geoHashGet(String key,String... members){
    try {
      List<String> geoHash = redisTemplate.opsForGeo().hash(key, members);
      if (null==geoHash) return new HashMap<>(0);
      int size = geoHash.size();
      Map<String,String> result = new HashMap<>(size);
      for (int i = 0; i < size; i++) {
        if (geoHash.get(i)==null) continue;
        result.put(members[i],geoHash.get(i));
      }
      return result;
    }catch (Exception e){
      logger.warn("redisService.geoHashGet异常：", e);
      return new HashMap<>(0);
    }
  }

  /**
   * 获得redis GEO中缓存的两地之间的km距离
   * 在极端情况下可能会产生高达0.5％的误差
   * @param key       redis的key
   * @param member1   地名1
   * @param member2   地名2
   * @return    两地间的千米距离
   */
  public double geoDistanceKm(String key,String member1,String member2){
    try {
      Distance distance = redisTemplate.opsForGeo().distance(key, member1, member2, DistanceUnit.KILOMETERS);
      return distance==null ? 0.0 : distance.getValue();
    }catch (Exception e){
      logger.warn("redisService.geoDistanceKm异常：", e);
      return 0.0;
    }
  }

  /**
   * 获得redis GEO中缓存的两地之间的m距离
   * 在极端情况下可能会产生高达0.5％的误差
   * @param key       redis的key
   * @param member1   地名1
   * @param member2   地名2
   * @return    两地间的米距离
   */
  public double geoDistance(String key,String member1,String member2){
    try {
      Distance distance = redisTemplate.opsForGeo().distance(key, member1, member2);
      return distance==null ? 0.0 : distance.getValue();
    }catch (Exception e){
      logger.warn("redisService.geoDistance异常：", e);
      return 0.0;
    }
  }

  /**
   * GEO类型还需要注意的是：
   *    在Redis 6.2.0中GEORADIUS命令被视为已弃用，在新代码中使用GEOSEARCH
   *    在Redis 6.2.0中GEORADIUSBYMEMBER命令被视为已弃用，在新代码中使用GEOSEARCHSTORE
   */


  // ========================================redis  stream相关操作========================================

  /* Stream是Redis 5.0引入的新数据类型，基于RadixTree数据结构实现，用于做消息队列(即MQ)
   * 以下是stream的一般操作命令
   */

  /**
   * 向redis指定key的stream中追加消息
   * @param key       redis的键
   * @param content   向stream中追加的消息
   * @return  消息ID(使用redis生成的ID 时间戳+序号)
   */
  public String streamAdd(@NotBlank String key,@NotNull Map<String,Object> content){
    try {
      RecordId recordId = redisTemplate.opsForStream().add(key, content);
      if (recordId!=null){
        return recordId.getValue();
      }
      return "";
    }catch (Exception e){
      logger.warn("redisService.streamAdd异常：", e);
      return "";
    }
  }


  /**
   * 从stream中获取指定区间的消息
   * @param key   redis的key
   * @param from  开始ID(包含)
   * @param to    结束ID(包含)
   * @return   指定范围内的消息
   */
  public List<MapRecord<String, Object, Object>> streamRange(@NotBlank String key,@NotBlank String from,@NotBlank String to){
    try {
      return redisTemplate.opsForStream().range(key, Range.from(Bound.inclusive(from)).to(Bound.inclusive(to)));
    }catch (Exception e){
      logger.warn("redisService.streamRange异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 从stream中获取该队列中前count条消息
   * @param key     redis的key
   * @param count   消息数量
   * @return    消息集合
   */
  public List<MapRecord<String, Object, Object>> streamRange(@NotBlank String key,int count) {
    try {
      return redisTemplate.opsForStream().range(key, Range.unbounded(),Limit.limit().count(count));
    } catch (Exception e) {
      logger.warn("redisService.streamRange异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 以倒序的方式从stream中获取到该队列中指定范围的消息
   * @param key   redis的key
   * @param from  开始ID(包含)
   * @param to    结束ID(包含)
   * @return    指定范围内的消息
   */
  public List<MapRecord<String, Object, Object>> streamReverseRange(@NotBlank String key,@NotBlank String from,@NotBlank String to){
    try {
      return redisTemplate.opsForStream().reverseRange(key, Range.from(Bound.inclusive(from)).to(Bound.inclusive(to)));
    }catch (Exception e){
      logger.warn("redisService.streamReverseRange异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 以倒序的方式从stream中获取该队列中倒数count条消息
   * @param key     redis的key
   * @param count   消息数量
   * @return    消息集合
   */
  public List<MapRecord<String, Object, Object>> streamReverseRange(@NotBlank String key,int count){
    try {
      return redisTemplate.opsForStream().reverseRange(key, Range.unbounded(),Limit.limit().count(count));
    }catch (Exception e){
      logger.warn("redisService.streamReverseRange异常：", e);
      return new ArrayList<>(0);
    }
  }


  /**
   * 查询stream中的消息数量
   * @param key  redis的key
   * @return  消息数量
   */
  public long streamLength(@NotBlank String key){
    try {
      Long size = redisTemplate.opsForStream().size(key);
      return size==null ? 0L : size;
    } catch (Exception e) {
      logger.warn("redisService.streamLength异常：", e);
      return 0L;
    }
  }

  /**
   * 对stream的长度进行裁剪，如果超出此长度则将最早的数据驱逐出去
   * @param key     redis的key
   * @param count   裁剪后的数量
   * @return   驱逐的消息数量
   */
  public long streamTrim(@NotBlank String key, long count){
    try {
      Long num = redisTemplate.opsForStream().trim(key, count);
      return num!=null ? num : 0L;
    }catch (Exception e){
      logger.warn("redisService.streamTrim异常：", e);
      return 0L;
    }
  }

  /**
   * 从stream消息列表中移除指定id的消息
   * @param key redis的key
   * @param ids 消息ID数组
   * @return  移除的消息数量
   */
  public long streamDel(@NotBlank String key, @NotNull String... ids){
    try {
      Long delete = redisTemplate.opsForStream().delete(key, ids);
      return delete!=null ? delete : 0L;
    }catch (Exception e){
      logger.warn("redisService.streamDel异常：", e);
      return 0L;
    }
  }


  /* 以下是stream消费相关命令 */

  /**
   * 从某个stream流中获取消息，可以指定游标ID、消息数量、阻塞等待时长
   * @param key       redis的key(即stream流的名称)
   * @param offsetId  游标ID。即为参考消息ID，需手动指定，不含当前消息ID
   * @param seconds   阻塞时长。即当游标之后不存在消息时阻塞等待的时长，为0则表示不阻塞等待
   * @param count     返回的消息数量
   * @return  消息集合
   */
  public List<MapRecord<String, Object, Object>> streamRead(@NotBlank String key,@NotBlank String offsetId,long seconds,long count){
    try {
      StreamReadOptions streamReadOption = StreamReadOptions.empty().count(count);
      if (seconds>0){ streamReadOption.block(Duration.ofSeconds(seconds)); }
      return redisTemplate.opsForStream().read(streamReadOption,StreamOffset.create(key, ReadOffset.from(offsetId)) );
    }catch (Exception e){
      logger.warn("redisService.streamRead异常：", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * 从多个stream流中获取消息，需要指定每个流开始读取的游标ID、key
   * @param streamOffsets   stream流设置  StreamOffset.create(key值, ReadOffset.from(游标ID))
   * @param seconds         阻塞时长。即当游标之后不存在消息时阻塞等待的时长，为0则表示不阻塞等待（共同的设置）
   * @param count           返回的消息数量（共同的设置）
   * @return  消息集合
   */
  public List<MapRecord<String, Object, Object>> streamRead(@NotNull StreamOffset<String>[] streamOffsets,long seconds,long count){
    StreamReadOptions streamReadOption = StreamReadOptions.empty().count(count);
    if (seconds>0){ streamReadOption.block(Duration.ofSeconds(seconds)); }
    return redisTemplate.opsForStream().read(streamReadOption, streamOffsets );
  }

  /**
   * 从某个stream流中获取新加入(监听时间之后)的消息，可以指消息数量、阻塞等待时长
   * @param key       redis的key(即stream流的名称)
   * @param seconds   阻塞时长。即当游标之后不存在消息时阻塞等待的时长，为0则表示不阻塞等待
   * @param count     返回的消息数量
   * @return  消息集合
   */
  public List<MapRecord<String, Object, Object>> streamRead(@NotBlank String key,long seconds,long count){
    try {
      StreamReadOptions streamReadOption = StreamReadOptions.empty().count(count);
      if (seconds>0){ streamReadOption.block(Duration.ofSeconds(seconds)); }
      return redisTemplate.opsForStream().read(streamReadOption,StreamOffset.create(key, ReadOffset.latest()) );
    }catch (Exception e){
      logger.warn("redisService.streamRead异常：", e);
      return new ArrayList<>(0);
    }
  }

  //======================================== Lua脚本操作 ========================================

  public Object sendjobUnlockScriptExecute(String k, String v){
    return redisTemplate.execute(sendjobUnlockScript, Collections.singletonList(k), v);
  }

}
