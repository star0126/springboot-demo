package com.star;

import com.star.client.RedisService;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;

/**
 * @author guoqing_li
 * @create 2021-03-15  11:40:16
 * @description redis stream类型测试
 **/
@SpringBootTest
public class RedisStreamTests {

  @Autowired
  private RedisService redisService;

  @Test
  void streamAdd(){
    System.out.println(redisService.streamAdd("msg1", Collections.singletonMap("name", "消息16")));
    System.out.println(redisService.streamAdd("msg1", Collections.singletonMap("name", "消息17")));
    System.out.println(redisService.streamAdd("msg1", Collections.singletonMap("name", "消息18")));
    System.out.println(redisService.streamAdd("msg1", Collections.singletonMap("name", "消息19")));
    System.out.println(redisService.streamAdd("msg1", Collections.singletonMap("name", "消息20")));
  }

  @Test
  void streamRange(){
    List<MapRecord<String, Object, Object>> msg1 = redisService.streamRange("msg1", "1615781528367-0", "1615781528435-0");
    if (msg1!=null){
      for (MapRecord<String, Object, Object> m : msg1) {
        System.out.println(m.toString());
      }
    }

    System.out.println("---------------------------");

    List<MapRecord<String, Object, Object>> msg2 = redisService.streamRange("msg1",6);
    if (msg2!=null){
      for (MapRecord<String, Object, Object> m : msg2) {
        System.out.println(m.toString());
      }
    }
  }

  @Test
  void streamSize(){
    System.out.println(redisService.streamLength("msg1"));
  }

  @Test
  void streamReverseRange(){
    List<MapRecord<String, Object, Object>> msg1 = redisService.streamReverseRange("msg1", "1615792308700-0", "1615792308751-0");
    if (msg1!=null){
      for (MapRecord<String, Object, Object> m : msg1) {
        System.out.println(m.toString());
      }
    }

    System.out.println("------------------------------");

    List<MapRecord<String, Object, Object>> msg2 = redisService.streamReverseRange("msg1", 20);
    if (msg2!=null){
      for (MapRecord<String, Object, Object> m : msg2) {
        System.out.println(m.toString());
      }
    }
  }

  @Test
  void streamTrim(){
    System.out.println(redisService.streamTrim("msg1", 15));
  }

  @Test
  void streamDel(){
    System.out.println(redisService.streamDel("msg1", "1615792308651-0"));
  }


  @Test
  void streamRead(){
    long l1 = System.currentTimeMillis();
    List<MapRecord<String, Object, Object>> mapRecords = redisService.streamRead("msg1","1615960229947-0",3,1);
    if (mapRecords!=null){
      for (MapRecord<String, Object, Object> mapRecord : mapRecords) {
        System.out.println(mapRecord.toString());
      }
    }
    long l2 = System.currentTimeMillis();

    System.out.println(l2-l1);
    List<MapRecord<String, Object, Object>> mapRecords2 = redisService.streamRead("msg1","1615960229947-0",0,1);
    if (mapRecords2!=null){
      for (MapRecord<String, Object, Object> mapRecord : mapRecords2) {
        System.out.println(mapRecord.toString());
      }
    }

    long l3 = System.currentTimeMillis();
    System.out.println(l3-l2);
  }

  @Test
  void streamRead2(){
    StreamOffset<String>[] s = new StreamOffset[]{
        StreamOffset.create("msg2", ReadOffset.from("1616032563142-0")),
        StreamOffset.create("msg1", ReadOffset.from("1615960229922-0"))
    };

    List<MapRecord<String, Object, Object>> mapRecords = redisService.streamRead(s, 1, 1);
    if (mapRecords!=null){
      for (MapRecord<String, Object, Object> mapRecord : mapRecords) {
        System.out.println(mapRecord.toString());
      }
    }
  }

  @Test
  void streamRead3() throws InterruptedException {

    List<MapRecord<String, Object, Object>> msg1 = redisService.streamRead("msg3", 30000, 2);
    if (msg1!=null){
      for (MapRecord<String, Object, Object> entries : msg1) {
        System.out.println(entries.toString());
      }
    }

  }

}
