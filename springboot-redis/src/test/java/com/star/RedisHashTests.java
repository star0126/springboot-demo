package com.star;

import com.star.client.RedisService;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author guoqing_li
 * @create 2021-03-24  11:35:47
 * @description redis map测试
 **/
@SpringBootTest
public class RedisHashTests {

  @Autowired
  RedisService redisService;

  @Test
  void hashPutTest(){
    Map<Object,Object> value = new HashMap<>(300);
    for (int i = 0; i < 800; i++) {
      value.put(UUID.randomUUID().toString().replaceAll("-", ""),1);
    }
    boolean b = redisService.hashPut("hash-key1", value);
    System.out.println(b);
  }

  @Test
  void hashHasKeyTests(){
    long l = System.currentTimeMillis();
    boolean b = redisService.hashHasKey("hash-key1", "5c3b78575b7043adb37cb8658f674a12");
    long e = System.currentTimeMillis();
    System.out.println(b);
    System.out.println(e-l);
  }

  @Test
  void hashGetTests(){
    long l = System.currentTimeMillis();
    Object o = redisService.hashGet("hash-key1", "5c3b78575b7043adb37cb8658f674a12");
    long e = System.currentTimeMillis();
    System.out.println(o);
    System.out.println(e-l);
  }

}
