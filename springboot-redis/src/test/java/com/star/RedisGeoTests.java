package com.star;

import com.star.client.RedisService;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Point;

/**
 * @author guoqing_li
 * @create 2021-03-11  13:47:23
 * @description redis GEO测试类
 **/
@SpringBootTest
public class RedisGeoTests {

  @Autowired
  RedisService redisService;

  @Test
  void addTest1(){
    System.out.println(redisService.geoAdd("city1", 117.03626124023438, 36.6384770095912, "济南"));
    System.out.println(redisService.geoAdd("city1", 112.57137028335572, 37.86402387385352, "太原"));
  }

  @Test
  void addTest2(){
    Map<Object, Point> map = new HashMap<>(2);
    map.put("吕梁",new Point(110.90385166763306,37.43889512963899));
    map.put("介休",new Point(111.92015322753909,37.017780300847434));
    System.out.println(redisService.geoAdd("city1", map));
  }

  @Test
  void getTest(){
    Map<String, Point> points = redisService.geoGet("city1", "襄汾", "吕梁", "介休", "临汾");
    if (points!=null){
      points.forEach((x, y)-> System.out.println(x+"-"+y.toString()));
    }
  }

  @Test
  void getHashTest(){
    Map<String,String> o = redisService.geoHashGet("city1", "襄汾", "吕梁", "介休", "临汾");
    if (o!=null){
      o.forEach((x, y)-> System.out.println(x+"-"+y));
    }
  }

  @Test
  void getGeoDistance(){
    double distance = redisService.geoDistanceKm("city1", "吕梁", "介休");
    double distance2 = redisService.geoDistance("city1", "吕梁", "介休");
    System.out.println(distance);
    System.out.println(distance2);
  }

}
