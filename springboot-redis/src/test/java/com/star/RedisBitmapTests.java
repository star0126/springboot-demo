package com.star;

import com.star.client.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author guoqing_li
 * @create 2021-03-09  16:47:09
 * @description  redis位图操作测试类
 **/
@SpringBootTest
public class RedisBitmapTests {

  @Autowired
  RedisService redisService;

  @Test
  void bitmapSetTest(){
    System.out.println(redisService.bitmapSet("key3", 3L,true));
  }

  @Test
  void bitmapGetTest(){
    System.out.println(redisService.bitmapGet("key1", 1L));
  }

  @Test
  void bitmapCountTest(){
    System.out.println(redisService.bitmapCount("key1"));
    System.out.println(redisService.bitmapCount("key1",0,100));
    System.out.println(redisService.bitmapCount("key1",3,100));
  }

  @Test
  void bitPosTest(){
    long result = redisService.bitPos("key1", true, 0L, 4L);
    System.out.println(result);

    long result2 = redisService.bitPos("key1", true, 4L, 8L);
    System.out.println(result2);
  }

  @Test
  void bitOpAndTest(){
    System.out.println(redisService.bitOpAnd("key4", "key1", "key2", "key3"));
    System.out.println(redisService.bitOpNot("key4", "key1"));
    System.out.println(redisService.bitOpOr("key4", "key1", "key2", "key3"));
    System.out.println(redisService.bitOpXor("key4", "key1", "key2", "key3"));
  }

  @Test
  void bitFieldTest(){
    System.out.println(redisService.bitFieldGetI32("key1", 0));
    System.out.println(redisService.bitFieldGetU8("key1", 0));
    System.out.println(redisService.bitFieldSetI8("key1", 0,8));
    System.out.println(redisService.bitFieldGetU8("key1", 0));
  }

}
