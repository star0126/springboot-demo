package com.star;

import com.star.client.RedisService;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @ProjectName: springboot-demo
 * @since: JDK-1.8
 * @author: dapeng-liguoqing
 * @create: 2020-05-27  18:08:37
 * @version: v1.0
 * @description:
 **/
@SpringBootTest
public class SpringbootRedisApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisService redisService;

    @Test
    void contextLoads(){
        redisTemplate.opsForValue().set("aaaa",123456);
        Long aaaa = redisTemplate.opsForValue().increment("aaaa");
        System.out.println(aaaa);
    }


    @Test
    void scanTestReady(){
        redisService.strSet("string1","dehfhjsdfj");
        redisService.hashPut("hash1","aaaa",1111);
        redisService.listLeftAdd("list2","dkjsdj");
        redisService.setAdd("set1","sdkjfsjf");
        redisService.zsetAdd("zset1","dhshdddks",100.5);
    }

    @Test
    void scanTest(){
        Set<String> scan = redisService.scan("*", 2);
        if (scan!=null){
            scan.forEach(System.out::println);
        }
    }

}
