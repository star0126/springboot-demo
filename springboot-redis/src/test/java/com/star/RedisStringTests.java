package com.star;

import com.alibaba.fastjson.JSONObject;
import com.star.client.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author guoqing_li   2021-07-06  09:40:36
 **/
@SpringBootTest
public class RedisStringTests {

    @Autowired
    private RedisService redisService;

    @Test
    void strAdd(){
        redisService.strSet("mytest1","hwfhwuhfwu");
        redisService.strSet("mytest2",123456);
        Map<String,Object> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c","sjdf");
        redisService.strSet("mytest3",map);
        redisService.strSet("mytest4",new Date());
        Aa aa = new Aa();
        aa.setA("sdh");
        aa.setB(43436);
        redisService.strSet("mytest5",aa);
        redisService.strSet("mytest6",new Aa());
        redisService.strSet("mytest7",new ArrayList<>());
    }


    @Test
    void strGet(){
        Object mytest7 = redisService.strGet("mytest7");
        System.out.println(JSONObject.toJSONString(JSONObject.parseArray(mytest7+"",List.class) ));
        Object mytest6 = redisService.strGet("mytest6");
        System.out.println(JSONObject.parseObject( mytest6+"",Aa.class).toString());
        Object mytest5 = redisService.strGet("mytest5");
        System.out.println(mytest5.toString());
        Object mytest4 = redisService.strGet("mytest4");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(simpleDateFormat.format(mytest4));
        Object mytest3 = redisService.strGet("mytest3");
        System.out.println(JSONObject.toJSONString(mytest3));
        Integer mytest2 = (Integer)redisService.strGet("mytest2");
        System.out.println(mytest2);
        String mytest1 = (String)redisService.strGet("mytest1");
        System.out.println(mytest1);
    }

    public static class Aa{
        String a;
        Integer b;

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        public Integer getB() {
            return b;
        }

        public void setB(Integer b) {
            this.b = b;
        }

        @Override
        public String toString() {
            return "Aa{" +
                    "a='" + a + '\'' +
                    ", b=" + b +
                    '}';
        }
    }

}
