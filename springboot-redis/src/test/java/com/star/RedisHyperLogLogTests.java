package com.star;

import com.star.client.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author guoqing_li
 * @create 2021-03-11  11:43:29
 * @description redis HyperLogLog测试
 **/
@SpringBootTest
public class RedisHyperLogLogTests {

  @Autowired
  RedisService redisService;

  @Test
  void addTest(){
    System.out.println(redisService.hyperLogLogAdd("key1", "137.0.0.1","569.0.0.2","569.2.3.155","524.0.0.1","127.0.0.2"));
  }

  @Test
  void countTest(){
    System.out.println(redisService.hyperLogLogCount("key2"));
  }

  @Test
  void mergeTest(){
    System.out.println(redisService.hyperLogLogMerge("key3", "key1", "key2"));
  }

}
