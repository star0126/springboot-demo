package com.star.service;

import com.star.dao.BmTaskDao;
import com.star.entity.BmTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  16:39:44
 * @description
 **/
@Service
public class BmTaskServiceImpl {

    @Autowired
    private BmTaskDao taskDao;

    public List<BmTask> find(){
        List<BmTask> all = taskDao.findAll();
        all.forEach(task -> System.out.println(task.toString()));
        return all;
    }

}
