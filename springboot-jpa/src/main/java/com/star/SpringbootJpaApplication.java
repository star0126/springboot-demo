package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  16:17:25
 * @description springboot启动器
 **/
@SpringBootApplication
public class SpringbootJpaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootJpaApplication.class,args);
    }
}