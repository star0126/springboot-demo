package com.star.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  16:19:22
 * @description task实体类
 **/
@Entity
public class BmTask implements Serializable {

    @Id
    @GeneratedValue
    private Integer taskId;  //编号
    private String taskName; //任务名
    private String taskType; //任务类型
    private String taskCreator; //任务创建者编号
    private Integer creatorDept; //创建者部门
    private Date creatTime; //任务创建时间
    private Date endTime; //任务结束时间
    private Date planStartTime; //任务计划开始时间
    private Date planEndTime; //任务计划结束时间
    private String taskExecutor; //任务执行者编号
    private String taskStatus; //任务状态
    private String taskSpec; //任务说明


    public BmTask() {
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskCreator() {
        return taskCreator;
    }

    public void setTaskCreator(String taskCreator) {
        this.taskCreator = taskCreator;
    }

    public Integer getCreatorDept() {
        return creatorDept;
    }

    public void setCreatorDept(Integer creatorDept) {
        this.creatorDept = creatorDept;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getPlanStartTime() {
        return planStartTime;
    }

    public void setPlanStartTime(Date planStartTime) {
        this.planStartTime = planStartTime;
    }

    public Date getPlanEndTime() {
        return planEndTime;
    }

    public void setPlanEndTime(Date planEndTime) {
        this.planEndTime = planEndTime;
    }

    public String getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(String taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskSpec() {
        return taskSpec;
    }

    public void setTaskSpec(String taskSpec) {
        this.taskSpec = taskSpec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BmTask bmTask = (BmTask) o;
        return Objects.equals(taskId, bmTask.taskId) &&
                Objects.equals(taskName, bmTask.taskName) &&
                Objects.equals(taskType, bmTask.taskType) &&
                Objects.equals(taskCreator, bmTask.taskCreator) &&
                Objects.equals(creatorDept, bmTask.creatorDept) &&
                Objects.equals(creatTime, bmTask.creatTime) &&
                Objects.equals(endTime, bmTask.endTime) &&
                Objects.equals(planStartTime, bmTask.planStartTime) &&
                Objects.equals(planEndTime, bmTask.planEndTime) &&
                Objects.equals(taskExecutor, bmTask.taskExecutor) &&
                Objects.equals(taskStatus, bmTask.taskStatus) &&
                Objects.equals(taskSpec, bmTask.taskSpec);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, taskName, taskType, taskCreator, creatorDept, creatTime, endTime, planStartTime, planEndTime, taskExecutor, taskStatus, taskSpec);
    }

    @Override
    public String toString() {
        return "BmTask{" +
                "taskId=" + taskId +
                ", taskName='" + taskName + '\'' +
                ", taskType='" + taskType + '\'' +
                ", taskCreator='" + taskCreator + '\'' +
                ", creatorDept=" + creatorDept +
                ", creatTime=" + creatTime +
                ", endTime=" + endTime +
                ", planStartTime=" + planStartTime +
                ", planEndTime=" + planEndTime +
                ", taskExecutor='" + taskExecutor + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", taskSpec='" + taskSpec + '\'' +
                '}';
    }
}
