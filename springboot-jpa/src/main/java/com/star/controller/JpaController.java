package com.star.controller;

import com.star.service.BmTaskServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  16:55:44
 * @description
 **/
@RestController
public class JpaController {

    @Autowired
    private BmTaskServiceImpl taskService;

    @GetMapping("/find")
    public Object find(){
        return taskService.find();
    }

}
