package com.star.dao;

import com.star.entity.BmTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dapeng-liguoqing
 * @create 2020-08-25  16:21:24
 * @description dao服务层
 **/
public interface BmTaskDao extends JpaRepository<BmTask, String> {
}
