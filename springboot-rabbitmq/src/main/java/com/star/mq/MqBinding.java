package com.star.mq;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author guoqing_li   2022-02-14  09:55:03
 * RabbitMQ主题和交换机绑定类
 **/
@Component
public class MqBinding {

    /**
     * 主题交换机
     */
    @Bean("topicExchange")
    public TopicExchange topicExchange(){
        return ExchangeBuilder.topicExchange(Constant.EXCHANGE).durable(true).build();
    }

    /**
     * 微信消息队列
     */
    @Bean("wxQueue")
    public Queue wxQueue(){
        return QueueBuilder.durable(Constant.QUEUE_WX).build();
    }

    /**
     * 绑定消息队列和交换器，绑定键为 wx.#
     */
    @Bean
    public Binding bindTopicExchange(@Qualifier("wxQueue") Queue wxQueue, @Qualifier("topicExchange") TopicExchange topicExchange){
        return BindingBuilder.bind(wxQueue).to(topicExchange).with("wx.#");
    }

}
