package com.star.mq;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

/**
 * @author guoqing_li   2022-02-24  14:56:41
 * RabbitMQ生产者
 **/
@Service
@Validated
public class Producer {

    @Resource
    private RabbitTemplate rabbitTemplate;


    public void sendMsg(@NotBlank String exchange, @NotBlank String routingKey, @NotBlank String type, @NotEmpty Map<String,Object> msg){

        String msgId = UUID.randomUUID().toString().replaceAll("-","");

        // 构建消息头
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("id",msgId);
        messageProperties.setHeader("type",type);

        // 构建消息
        Message message = new Message(JSONObject.toJSONString(msg).getBytes(StandardCharsets.UTF_8),messageProperties);

        // 构建消息回调
        CorrelationData correlationData = new CorrelationData();
        correlationData.setId(msgId);
        correlationData.setReturnedMessage(message);

        rabbitTemplate.convertAndSend(exchange,routingKey, message, correlationData);
    }


}
