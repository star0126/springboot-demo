package com.star.mq;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author guoqing_li   2022-02-11  15:35:45
 * RabbitMQ消费者
 **/
@Component
public class Consumer {


    private static final Logger log = LoggerFactory.getLogger(Consumer.class);

    /**
     * 监听绑定了topic交换机的的消息队列
     */
    @RabbitListener(queues = Constant.QUEUE_WX)
    public void topicConsumer(Message message, Channel channel){
        MessageProperties messageProperties = message.getMessageProperties();
        byte[] body = message.getBody();
        log.info("---------------------------");
        log.info("收到消息：{}",new String(body));
        log.info("交换机：{}",messageProperties.getReceivedExchange());
        log.info("消息管道：{}",messageProperties.getConsumerQueue());
        log.info("路由键：{}",messageProperties.getReceivedRoutingKey());
        log.info("---------------------------");
        try {
            // 手动进行消费者应答，如果消息无成功应答则消息依然会被保存在队列中，可以开启重试机制，当前消费失败的信息不会影响后续消息的消费
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
