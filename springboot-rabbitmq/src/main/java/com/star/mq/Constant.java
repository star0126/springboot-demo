package com.star.mq;

/**
 * @author guoqing_li   2022-02-11  15:59:18
 * RabbitMQ常量类
 **/
public class Constant {

    public final static String EXCHANGE = "message_service";

    public final static String QUEUE_WX = "wx_queue";

    public final static String KEY_WX = "wx.a";

}
