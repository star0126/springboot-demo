package com.star.mq.model;

import org.springframework.amqp.core.Message;

import java.io.Serializable;

/**
 * @author guoqing_li   2022-02-21  14:11:51
 * RabbitMQ消息模型
 **/
public class RabbitmqMsgModel implements Serializable {

    private String exchange;
    private String queue;
    private String routingKey;
    private String msgId;
    private String type;
    private Message message;


    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
