package com.star.controller;

import com.star.mq.Constant;
import com.star.mq.Producer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author guoqing_li   2022-02-11  15:28:27
 * 测试控制器
 **/
@RestController
public class TestController {

    @Resource
    private Producer producer;

    @PostMapping(value="/topic")
    public ResponseEntity<String> sendTopic(@RequestParam(required = false ,defaultValue = Constant.KEY_WX) String key,
                                            @RequestBody Map<String,Object> map){

        String type = "MQ:test:wx";

        producer.sendMsg(Constant.EXCHANGE,key,type,map);
        return ResponseEntity.ok().body("success");
    }

}
