package com.star.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author guoqing_li   2022-02-11  15:30:44
 **/
@Configuration
public class RabbitmqConfig {

    private static final Logger log = LoggerFactory.getLogger(RabbitmqConfig.class);

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
        rabbitTemplate.setConnectionFactory(connectionFactory);
        // 设置序列化机制
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        // 消息是否成功发送回调机制设置
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (ack){
                log.info("RabbitMQ消息发送成功（correlationData={}）",correlationData);
            } else {
                log.error("RabbitMQ消息发送失败（correlationData={} cause={}）",correlationData,cause);
            }
        });
        // 消息是否被成功路由到队列回调机制设置
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey)->{
            log.error("RabbitMQ消息未被路由到队列（message={}, replyCode={}, replyText={}, exchange={}, routingKey={}）",
                    message, replyCode, replyText, exchange, routingKey);
        });
        return rabbitTemplate;
    }



}
