# RabbitMQ

## RabbitMQ安装部署
- ### docker
  ```shell
  # rabbitmq:management带有管理界面插件
  docker pull rabbitmq:management
  # 5672是消息收发端口，15672是管理界面端口
  # RABBITMQ_DEFAULT_USER是初始管理员用户名环境变量，RABBITMQ_DEFAULT_PASS是初始管理员密码环境变量，如不设置则使用默认
  docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin rabbitmq:management
  ```
  - docker容器启动后在浏览器中打开 http://localhost:15672/ 即可进入rabbitmq管理端界面