# springboot-demo

本项目是使用maven构建的springboot-2.3使用示例

springboot-2.3发行说明：[发行说明链接](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.3-Release-Notes)



**当前项目中使用的springboot版本：springboot 2.3.8.RELEASE**



- ### springboot-actuator
  springboot整合actuator监控模块
  
  - 集成技术：
    - actuator
    - docker
  - 主要内容：
    - actuator简单配置
    - tomcat日志收集简单配置
  
- ### springboot-aop
  springboot整合aop模块

  - 集成技术：
    - aop
    - docker
    - log4j2日志
  - 主要内容：
    - aop前置通知、后置通知、返回通知、异常通知、环绕增强(日志收集)

- ### springboot-druid
  springboot整合阿里druid数据库连接池模块

  - 集成技术：
    - jdbcTemplate
    - MySQL  5.5.56
    - Druid数据库连接池
    - mybatis-plus 3.4.2
    - knife4j生成在线接口文档
  - 主要内容：
    - mybatis plus操作mysql简单示例
    - Druid数据库连接池简单配置

- ### springboot-easypoi
  springboot整合easypoi模块

  - 集成技术：
    - MySQL 8.0.22
    - mybatis-plus 3.4.2
    - Hikari数据库连接池
    - easypoi 4.2.0 
  - 主要内容：
    - 简单的xlsx格式Excel文件导出（从MySQL导出到Excel）
    - 简单的Excel文件导入（从Excel导入到MySQL）
  - 文档链接：
    - easypoi 用户手册--[链接地址](http://doc.wupaas.com/docs/easypoi/easypoi-1c0u4mo8p4ro8)




- ### springboot-elasticsearch-bboss
  springboot整合elasticsearch(使用elasticsearch bboss)模块



- ### springboot-jdbctemplate
  springboot整合jdbcTemplate模块
  
  - 集成技术：
    - jdbcTemplate
    - MySQL  5.5.56
    - Hikari数据库连接池
  - 主要内容：
    - jdbcTemplate操作mysql简单示例
  
- ### springboot-jpa
  springboot整合阿里jpa模块

  - 集成技术：
    - jpa
    - MySQL  5.5.56
    - Hikari数据库连接池
  - 主要内容：
    - jpa操作mysql简单示例

- ### springboot-kafka
  springboot整合kafka模块

  - 集成技术：
    - Kafka  2.11-2.4.0
    - fastjson
  - 主要内容：
    - kafka生产者、消费者简单示例

- ### springboot-knife4j
  springboot整合knife4j增强接口文档模块

  - 集成技术：
    - knife4j 2.0.2
  - 主要内容：
    - knife4j生成在线接口文档
    - knife4j在生产环境启停配置

- ### springboot-mongodb
  springboot整合mongodb模块
  
  - 集成技术：
    - mongoDB 4.4.2
  - 主要内容：
    - 使用mongoTemplate操作mongo
    - mongo增删改查操作演示

- ### springboot-quartz
  springboot整合quartz模块

  - 集成技术：
    - Quartz
  - 主要内容：
    - 基于内存缓存的Quartz定时任务

- ### springboot-rabbitmq
  springboot整合RabbitMQ模块

  - 集成技术：
    - RabbitMQ 3.9.13
  - 主要内容：
    - docker搭建RabbitMQ
    - rabbitTemplate操作RabbitMQ
    - 主题模式消息生产与消费
    - 手动ACK

- ### springboot-redis
  springboot整合redis模块
  
  - 集成技术：
  - redis 6.0.9
  - 主要内容：
    
    - 使用lettuce连接池连接redis，使用redisTemplate操作redis
    - redis的key采用String序列化、value采用Jackson序列化并去除null值和包路径
    - 使用validation校验redis操作工具类入参
    - redis基本数据类型string、list、set、hash、zset的操作工具
    - redis扩展数据类型bitmap、hyperLogLog、geo的操作工具
    - redis扩展数据类型stream的操作工具
    - redis执行lua脚本
    - redis单机版分布式锁（待完成）
    - redis集群版分布式锁（待完成）
  - 文档链接：
    
    - redis官方的命令文档--[链接地址](https://redis.io/commands)
    - redis使用手册(版本较老)--[链接地址](http://redisdoc.com/index.html)
    - redis命令演示网页--[链接地址](https://try.redis.io/)
    - 腾讯云redis教程手册--[链接地址](https://cloud.tencent.com/developer/doc/1203)
  
- ### springboot-rocketmq
  springboot整合RocketMQ模块

  - 集成技术：
    - RocketMQ 4.8.0 (开启ACL安全功能)
    - fastjson
    
  - 主要内容：
    - 消息生产者--转换发送
    - 消息生产者--异步发送
    - Push模式消息消费者
    
  - 文档链接：
    - rocketmq-spring用户手册--[链接地址](https://github.com/apache/rocketmq-spring/wiki/%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8C)

- ### springboot-thymeleaf
  springboot整合Thymeleaf模块

  - 集成技术：
    - Thymeleaf