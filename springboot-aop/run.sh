#!/usr/bin/env bash

mvn clean install package -Dmaven.test.skip=true
echo 'springboot-aop 项目打包完成'

docker rmi springboot-aop:latest

docker build -t springboot-aop:latest .
echo 'springboot-aop 镜像创建完成'

docker run -d --name "springboot-aop" -e JAVA_OPTS='-Xmx500M -Xms300M -XX:MaxMetaspaceSize=192M -XX:MetaspaceSize=192M' springboot-aop:latest
echo 'springboot-aop 容器启动完成'