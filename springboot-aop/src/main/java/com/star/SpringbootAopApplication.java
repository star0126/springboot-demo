package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li
 * @create 2020-10-10  10:51:11
 * @description springboot启动器
 **/
@SpringBootApplication
public class SpringbootAopApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootAopApplication.class,args);
    }
}
