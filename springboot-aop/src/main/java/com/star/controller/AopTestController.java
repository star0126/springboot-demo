package com.star.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guoqing_li
 * @create 2020-10-10  10:56:59
 * @description aop测试控制器
 **/
@RestController
public class AopTestController {

    @GetMapping("/zimuge")
    public String zimuge(){
        String msg = "字母哥上场打球";
        System.out.println(msg);
        return msg;
    }

    @GetMapping("/kuli")
    public String kuli(){
        String msg = "库里上场打球";
        System.out.println(msg);
        int err = 10/0;
        return msg;
    }

    @GetMapping("/hadeng")
    public String hadeng(){
        String msg = "哈登上场打球";
        System.out.println(msg);
        return msg;
    }

}
