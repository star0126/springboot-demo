package com.star.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author guoqing_li
 * @create 2020-10-10  11:38:03
 * @description AOP切面类
 **/
@Aspect
@Component
public class AopAspect {

    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 定义切入点
     */
    @Pointcut("execution(* com.star.controller..*.*(..))")
    public void aopPointcut(){}

    /**
     * 前置通知
     */
    @Before("aopPointcut()")
    public void aopBeforeNotice(){
        logger.info("经纪人正在处理球星赛前事务！");
    }

    /**
     * 后置通知
     */
    @After("aopPointcut()")
    public void aopAfterNotice(){
        logger.info("球迷为球星鼓掌！");
    }

    /**
     *返回通知
     */
    @AfterReturning(value = "aopPointcut()", returning = "keys")
    public void aopAfterReturningNotice(JoinPoint joinPoint, Object keys){
        System.out.println("返回值为："+ keys);
        logger.debug("【通知返回】收到球迷的良好反馈");
    }

    /**
     * 异常通知
     */
    @AfterThrowing("aopPointcut()")
    public void aopAfterThrowingNotice(){
        logger.error("【异常通知】球赛出现事故");
    }

    /**
     * 增强环绕 用于实现日志收集
     */
    @Around("aopPointcut()")
    public Object aopAfterNotice(ProceedingJoinPoint pjp) throws Throwable {
        // 前置
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String userId = request.getHeader("x-currUserId");
        String requestUrl = request.getRequestURI();
        String method = request.getMethod();
        String param = Arrays.toString(pjp.getArgs());
        long start = System.currentTimeMillis();
        try {
            // 分界
            Object result = pjp.proceed();
            // 后置
            long end = System.currentTimeMillis();
            long spendTime = end-start;
            int code = 0;
            if (result instanceof ResponseEntity){
                ResponseEntity responseEntity = (ResponseEntity) result;
                code = responseEntity.getStatusCodeValue();
            }
            logger.info("request:{url={},method={},params={},userId={},code={},spendTime={}ms}", requestUrl, method, param, userId, code, spendTime);
            return result;
        }catch (Exception e) {
            logger.error("request error:{url={},method={},params={},userId={},err={}}", requestUrl, method, param, userId, e.toString());
            throw e;
        }
    }


}
