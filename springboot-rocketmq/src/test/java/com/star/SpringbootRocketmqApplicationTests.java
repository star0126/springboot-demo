package com.star;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author guoqing_li
 * @create 2021-01-27  14:37:36
 * @description 单元测试类
 **/
@SpringBootTest
public class SpringbootRocketmqApplicationTests {

  @Test
  void contextLoads(){ }
}
