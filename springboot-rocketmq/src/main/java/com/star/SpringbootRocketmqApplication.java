package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guoqing_li
 * @create 2021-01-27  14:33:25
 * @description springboot启动器
 **/
@SpringBootApplication
public class SpringbootRocketmqApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootRocketmqApplication.class,args);
  }
}
