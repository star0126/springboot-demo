package com.star.service;

import com.alibaba.fastjson.JSONObject;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author guoqing_li
 * @create 2021-01-27  14:43:00
 * @description Push模式下RocketMQ消费者
 **/
@Component
public class PushConsumers{

  @Service
  @RocketMQMessageListener(consumerGroup = "springboot-test", topic = "test-topic-1",accessKey = "rocketmq2", secretKey = "12345678")
  public class PushConsumer1 implements RocketMQListener<Object> {
    @Override
    public void onMessage(Object message) {
      System.out.println("==============================收到消息=======================");
      String msg = JSONObject.toJSONString(message);
      System.out.println(msg);
    }
  }

}
