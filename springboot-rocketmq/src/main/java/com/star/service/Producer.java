package com.star.service;

import com.alibaba.fastjson.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * @author guoqing_li
 * @create 2021-01-27  14:41:50
 * @description RocketMQ生产者
 **/
@Service
public class Producer {

  @Resource
  private RocketMQTemplate rocketMQTemplate;

  private static String TOPIC = "test-topic-1";


  public void sendMsgAndConvert1(){
    System.out.println("==============================开始发送消息=======================");
    System.out.println("发送简单消息..");
    rocketMQTemplate.convertAndSend(TOPIC,"我是测试消息sendMsgAndConvert-1");
    System.out.println("==============================发送消息结束=======================");
  }

  public void sendMsgAndConvert2(){
    System.out.println("==============================开始发送消息=======================");
    System.out.println("发送带headers消息..");
    Map<String,Object> headers = new HashMap<>(2);
    headers.put("author","lgq");
    headers.put("type",0);
    rocketMQTemplate.convertAndSend(TOPIC,"我是测试消息sendMsgAndConvert-2",headers);
    System.out.println("==============================发送消息结束=======================");
  }

  public void sendMsgAndConvert3(){
    System.out.println("==============================开始发送消息=======================");
    System.out.println("发送带后置处理器消息..");
    Map<String,Object> headers = new HashMap<>(2);
    headers.put("author","lgq");
    headers.put("type",0);
    rocketMQTemplate.convertAndSend(TOPIC,"我是测试消息（带后置处理器）",headers, message -> {
      MessageHeaders msgHeaders = message.getHeaders();
      Object msgPayload = message.getPayload();
      System.out.println("后置获得消息headers：" + msgHeaders);
      System.out.println("后置获得消息payload：" + JSONObject.toJSONString(msgPayload));
      return message;
    });
    System.out.println("==============================发送消息结束=======================");
  }

  public void sendAsyncMsg1(){
    System.out.println("==============================开始发送带回调的Async消息=======================");
    Map<String,Object> map = new HashMap<>();
    map.put("name", "mick");
    map.put("age", 52);
    map.put("msg", "我的消息");
    rocketMQTemplate.asyncSend(TOPIC, map, new SendCallback() {
      // 消息发送成功的回调
      @Override
      public void onSuccess(SendResult sendResult) {
        System.out.println("发送成功回调");
        System.out.println(sendResult.toString());
      }
      // 消息发送失败的回调
      @Override
      public void onException(Throwable throwable) {
        System.out.println("发送失败回调");
        System.out.println(throwable.toString());
      }
    });
    System.out.println("==============================发送Async消息结束=======================");
  }

  public void sendAsyncMsg2(){
    System.out.println("==============================开始发送带回调有超时毫秒数的Async消息=======================");
    List<String> list = new ArrayList<>();
    list.add("消息1");
    list.add("消息2");
    rocketMQTemplate.asyncSend(TOPIC, MessageBuilder.withPayload(list).build(),new SendCallback() {
      // 消息发送成功的回调
      @Override
      public void onSuccess(SendResult sendResult) {
        System.out.println("发送成功回调");
        System.out.println(sendResult.toString());
      }
      // 消息发送失败的回调
      @Override
      public void onException(Throwable throwable) {
        System.out.println("发送失败回调");
        System.out.println(throwable.toString());
      }
    },1000);
    System.out.println("==============================发送Async消息结束=======================");
  }

  public void sendAsyncMsg3(){
    System.out.println("==============================开始发送带回调有超时毫秒数有延迟级别的Async消息=======================");

    rocketMQTemplate.asyncSend(TOPIC, MessageBuilder.withPayload(123456).build(),new SendCallback() {
      // 消息发送成功的回调
      @Override
      public void onSuccess(SendResult sendResult) {
        System.out.println("发送成功回调");
        System.out.println(sendResult.toString());
      }
      // 消息发送失败的回调
      @Override
      public void onException(Throwable throwable) {
        System.out.println("发送失败回调");
        System.out.println(throwable.toString());
      }
    },100, 5);
    System.out.println("==============================发送Async消息结束=======================");
  }

}
