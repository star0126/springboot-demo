package com.star.controller;

import com.star.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guoqing_li
 * @create 2021-01-27  16:49:38
 * @description 测试控制器
 **/
@RestController
public class TestController {

  @Autowired
  private Producer producer;

  @GetMapping("/msg/send-1")
  public ResponseEntity<String> sendMsg1(){
    producer.sendMsgAndConvert1();
    producer.sendMsgAndConvert2();
    producer.sendMsgAndConvert3();
    return ResponseEntity.ok().body("发送结束");
  }

  @GetMapping("/msg/send-2")
  public ResponseEntity<String> sendMsg2(){
    producer.sendAsyncMsg1();
    producer.sendAsyncMsg2();
    producer.sendAsyncMsg3();
    return ResponseEntity.ok().body("发送完毕");
  }

}
